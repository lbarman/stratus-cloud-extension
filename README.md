# Stratus - Secure Cloud Extention
## A secure, open-source cloud for collaborative work, with fine-grained rights management. ##
### ###
Created by [Ludovic Barman](http://www.lbarman.ch) as a Master semester project for [LASEC, EPFL](http://lasec.epfl.ch/)

February-June 2014

**************************

### Abstract ###

The purpose of this project is to create a free, open-source, secure, robust cloud software. The common cloud services such as Dropbox lack transparency; the data connections are encrypted, but the storage is presumably not secure enough, which makes these softwares unfit for handling highly sensitive data. In addition, these softwares do not provide fine-grained rights nor robust file versioning.

The objective is therefore to design a software handling local, strong encryption, with different level of fine-grained rights, and the ability to pass or revoke those rights to collaborators. To accelerate the development, we use Dropbox as an underlying file exchange service, providing a common shared insecure repository across the network.

### More informations ###

All information regarding the design (cryptographic scheme, version handling, etc) are detailled in the paper associated with the project. It is available here :

>/deliverables/SecureCloudExtention.pdf

In addition, some slides were made for the presentation given on the 23rd of June 2014, EPFL :

>/deliverables/SecureCloudExtention-Slides-23-06-2014.pdf

### Source code ###

The source code, written in Scala, has been released under the [MIT Licence](http://choosealicense.com/licenses/mit/)

You are welcome to do any improvement to the existing code (help for further developments would be very appreciated), as long as you provide a clear reference to this project and its licence. The details are available here :

>/LICENCE.txt

### Prerequisites ###

Production :

* Java 7 version 51

* Scala 2.10.3

* a modern web browser (web-based GUI)

Development :

* Scala IDE 3.0.2

* SBT (Simple Build Tool) 0.13

### Quick start ###

1. See below section on how to run the software
2. Click on the link "Get started" in the menu. 

All information on how to get the software up-and-running in five minutes for you and your collaborators are explained there.

You can access this file directly (it's a simple HTML file) whithout the program running here :

>/deliverables/getting-started.html

### How to start ###

How to start from the binaries :

1. extract the archive called SecureCloudRepo_vXXX.tar.gz (in deliverables/)

2. in Windows, run .\bin\SecureCloudRepo.bat

3. in Unix, run ./bin/SecureCloudRepo

4. Keep the terminal open

5. Navigate with you webbrowser to http://localhost:8888/



How to start from the source :

1. Using a terminal, navigate to the root folder, where built.sbt is.

2. Run "sbt". Let it resolve the dependencies; the first time, it might take a while

3. run "run –op OPERATION [–file FILENAME ][–anyinfo EXTRA_INFORMATION]" for tests or just "run" for the webserver

4. Navigate with you webbrowser to http://localhost:8888/

You can find the complete list of tests in Appendice-D in the report /deliverables/SecureCloudExtention.pdf

### Futur development policy ###

Although the academic project is finished, I am very interested in further developping this software. All suggestions, bug reports, or improvements are kindly appreciated and will recieve all due credits.

### Contacts ###

[Ludovic Barman](http://www.lbarman.ch)