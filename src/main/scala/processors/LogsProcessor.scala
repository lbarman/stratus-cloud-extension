package main.scala.processors

import java.util.Date

import scala.collection.mutable.LinkedList

import akka.actor.{Actor, actorRef2Scala}

class LogsProcessor extends Actor {

  val dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy' at 'HH:mm:ss");
  val messages = LinkedList[String](dateFormat.format(new Date) + ": " + "[LogsProcessor] Init.")

  def receive = {

    case LoggerWorkerPrintAll =>
      sender ! messages.toArray

    case msg : String =>
      messages append LinkedList(dateFormat.format(new Date) + ": " + msg)
  }
}

case object LoggerWorkerPrintAll