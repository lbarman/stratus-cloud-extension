package main.scala.processors

import scala.concurrent.{ Future, promise }
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.{ Actor, actorRef2Scala }
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{ Config, Core }
import main.scala.Core.SuccessStatusWithMessage
import main.scala.common.{ FileToolBox, PathNormalization, XMLToolBox }
import main.scala.crypto.{ AbstractCryptoPrimitives, KeyDefs, Randomness }
import main.scala.db.engine.{ FieldStatus, LocalKeyDirDatabaseEntry, LocalKeyFileDatabaseEntry, PublicIdentityDatabaseEntry, PublicMessageDatabaseEntry, PublicStructuralDirDatabaseEntry, PublicStructuralFileDatabaseEntry }
import main.scala.db.engine.AbstractDatabase
import main.scala.db.engine.DatabaseField.{ String2DatabaseField, Tuple2DatabaseField }
import main.scala.filesystem.{ AbstractDirectory, AbstractFile }
import main.scala.filesystem.repo.AbstractRepo

/*
 * Input/Output commands
 */
case class CommitNewFile(path : String)
case class CommitNewVersionOfFile(path : String, previousVersion : AbstractFile)
case class CommitDeletionOfFile(path : String, previousVersion : AbstractFile)
case class CommitNewDir(path : String)

case class SendKeyFile(toAddressing : String, keyPath : String, onlyR : Boolean)
case class SendKeyDir(toAddressing : String, keyPath : String, rights : (Boolean, Boolean, Boolean, Boolean))
case class DeleteMessage(msgAddr : String)

case class CommitNewIdentity(claimedIdentity : String)
case class StorePublicIdentity(addressing : String)
case class RejectPublicIdentity(addressing : String)

/**
  * Used to make changes to the repositories. Such changes include :
  * - commit new files, commit new versions of files, commit deletions
  * - commit directories
  * - commit a new identity
  * - accept or reject an identity
  * - send a message or delete it
  * In general, this is where the cryptographic scheme is implemented.
  */
class CommitProcessor(repoSource : AbstractRepo, repoRemote : AbstractRepo,
                      localDb : AbstractDatabase, remoteDb : AbstractDatabase,
                      crypto : AbstractCryptoPrimitives) extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def receive = {

    /*
     * File and directories commits
     */
    case c : CommitNewFile =>
      sender ! Await.result(this.commitNewFile(c.path).mapTo[SuccessStatusWithMessage], Config.defaultTimeout)

    case c : CommitNewVersionOfFile =>
      sender ! this.commitNewVersion(c.path, c.previousVersion)

    case c : CommitDeletionOfFile =>
      sender ! this.commitDeletion(c.path, c.previousVersion)

    case c : CommitNewDir =>
      sender ! Await.result(this.commitNewDir(c.path).mapTo[SuccessStatusWithMessage], Config.defaultTimeout)

    /*
     *  Messages
     */
    case c : SendKeyFile =>
      sender ! this.sendFileKey(c.toAddressing, c.keyPath, c.onlyR)

    case c : SendKeyDir =>
      sender ! this.sendDirKey(c.toAddressing, c.keyPath, c.rights)

    case s : DeleteMessage =>
      sender ! this.deleteMessage(s.msgAddr)

    /*
     * Identities
     */
    case c : CommitNewIdentity =>
      sender ! this.commitNewIdentity(c.claimedIdentity)

    case s : StorePublicIdentity =>
      sender ! this.acceptPublicIdentity(s.addressing)

    case s : RejectPublicIdentity =>
      sender ! this.rejectPublicIdentity(s.addressing)

    case _ =>
      Core log "[CommitProcessor] [Error] Unknown message."
      (false, s"The CommitProcessor does not know what to do with this message.")
  }

  /*
   *     Commits (files, directories)
   * ###########################################################
   */

  /**
    * The addressing computed for each file and directory
    */
  def getAddressingHashInput(absolutePath : String, version : Int, timestamp : Long) =
    "[" + absolutePath + "|" + version + "|" + timestamp + "|" + Core.identity.getPublicKeyString() + "]"

  /**
    * Commits a new directory to the repository, given only a relative, inner path.
    */
  def commitNewDir(relativePathInput : String) : Future[SuccessStatusWithMessage] =
    {
      Core log s"[CommitProcessor] Trying to commit new dir '$relativePathInput'."

      val relativePath = PathNormalization.normalizeForInnerUse(relativePathInput, true, true)
      val aboveDirPath = PathNormalization.innerAbovePath(relativePath, true)

      Core log s"[CommitProcessor] -- relative path is '$relativePath'"
      Core log s"[CommitProcessor] -- above dir path is '$aboveDirPath'"

      Core log s"[CommitProcessor] Querying above path to DiscoveryProcessor..."

      //checking if we have the right for the above dir (i.e. it was discovered) :
      val futureAboveDir = (Core.discoveryProcessor ? new DiscoverAbstractRepo(aboveDirPath)).mapTo[Option[AbstractDirectory]]

      val resultPromise = promise[SuccessStatusWithMessage]
      val resultFuture = resultPromise.future

      futureAboveDir.onFailure {
        case e : Exception =>
          Core log s"[CommitProcessor] [Error] Future DiscoverAbstractRepo($aboveDirPath) failed with reason $e."
          resultPromise success (false, s"Exception : $e")
      }

      futureAboveDir.onSuccess {
        case dirOption : Option[AbstractDirectory] => dirOption match {
          case Some(abstractDir) =>
            {
              val res = this.commitNewDir(relativePath, abstractDir, true)
              resultPromise success res
            }
          case None =>
            Core log s"[CommitProcessor] [Error] Future DiscoverAbstractRepo($aboveDirPath)" +
              s"lead None as an answer. Do you have rights on $aboveDirPath ?"
            resultPromise success (false, s"Cannot find the above directory '$aboveDirPath'; do you have RW rights on it ?")
        }
        case _ =>
          Core log s"[CommitProcessor] [Error] Future DiscoverAbstractRepo($aboveDirPath) didnt" +
            s"lead to an AbstractDirectory. Do you have rights on $aboveDirPath ?"
          resultPromise success (false, s"Cannot find the above directory '$aboveDirPath'; do you have RW rights on it ?")
      }

      resultFuture

    }

  /**
    * Commits a new directory to the repository
    */
  def commitNewDir(relativePathInput : String, aboveDir : AbstractDirectory, acceptCascade : Boolean = true) : SuccessStatusWithMessage =
    {
      Core log "[CommitProcessor] Trying to commit new dir '" + relativePathInput + "', above dir is '" + aboveDir.infos('fullpath) + "'"

      var psdbe = new PublicStructuralDirDatabaseEntry;
      var lkdbe = new LocalKeyDirDatabaseEntry
      val relativePath = PathNormalization.normalizeForInnerUse(relativePathInput, true, true)

      Core log "[CommitProcessor] -- relative path is \"" + relativePath + "\""

      val b = Await.result(ask(Core.discoveryProcessor, DiscoverAbstractRepo(relativePath)).mapTo[Option[AbstractDirectory]], Config.defaultTimeout)

      if (!b.isEmpty) {
        Core log s"[CommitProcessor] -- [Error] '$relativePath' already exists on repository."
        (false, s"'$relativePath' already exists on repository.")
      }
      else if (!FileToolBox.dirDirectlyInside(relativePath, aboveDir.infos('fullpath))) {
        Core log s"[CommitProcessor] -- [Error] '" + aboveDir.infos('fullpath) + s"' isn't the directory above '$relativePath'."
        (false, s"'" + aboveDir.infos('fullpath) + s"' isn't the directory above '$relativePath'. Something strange is going on.")
      }
      else {
        //addressing
        val version = 1 //constant for dir
        val timestamp = System.currentTimeMillis
        val addressingHashInput = getAddressingHashInput(relativePath, version, timestamp)
        val addressing = (crypto.bytes_2_b64(crypto.hash(addressingHashInput)), FieldStatus.Hash)
        psdbe.addressing = addressing
        lkdbe.addressing = addressing

        Core log "[CommitProcessor] -- addressing is \"" + lkdbe.addressing.getValue + "\""

        //symmetric crypt for name
        val k = crypto.newSymmetricKey(Config.aesKeySizeBits)
        val iv = crypto.newSymmetricKey(Config.aesKeySizeBits)
        psdbe.filename_path_version = (crypto.bytes_2_b64(crypto.symmetricEncrypt(addressingHashInput.getBytes("UTF-8"), k, iv)), FieldStatus.AES)
        psdbe.iv = crypto.bytes_2_b64(iv)

        //init the keys
        val KInit = crypto.newSignatureKey(Config.signKeySizeBits)
        val KCascadeRead = crypto.newAsymmetricKey(Config.signKeySizeBits)
        val KCascadeWrite = crypto.newAsymmetricKey(Config.signKeySizeBits)

        //store the public keys
        psdbe.KInitPubD = (crypto.publicKey_2_string(KInit.getPublic()), FieldStatus.RSA_Public_Key)
        psdbe.KCascadeReadPub = (crypto.publicKey_2_string(KCascadeRead.getPublic()), FieldStatus.RSA_Public_Key)
        psdbe.KCascadeWritePub = (crypto.publicKey_2_string(KCascadeWrite.getPublic()), FieldStatus.RSA_Public_Key)

        //store the private keys
        lkdbe.K = (crypto.bytes_2_b64(k), FieldStatus.AES_Key)
        lkdbe.KInitPriv = (crypto.privateKey_2_string(KInit.getPrivate()), FieldStatus.RSA_Private_Key)
        lkdbe.KCascadeReadPriv = (crypto.privateKey_2_string(KCascadeRead.getPrivate()), FieldStatus.RSA_Private_Key)
        lkdbe.KCascadeWritePriv = (crypto.privateKey_2_string(KCascadeWrite.getPrivate()), FieldStatus.RSA_Private_Key)

        //we sign the information
        def signInputDir(psdbe : PublicStructuralDirDatabaseEntry) : String =
          "[" + psdbe.addressing.getValue + "|" + psdbe.KInitPubD.getValue + "|" + psdbe.KCascadeReadPub.getValue + "|" + psdbe.KCascadeWritePub.getValue + "]"

        val signInput = signInputDir(psdbe)

        val publicKeyID = crypto.string_2_publicKey(Core.identity.getPublicKeyString)
        val privateKeyID = crypto.string_2_privateKey(Core.identity.getPrivateKeyString)

        val tag = crypto.sign(crypto.hash(signInput), privateKeyID)
        psdbe.sign_v0_tag = (crypto.bytes_2_b64(tag), FieldStatus.RSA)
        psdbe.sign_v0_publickey = (crypto.publicKey_2_string(publicKeyID), FieldStatus.RSA_Public_Key)

        //CCasc[Read/Write]
        if (acceptCascade && !aboveDir.infos('fullpath).equals(Config.unix_dir_separator)) {
          def isNoneOrEmpty(s : Option[String]) = s.isEmpty || s.get.equals("0")
          val aboveKeys = aboveDir.keys();

          if (isNoneOrEmpty(aboveKeys.get(KeyDefs.KCascReadPub)) || isNoneOrEmpty(aboveKeys.get(KeyDefs.KCascWritePub))) {
            Core log "[CommitProcessor] [Error] tried to apply Cascade on $relativePath, but either one of the cascading keys are null."
          }
          else {
            //keys
            val kCascReadPub = crypto.string_2_publicKey(aboveKeys.get(KeyDefs.KCascReadPub).get.getValue)
            val kCascWritePub = crypto.string_2_publicKey(aboveKeys.get(KeyDefs.KCascWritePub).get.getValue)

            //data for cascade are the key for the cascade below
            val dataForCCascK = lkdbe.K.getValue.getBytes("UTF-8")
            val dataForCCascRead = lkdbe.KCascadeReadPriv.getValue.getBytes("UTF-8")
            val dataForCCascWrite = lkdbe.KCascadeWritePriv.getValue.getBytes("UTF-8")

            val cCascK = crypto.asymmetricEncrypt(dataForCCascK, kCascReadPub)
            val (cipherData1, symmKeyCipher1, iv1) = crypto.hybridEncrypt(dataForCCascRead, kCascReadPub, Config.aesKeySizeBits)
            val (cipherData2, symmKeyCipher2, iv2) = crypto.hybridEncrypt(dataForCCascWrite, kCascWritePub, Config.aesKeySizeBits)

            List[Array[Byte]](cipherData1, iv1, symmKeyCipher1).foreach(f => Core log "[Error] " + crypto.bytes_2_b64(f))
            List[Array[Byte]](cipherData2, iv2, symmKeyCipher2).foreach(f => Core log "[Error] " + crypto.bytes_2_b64(f))

            def hybridToString(length : Int, data : Array[Byte]*) =
              (length + "|" + (data.map(bytes => crypto.bytes_2_b64(bytes)).mkString("|")))

            val cCascRead = hybridToString(dataForCCascWrite.length, cipherData1, symmKeyCipher1, iv1)
            val cCascWrite = hybridToString(dataForCCascWrite.length, cipherData2, symmKeyCipher2, iv2)

            psdbe.cCascK = (crypto.bytes_2_b64(cCascK), FieldStatus.RSA)
            psdbe.cCascRead = (cCascRead, FieldStatus.Hybrid)
            psdbe.cCascWrite = (cCascWrite, FieldStatus.Hybrid)
            Core log "[CommitProcessor] [Debug] Successfully applied hybrid encryption !"

            Core log "[CommitProcessor] [Info] for later comparison, dataForCCascRead is (" +
              dataForCCascRead.length + ")\n" + lkdbe.KCascadeReadPriv.getValue + "\n [Info] and dataForCCascWrite is (" +
              dataForCCascWrite.length + ")\n" + lkdbe.KCascadeWritePriv.getValue + ". End"

          }

        }

        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(aboveDir.infos('fullpath)))
        psdbe.hash_directory_path = (hashOfAboveDir, FieldStatus.Hash)

        remoteDb.insertEntry(psdbe)
        localDb.insertEntry(lkdbe)

        Core log "[CommitProcessor] -- Done."
        (true, "Success")
      }
    }

  /**
    * Commits a new file to the repository
    */
  def commitNewFile(relativePathInput : String) : Future[SuccessStatusWithMessage] =
    {
      Core log s"[CommitProcessor] Trying to commit new file '$relativePathInput'."

      val relativePathInputNormalized = PathNormalization.normalizeForInnerUse(relativePathInput, true, false)
      val aboveDirPath = PathNormalization.innerAbovePath(relativePathInputNormalized, false)

      Core log s"[CommitProcessor] -- relative path is '$relativePathInput'"
      Core log s"[CommitProcessor] -- relative path normalized is '$relativePathInputNormalized'"
      Core log s"[CommitProcessor] -- above dir path is '$aboveDirPath'"

      Core log s"[CommitProcessor] Querying above path to DiscoveryProcessor..."

      //checking if we have the right for the above dir (i.e. it was discovered) :
      val futureAboveDir = (Core.discoveryProcessor ? new DiscoverAbstractRepo(aboveDirPath)).mapTo[Option[AbstractDirectory]]

      val resultPromise = promise[SuccessStatusWithMessage]
      val resultFuture = resultPromise.future

      futureAboveDir.onFailure {
        case e : Exception =>
          Core log s"[CommitProcessor] [Error] Future DiscoverAbstractRepo($aboveDirPath) failed with reason $e."
          resultPromise success (false, s"Exception : $e")
      }

      futureAboveDir.onSuccess {
        case dirOption : Option[AbstractDirectory] => dirOption match {
          case Some(abstractDir) =>
            {
              val res = this.commitNewFileWithDirectory(relativePathInputNormalized, abstractDir, true)
              resultPromise success res
            }
          case None =>
            Core log s"[CommitProcessor] [Error] Future DiscoverAbstractRepo($aboveDirPath)" +
              s"lead None as an answer. Do you have rights on $aboveDirPath ?"
            resultPromise success (false, s"Cannot find the above directory '$aboveDirPath'; do you have RW rights on it ?")
        }
        case _ =>
          Core log s"[CommitProcessor] [Error] Future DiscoverAbstractRepo($aboveDirPath) didnt" +
            s"lead to an AbstractDirectory. Do you have rights on $aboveDirPath ?"
          resultPromise success (false, s"Cannot find the above directory '$aboveDirPath'; do you have RW rights on it ?")
      }

      resultFuture
    }

  /**
    * Commits a new file to the repository
    */
  def commitNewFileWithDirectory(relativePathInput : String, aboveDir : AbstractDirectory, acceptCascade : Boolean = true) : SuccessStatusWithMessage =
    {
      Core log s"[CommitProcessor] Trying to commit new file '$relativePathInput', above dir is '" + aboveDir.infos('fullpath) + "'"

      var psdbe = new PublicStructuralFileDatabaseEntry;
      var lkdbe = new LocalKeyFileDatabaseEntry
      psdbe.isDeletion = "no"

      val relativePathNormalized = PathNormalization.normalizeForInnerUse(relativePathInput, true, false)
      val aboveDirPath = PathNormalization.innerAbovePath(relativePathNormalized, false)

      Core log s"[CommitProcessor] -- relative path is '$relativePathInput'"
      Core log s"[CommitProcessor] -- absolute path normalized is '$relativePathNormalized'"
      Core log s"[CommitProcessor] -- above dir path is '$aboveDirPath'"

      val prevVersionExists = Await.result(ask(Core.discoveryProcessor, DiscoverPreviousVersion(relativePathNormalized)).mapTo[Option[AbstractFile]], Config.defaultTimeout)

      if (!prevVersionExists.isEmpty) {
        Core log s"[CommitProcessor] -- [Error] '$relativePathNormalized' already exists on repository."
        (false, s"'$relativePathNormalized' already exists on remote repository.")
      }
      else if (!repoSource.doesFileExists(relativePathNormalized)) {
        Core log s"[CommitProcessor] -- [Error] '$relativePathNormalized' doesnt exists"
        (false, s"'$relativePathNormalized' does not exist on source repository.")
      }
      else if (aboveDir.isKeyEmptyOrZero(KeyDefs.KInitPriv_D) && !aboveDir.infos('fullpath).equals(Config.unix_dir_separator)) {
        Core log s"[CommitProcessor] -- [Error] new file inside '" + aboveDir.infos('fullpath) + "' : you don't have KInitPriv_F, do you have rights for this directory ?."
        (false, s"This is a new file inside '" + aboveDir.infos('fullpath) + "', but you don't have KInitPriv. Do you have W right on the directory ?")
      }
      //TODO : use repo to manage files
      else if (!FileToolBox.fileDirectlyInside(relativePathNormalized, aboveDir.infos('fullpath))) {
        Core log s"[CommitProcessor] -- [Error] '" + aboveDir.infos('fullpath) + s"' isn't the directory above '$relativePathNormalized'."
        (false, s"'" + aboveDir.infos('fullpath) + s"' isn't the directory above '$relativePathNormalized'. Something strange is going on.")
      }
      else {
        //addressing
        val version = 1
        val timestamp = System.currentTimeMillis

        val addressingHashInput = getAddressingHashInput(relativePathNormalized, version, timestamp)
        psdbe.addressing = (crypto.bytes_2_b64(crypto.hash(addressingHashInput)), FieldStatus.Hash)
        lkdbe.addressing = psdbe.addressing

        Core log "[CommitProcessor] -- addressing is \"" + lkdbe.addressing.getValue + "\""

        //symmetric file encrypt
        val k = crypto.newSymmetricKey(Config.aesKeySizeBits)
        val iv = crypto.newSymmetricKey(Config.aesKeySizeBits)
        crypto.symmetricEncrypt(repoSource.getInputStreamForFile(relativePathNormalized), repoRemote.getOutputStreamForFile(Config.unix_dir_separator + psdbe.addressing.getValue), k, iv)

        lkdbe.K = (crypto.bytes_2_b64(k), FieldStatus.AES_Key)
        psdbe.iv = crypto.bytes_2_b64(iv)
        psdbe.filename_path_version = (crypto.bytes_2_b64(crypto.symmetricEncrypt(addressingHashInput.getBytes("UTF-8"), k, iv)), FieldStatus.AES)

        //we compute the hash of our file
        val fileHash = crypto.hash(repoRemote.getInputStreamForFile(Config.unix_dir_separator + psdbe.addressing.getValue))
        val signInput = addressingHashInput + crypto.bytes_2_b64(fileHash)

        //signature V \in N
        val signKey = crypto.newSignatureKey(Config.signKeySizeBits)
        val tag_kwrite = crypto.sign(crypto.hash(signInput), signKey.getPrivate())
        psdbe.KWritePub = (crypto.publicKey_2_string(signKey.getPublic()), FieldStatus.RSA_Public_Key)
        lkdbe.KWritePriv = (crypto.privateKey_2_string(signKey.getPrivate()), FieldStatus.RSA_Private_Key)
        psdbe.kwritepub_tag = (crypto.bytes_2_b64(tag_kwrite), FieldStatus.RSA)

        //signature Owner
        val publicKeyID = crypto.string_2_publicKey(Core.identity.getPublicKeyString)
        val privateKeyID = crypto.string_2_privateKey(Core.identity.getPrivateKeyString)
        val tag_owner = crypto.sign(crypto.hash(signInput), privateKeyID)
        psdbe.sign_vN_tag = (crypto.bytes_2_b64(tag_owner), FieldStatus.RSA)
        psdbe.sign_vN_publickey = (crypto.publicKey_2_string(publicKeyID), FieldStatus.RSA_Public_Key)

        //CCasc[Read/Write]
        if (acceptCascade && !aboveDir.infos('fullpath).equals(Config.unix_dir_separator)) {
          def isNoneOrEmpty(s : Option[String]) = s.isEmpty || s.get.equals("0")
          val aboveKeys = aboveDir.keys();

          if (isNoneOrEmpty(aboveKeys.get(KeyDefs.KCascReadPub)) || isNoneOrEmpty(aboveKeys.get(KeyDefs.KCascWritePub))) {
            Core log "[CommitProcessor] [Error] tried to apply Cascade on $relativePath, but either one of the cascading keys are null."
          }
          else {
            //keys
            val kCascReadPub = crypto.string_2_publicKey(aboveKeys.get(KeyDefs.KCascReadPub).get.getValue)
            val kCascWritePub = crypto.string_2_publicKey(aboveKeys.get(KeyDefs.KCascWritePub).get.getValue)

            val dataForCCascRead = k
            val dataForCCascWrite = lkdbe.KWritePriv.getValue.getBytes("UTF-8")

            val cCascRead = crypto.asymmetricEncrypt(dataForCCascRead, kCascReadPub)

            val (cipherData, symmKeyCipher, iv) = crypto.hybridEncrypt(dataForCCascWrite, kCascWritePub, Config.aesKeySizeBits)

            def hybridToString(length : Int, data : Array[Byte]*) =
              (length + "|" + (data.map(bytes => crypto.bytes_2_b64(bytes)).mkString("|")))

            val cCascWrite = hybridToString(dataForCCascWrite.length, cipherData, symmKeyCipher, iv)

            psdbe.cCascRead = (crypto.bytes_2_b64(cCascRead), FieldStatus.Hybrid)
            psdbe.cCascWrite = (cCascWrite, FieldStatus.Hybrid)
            Core log "[CommitProcessor] [Debug] Successfully applied hybrid encryption !"

          }

        }

        //hash of above directory
        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(aboveDir.infos('fullpath)))
        psdbe.hash_directory_path = (hashOfAboveDir, FieldStatus.Hash)

        //hash of previous version (we don't use it, but we don't want to publicly display that this is a new version. so : random)
        psdbe.hash_version_one = crypto.hashB64(Randomness.randomAlphanumericString(100))

        //signature V = 0
        if (aboveDir.infos('fullpath).equals(Config.unix_dir_separator)) {
          //only exception : in ROOT DIR, not needed
          Core log "[CommitProcessor] -- signV0 : root dir, skipping."

          psdbe.sign_v0_tag = ("0", FieldStatus.RSA)
          psdbe.sign_v0_publickey = ("0", FieldStatus.RSA_Public_Key)

          remoteDb.insertEntry(psdbe)
          localDb.insertEntry(lkdbe)

          Core log "[CommitProcessor] -- Done."
          (true, "Success")
        }
        else {
          val KDInitPubStr = aboveDir.keys.get(KeyDefs.KInitPub_D)
          val KDInitPrivStr = aboveDir.keys.get(KeyDefs.KInitPriv_D)
          Core log s"[CommitProcessor] -- gonna signV0, keys are $KDInitPubStr and $KDInitPrivStr."

          if (KDInitPubStr.isEmpty || KDInitPrivStr.isEmpty) {
            Core log s"[CommitProcessor] -- tried to signV0, but at least one of the key is empty"
            (true, "Impossible to sign with above directory's write key KInitPub and KInitPriv. One of those key does not exist. Do you have W right on the above directory '" + aboveDir.infos('fullpath) + "'")

          }
          else {
            val KDInitPub = crypto.string_2_publicKey(KDInitPubStr.get)
            val KDInitPriv = crypto.string_2_privateKey(KDInitPrivStr.get)

            val signInputDir = signInput + hashOfAboveDir
            val tag = crypto.sign(crypto.hash(signInputDir), KDInitPriv)
            psdbe.sign_v0_tag = (crypto.bytes_2_b64(tag), FieldStatus.RSA)
            psdbe.sign_v0_publickey = (KDInitPubStr.get, FieldStatus.RSA_Public_Key)

            remoteDb.insertEntry(psdbe)
            localDb.insertEntry(lkdbe)

            Core log "[CommitProcessor] -- Done."
            (true, "Success")

          }
        }
      }
    }

  /**
    * Commits a new version of a file to the repository. Must specify the previous version.
    */
  def commitNewVersion(relativePathInput : String, previousVersion : AbstractFile) : SuccessStatusWithMessage =
    {
      Core log s"[CommitProcessor] Trying to commit new version of file '$relativePathInput'"

      var psdbe = new PublicStructuralFileDatabaseEntry;
      var lkdbe = new LocalKeyFileDatabaseEntry
      psdbe.isDeletion = "no"

      val relativePathNormalized = PathNormalization.normalizeForInnerUse(relativePathInput, true, false)

      Core log s"[CommitProcessor] -- relative path normalized is '$relativePathNormalized'"

      if (!repoSource.doesFileExists(relativePathNormalized)) {
        Core log s"[CommitProcessor] -- [Error] '$relativePathNormalized' doesnt exists"
        (false, s"'$relativePathNormalized' does not exist in source repository.")
      }
      else if (!relativePathNormalized.equals(previousVersion.infos('fullpath))) {
        Core log s"[CommitProcessor] -- [Error] '$relativePathNormalized' isn't a new version for " + previousVersion.infos('fullpath) + "."
        (false, s"'$relativePathNormalized' isn't a new version for " + previousVersion.infos('fullpath) + ".")
      }
      else if (previousVersion.keys().get(KeyDefs.KWritePriv_F).get.trim().equals("")) {
        Core log s"[CommitProcessor] -- [Error] new version of '$relativePathNormalized' : you don't have KWritePriv_F, do you have rights for this ?."
        (false, s"This is a new version of '$relativePathNormalized', but you don't have KWritePriv from previous version. Do you have RW right ?")
      }
      else {
        //addressing
        val version = previousVersion.version.toInt + 1
        val timestamp = System.currentTimeMillis
        val addressingHashInput = getAddressingHashInput(relativePathNormalized, version, timestamp)
        psdbe.addressing = (crypto.bytes_2_b64(crypto.hash(addressingHashInput)), FieldStatus.Hash)
        lkdbe.addressing = psdbe.addressing

        Core log "[CommitProcessor] -- addressing is \"" + lkdbe.addressing.getValue + "\""

        //symmetric file encrypt
        val keySizeBits = Config.aesKeySizeBits
        val k = crypto.b64_2_bytes(previousVersion.keys().get(KeyDefs.K_F).get);
        val iv = crypto.newSymmetricKey(keySizeBits)
        crypto.symmetricEncrypt(repoSource.getInputStreamForFile(relativePathNormalized), repoRemote.getOutputStreamForFile(Config.unix_dir_separator + psdbe.addressing.getValue), k, iv)

        lkdbe.K = (crypto.bytes_2_b64(k), FieldStatus.AES_Key)
        psdbe.iv = crypto.bytes_2_b64(iv)
        psdbe.filename_path_version = (crypto.bytes_2_b64(crypto.symmetricEncrypt(addressingHashInput.getBytes("UTF-8"), k, iv)), FieldStatus.AES)

        //we compute the hash of our file
        val fileHash = crypto.hash(repoRemote.getInputStreamForFile(Config.unix_dir_separator + psdbe.addressing.getValue))
        val signInput = addressingHashInput + crypto.bytes_2_b64(fileHash)

        //hash of above directory
        psdbe.hash_directory_path = previousVersion.publicDatabaseEntry.hash_directory_path

        //hash of previous version
        psdbe.hash_version_one = crypto.hashB64(previousVersion.infos.get('fullpath).get)

        //we use the same previous keys, so the same previous cCasc
        psdbe.cCascRead = previousVersion.publicDatabaseEntry.cCascRead
        psdbe.cCascWrite = previousVersion.publicDatabaseEntry.cCascWrite

        //signature Owner
        val publicKeyID = crypto.string_2_publicKey(Core.identity.getPublicKeyString)
        val privateKeyID = crypto.string_2_privateKey(Core.identity.getPrivateKeyString)
        val tag_owner = crypto.sign(crypto.hash(signInput), privateKeyID)
        psdbe.sign_vN_tag = (crypto.bytes_2_b64(tag_owner), FieldStatus.RSA)
        psdbe.sign_vN_publickey = (crypto.publicKey_2_string(publicKeyID), FieldStatus.RSA_Public_Key)

        //signature V \in N
        val pubKStr = previousVersion.keys().get(KeyDefs.KWritePub_F).get
        val privKStr = previousVersion.keys().get(KeyDefs.KWritePriv_F).get
        val pubK = crypto.string_2_publicKey(pubKStr)
        val privK = crypto.string_2_privateKey(privKStr)
        val tag_kwrite = crypto.sign(crypto.hash(signInput), privK)
        psdbe.KWritePub = (pubKStr, FieldStatus.RSA_Public_Key)
        lkdbe.KWritePriv = (privKStr, FieldStatus.RSA_Private_Key)
        psdbe.kwritepub_tag = (crypto.bytes_2_b64(tag_kwrite), FieldStatus.RSA)

        remoteDb.insertEntry(psdbe)
        localDb.insertEntry(lkdbe)

        Core log "[CommitProcessor] -- Done."
        (true, "Success")
      }
    }

  /**
    * Commits a new deletion (that stops the history line) to the repository. Must specify the previous version of that file.
    */
  def commitDeletion(relativePathInput : String, previousVersion : AbstractFile) : SuccessStatusWithMessage =
    {
      Core log s"[CommitProcessor] Trying to commit deletion of file '$relativePathInput'"

      var psdbe = new PublicStructuralFileDatabaseEntry;
      var lkdbe = new LocalKeyFileDatabaseEntry
      psdbe.isDeletion = "yes"
      val relativePathNormalized = PathNormalization.normalizeForInnerUse(relativePathInput, true, false)

      Core log s"[CommitProcessor] -- relative normalized path is '$relativePathNormalized'"

      if (!relativePathNormalized.equals(previousVersion.infos('fullpath))) {
        Core log s"[CommitProcessor] -- [Error] '$relativePathNormalized' isn't a new deletion for " + previousVersion.infos('fullpath) + "."
        (false, s"'$relativePathNormalized' isn't a deletion for " + previousVersion.infos('fullpath) + ".")
      }
      else if (previousVersion.keys().get(KeyDefs.KWritePriv_F).get.trim().equals("")) {
        Core log s"[CommitProcessor] -- [Error] new version of '$relativePathNormalized' : you don't have KWritePriv_F, do you have rights for this ?."
        (false, s"This is a deletion of '$relativePathNormalized', but you don't have KWritePriv from previous version. Do you have RW right ?")
      }
      else {
        //addressing
        val version = previousVersion.version.toInt + 1
        val timestamp = System.currentTimeMillis
        val addressingHashInput = getAddressingHashInput(relativePathNormalized, version, timestamp)
        psdbe.addressing = (crypto.bytes_2_b64(crypto.hash(addressingHashInput)), FieldStatus.Hash)
        lkdbe.addressing = psdbe.addressing

        Core log "[CommitProcessor] -- addressing is \"" + lkdbe.addressing.getValue + "\""

        //symmetric file encrypt
        val keySizeBits = Config.aesKeySizeBits
        val k = crypto.b64_2_bytes(previousVersion.keys().get(KeyDefs.K_F).get);
        val iv = crypto.newSymmetricKey(keySizeBits)

        lkdbe.K = (crypto.bytes_2_b64(k), FieldStatus.AES_Key)
        psdbe.iv = crypto.bytes_2_b64(iv)
        psdbe.filename_path_version = (crypto.bytes_2_b64(crypto.symmetricEncrypt(addressingHashInput.getBytes("UTF-8"), k, iv)), FieldStatus.AES)

        //hash of above directory
        psdbe.hash_directory_path = previousVersion.publicDatabaseEntry.hash_directory_path

        //hash of previous version
        psdbe.hash_version_one = crypto.hashB64(previousVersion.infos.get('fullpath).get)

        //we use the same previous keys, so the same previous cCasc
        psdbe.cCascRead = previousVersion.publicDatabaseEntry.cCascRead
        psdbe.cCascWrite = previousVersion.publicDatabaseEntry.cCascWrite

        //we compute the hash of our file
        val signInput = addressingHashInput

        //signature Owner
        val publicKeyID = crypto.string_2_publicKey(Core.identity.getPublicKeyString)
        val privateKeyID = crypto.string_2_privateKey(Core.identity.getPrivateKeyString)
        val tag_owner = crypto.sign(crypto.hash(signInput), privateKeyID)
        psdbe.sign_vN_tag = (crypto.bytes_2_b64(tag_owner), FieldStatus.RSA)
        psdbe.sign_vN_publickey = (crypto.publicKey_2_string(publicKeyID), FieldStatus.RSA_Public_Key)

        //signature V \in N
        val pubKStr = previousVersion.keys().get(KeyDefs.KWritePub_F).get
        val privKStr = previousVersion.keys().get(KeyDefs.KWritePriv_F).get
        val pubK = crypto.string_2_publicKey(pubKStr)
        val privK = crypto.string_2_privateKey(privKStr)
        val tag_kwrite = crypto.sign(crypto.hash(signInput), privK)
        psdbe.kwritepub_tag = (crypto.bytes_2_b64(tag_kwrite), FieldStatus.RSA)
        psdbe.KWritePub = (pubKStr, FieldStatus.RSA_Public_Key)
        lkdbe.KWritePriv = (privKStr, FieldStatus.RSA_Private_Key)

        remoteDb.insertEntry(psdbe)
        localDb.insertEntry(lkdbe)

        Core log "[CommitProcessor] -- Done."
        (true, "Success")
      }
    }

  /*
   *     Identities management
   * ###########################################################
   */

  /**
    * Commits a new asymmetric key pair bound to the claimedIdentity toward the public repository.
    * Stores the private key in the local repository.
    */
  def commitNewIdentity(claimedIdentity : String) : SuccessStatusWithMessage =
    {
      def pcName() = {
        var hostname = "Unknown";
        var reason = "OK"

        try {
          hostname = java.net.InetAddress.getLocalHost().getHostName()
        }
        catch {
          case e : Exception => reason = s"[Operation Failed] $e."
        }
        (hostname, reason)
      }

      val pidbe = new PublicIdentityDatabaseEntry
      val timestamp = System.currentTimeMillis().toString

      pidbe.owner = claimedIdentity
      pidbe.published_on = timestamp
      val pcinfo = pcName
      pidbe.pcName = pcinfo._1
      pidbe.pcNameReason = pcinfo._2

      val signInput = "[" + claimedIdentity + "||" + timestamp + "||" + pcinfo._1 + "||" + pcinfo._2 + "]"
      val signKey = crypto.newSignatureKey(Config.signKeySizeBits)

      pidbe.addressing = (crypto.bytes_2_b64(crypto.hash(signInput)), FieldStatus.Hash)

      val tag = crypto.sign(crypto.hash(signInput), signKey.getPrivate())
      pidbe.signature = (crypto.bytes_2_b64(tag), FieldStatus.RSA)
      pidbe.public_key = (crypto.publicKey_2_string(signKey.getPublic()), FieldStatus.RSA_Public_Key)

      //important : write public without the private key !
      pidbe.private_key = ("", FieldStatus.RSA_Private_Key)
      assert(pidbe.private_key.getValue.equals(""))
      remoteDb.insertEntry(pidbe)

      pidbe.private_key = (crypto.privateKey_2_string(signKey.getPrivate()), FieldStatus.RSA_Private_Key)

      //now write with private key
      (localDb.insertEntry(pidbe), "This is the result of the database insertion operation. If negative, it might be a collision on the filesystem.")

    }

  /**
    * Accepts a public identity, i.e. copies the public record in the local database
    */
  def acceptPublicIdentity(addressing : String) : SuccessStatusWithMessage =
    {
      val idOption = remoteDb.getEntriesPIDB.filter(_.addressing.getValue.equals(addressing)).headOption

      if (idOption.isEmpty) {
        (false, s"Trying to accept an identity ($addressing), but it does no longer exists")
      }
      {
        val id = idOption.get
        //should be already null
        id.private_key = ("", FieldStatus.RSA_Private_Key)
        (localDb.insertEntry(id), "This is the result of the database insertion operation. If negative, it might be a collision on the filesystem.")
      }
    }

  /**
    * Reject a public identity, i.e. deletes the public record in the local database
    */
  def rejectPublicIdentity(addressing : String) : SuccessStatusWithMessage =
    {
      val idOption = localDb.getEntriesPIDB.filter(_.addressing.getValue.equals(addressing)).headOption

      if (idOption.isEmpty) {
        (false, s"Trying to delete an identity ($addressing), but it does no longer exists")
      }
      {
        (localDb.deleteEntry(idOption.get), "This is the result of the database deletion operation. If negative, it might be a lock in the filesystem.")
      }
    }

  /*
   *     Messaging
   * ###########################################################
   */

  /**
    * Sub function for message sending
    */
  def subDoSend(plainText : String, plainAddr : String, plainType : String, desc : String, keys : String, toId : PublicIdentityDatabaseEntry) {

    val plainData = plainText.getBytes("UTF-8");
    val pk = crypto.string_2_publicKey(toId.public_key.getValue)

    Core log "[Debug] Gonna encrypt : " + plainText

    val cipherData = crypto.hybridEncrypt(plainData, pk, Config.aesKeySizeBits)

    val message = new PublicMessageDatabaseEntry()
    message.from = (Core.identity.getPublicKeyString, FieldStatus.RSA_Public_Key)
    message.to = toId.public_key
    message.contentName = (plainAddr, FieldStatus.Plain)
    message.contentType = (plainType, FieldStatus.Plain)
    message.contentDesc = (desc, FieldStatus.Plain)
    message.contentKeys = (keys, FieldStatus.Plain)
    message.timestamp = (System.currentTimeMillis.toString, FieldStatus.Plain)
    message.iv = crypto.bytes_2_b64(cipherData._3)
    message.contentLength = plainData.length.toString
    message.cipherAsymm = crypto.bytes_2_b64(cipherData._2)
    message.cipherSymm = crypto.bytes_2_b64(cipherData._1)
    message.addressing = crypto.bytes_2_b64(crypto.hash(cipherData._1))

    //sign
    val privateKey = crypto.string_2_privateKey(Core.identity.getPrivateKeyString)
    val cipher = crypto.sign(crypto.hash(message.toString()), privateKey)
    message.sign_tag = (crypto.bytes_2_b64(cipher), FieldStatus.RSA)
    message.sign_pk = (Core.identity.getPublicKeyString, FieldStatus.RSA_Public_Key)

    remoteDb.insertEntry(message)
  }

  /**
    * Send a message containing a directory key to a given identity, here identified by the addressing of the public record.
    * The key is also identified by is addressing, for instance in lkddbe_XXX, XXX is the addressing.
    * Rights follow this format : (Read, Write, CascRead, CascWrite)
    * - Write implies Read
    * - CascRead implies Read
    * - CascWrite implies Write (which implies Read)
    */
  def sendDirKey(toAddressing : String, keyAddr : String, rights : (Boolean, Boolean, Boolean, Boolean)) : SuccessStatusWithMessage =
    {
      Core log s"[CommitProcessor] Gonna send dir key $keyAddr to $keyAddr, rights=$rights."
      val keyOpt = localDb.getEntryLKDBD(keyAddr)
      val toIdOption = remoteDb.getEntryPIDB(toAddressing)
      if (keyOpt.isEmpty) {
        Core log s"[CommitProcessor] [Error] Tried to send key $keyAddr, but it doesn't exists."
        (false, s"Trying to send a key ($keyAddr) but it no longer exists")
      }
      else if (!toIdOption.isDefined) {
        Core log s"[CommitProcessor] [Error] Tried to send a key to $toAddressing, no public id found."
        (false, s"Trying to send a key to someone, but his identity ($toAddressing) no longer exists")
      }
      else {
        val toId = toIdOption.get
        val key = keyOpt.get

        Core log s"[DiscoveryProcessor] [Info] $key."
        val xml = key.toXml

        val allKeysToStrip = scala.collection.mutable.ArrayBuffer.empty[String]
        if (!rights._1 && !rights._2 && !rights._3 && !rights._4) allKeysToStrip += "k"
        if (!rights._2 && !rights._4) allKeysToStrip += "kinitpriv"
        if (!rights._3 && !rights._4) allKeysToStrip += "kcascadereadpriv"
        if (!rights._4) allKeysToStrip += "kcascadewritepriv"

        val allFiles = Await.result(ask(Core.discoveryProcessor, DiscoverAllReadableFiles).mapTo[DiscoveredAllFiles], Config.defaultTimeout)
        val desc = { allFiles.directories filter { d => d.infos('addressing).equals(keyAddr) } map { d => d.infos('fullpath) } headOption } getOrElse "No information."
        val keys = (if (rights._1) "-r-" else "") +
          (if (rights._2) "-r-w-" else "") +
          (if (rights._3) "-r-cr-" else "") +
          (if (rights._4) "-r-w-cr-cw-" else "")

        //strip the W key if needed
        val plainText = XMLToolBox.compactString(XMLToolBox.multiStripTag(xml, allKeysToStrip.toArray))

        //send
        subDoSend(plainText, keyAddr, "lkddbe", desc, keys, toId)

        Core log s"[CommitProcessor] [Success] Key $keyAddr sent to $toAddressing, rights=$rights."

        (true, "Success")
      }
    }

  /**
    * Send a message containing a file key to a given identity, here identified by the addressing of the public record.
    * The key is also identified by is addressing, for instance in lkfdbe_XXX, XXX is the addressing.
    * Rights follow this format : (!Write)
    * - Write implies Read.
    * => if boolean is true, Read. if false, Read + Write.
    */
  def sendFileKey(toAddressing : String, keyAddr : String, onlyR : Boolean) : SuccessStatusWithMessage =
    {
      Core log s"[CommitProcessor] Gonna send file key $keyAddr to $keyAddr, onlyR= $onlyR."
      val keyOpt = localDb.getEntryLKFBD(keyAddr)
      val toIdOption = remoteDb.getEntryPIDB(toAddressing)
      if (keyOpt.isEmpty) {
        Core log s"[CommitProcessor] [Error] Tried to send key $keyAddr, but it doesn't exists."
        (false, s"Trying to send a key ($keyAddr) but it no longer exists")
      }
      else if (!toIdOption.isDefined) {
        Core log s"[CommitProcessor] [Error] Tried to send a key to $toAddressing, no public id found."
        (false, s"Trying to send a key to someone, but his identity ($toAddressing) no longer exists")
      }
      else {
        val toId = toIdOption.get
        val key = keyOpt.get

        val xml = key.toXml

        //strip the W key if needed
        val plainText = XMLToolBox.compactString(
          if (onlyR) XMLToolBox.stripTag(xml, "kwritepriv")
          else xml
        )

        val allFiles = Await.result(ask(Core.discoveryProcessor, DiscoverAllReadableFiles).mapTo[DiscoveredAllFiles], Config.defaultTimeout)
        val desc = { allFiles.files filter { d => d.infos('addressing).equals(keyAddr) } map { d => d.infos('fullpath) } headOption } getOrElse "No information."
        val keys = if (onlyR) "-r-" else "-r-w-"

        //send
        subDoSend(plainText, keyAddr, "lkfdbe", desc, keys, toId)

        Core log s"[CommitProcessor] [Success] Key $keyAddr sent to $toAddressing, onlyR= $onlyR."

        (true, "Success")
      }
    }

  /**
    * Deletes a message (the record in the public database)
    */
  def deleteMessage(msgAddr : String) : SuccessStatusWithMessage =
    {
      val msgOption = remoteDb.getEntryPMDB(msgAddr)

      if (msgOption.isEmpty) {
        Core log s"[CommitProcessor] tried to delete message $msgAddr, but can't find it."
        (false, s"Trying to send a key ($msgAddr) but it no longer exists")

      }
      else {
        (remoteDb.deleteEntry(msgOption.get), "This is the result of the database insertion operation. If negative, it might be a collision on the filesystem.")
      }
    }
}
