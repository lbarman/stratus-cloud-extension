package main.scala.processors

import scala.Option.option2Iterable
import scala.concurrent.Await

import akka.actor.{ Actor, actorRef2Scala }
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{ Config, Core }
import main.scala.common.DBToolBox
import main.scala.crypto.{ AbstractCryptoPrimitives, KeyDefs }
import main.scala.db.engine.{ PublicStructuralDirDatabaseEntry, PublicStructuralFileDatabaseEntry }
import main.scala.db.engine.AbstractDatabase
import main.scala.db.engine.DatabaseField.DatabaseField2String
import main.scala.filesystem.{ AbstractDirectory, AbstractFile }
import main.scala.filesystem.repo.AbstractRepo

/*
 * Input/Output commands
 */
case class HarvestTopDown(rootPath : String)
case object HarvestNewFiles

/**
  * Used to discover new keys
  * - from cascading from a directory
  * - for new version of files we own
  */
class KeyHarvesterProcessor(repoSource : AbstractRepo, repoRemote : AbstractRepo,
                            localDb : AbstractDatabase, remoteDb : AbstractDatabase, crypto : AbstractCryptoPrimitives) extends Actor {

  implicit val timeout = new Timeout(Config.defaultTimeout)

  def receive = {

    case d : HarvestTopDown =>
      sender ! this.harvestTopDown(d.rootPath)

    case HarvestNewFiles =>
      sender ! this.harvestLaterallyAllFiles()
  }

  def decryptAes(data : String, k : String, iv : String) =
    {
      if (data.trim().equals(""))
        ""
      else
        new String(crypto.symmetricDecrypt(crypto.b64_2_bytes(data), crypto.b64_2_bytes(k), crypto.b64_2_bytes(iv))).trim()
    }

  /**
    * Discover new versions of all the files for which we have the rights
    */
  def harvestLaterallyAllFiles() =
    {
      Core log s"[DiscoveryProcessor] Starting to harvest new versions for all files. "

      val firstVersions = Await.result(ask(Core.discoveryProcessor, DiscoverAllFirstVersionOfFiles).mapTo[Set[AbstractFile]], Config.defaultTimeout)

      var count = 0;
      firstVersions foreach { f =>
        count += harvestLaterally(f)
      }

      count
    }

  /**
    * Discover new versions for a file
    */
  def harvestLaterally(file : AbstractFile) =
    {
      Core log s"[DiscoveryProcessor] Gonna harvest new versions for file " + file.infos.get('fullpath).get + ". "

      val hashOfV1 = crypto.hashB64(file.infos.get('fullpath).get)
      val addressing = file.infos.get('addressing).get
      val KF = file.keys().get(KeyDefs.K_F) getOrElse ""
      val KWritePriv = file.keys().get(KeyDefs.KWritePriv_F) getOrElse ""

      Core log s"[DiscoveryProcessor] -- looking for files with hashV1 " + hashOfV1 + ". "

      val allFileEntries = remoteDb.getEntriesPSFDB()
      val hashSetAllFileEntries = allFileEntries.toSet[PublicStructuralFileDatabaseEntry];

      val fileEntriesOfThisFile = hashSetAllFileEntries.filter(e => e.hash_version_one.getValue.equals(hashOfV1))

      var count = 0;

      fileEntriesOfThisFile foreach { f =>

        Core log s"[DiscoveryProcessor] -- discovered new version " + f.addressing.getValue + " KF=" + KF + " KWritePriv=" + KWritePriv
        count += DBToolBox.addAndMergeFileKey(localDb, f.addressing.getValue, KF, KWritePriv)
      }

      count
    }

  def harvestTopDown(rootPath : String) =
    {
      def fileLookup(aboveDirHash : String) =
        {
          val allFileEntries = remoteDb.getEntriesPSFDB()
          val hashSetAllFileEntries = allFileEntries.toSet[PublicStructuralFileDatabaseEntry];

          val fileEntriesInThisDir = hashSetAllFileEntries.filter(e => e.hash_directory_path.getValue.equals(aboveDirHash))

          fileEntriesInThisDir
        }

      def dirLookup(aboveDirHash : String) =
        {
          val allDirEntries = remoteDb.getEntriesPSDDB()
          val hashSetAllDirEntries = allDirEntries.toSet[PublicStructuralDirDatabaseEntry];

          val dirEntriesInThisDir = hashSetAllDirEntries.filter(e => e.hash_directory_path.getValue.equals(aboveDirHash))

          dirEntriesInThisDir
        }

      def getReadKeysForFile(file : PublicStructuralFileDatabaseEntry, KCascReadPriv : String) =
        {
          if (file.cCascRead.getValue.isEmpty() || file.cCascRead.getValue.equals("0")) {
            Core log s"[DiscoveryProcessor] [Error] file " + file.addressing.getValue + " has an invalid cCascRead=" + file.cCascRead + " ! "
            None
          }
          else {
            val pk = crypto.string_2_privateKey(KCascReadPriv)
            val k_f = crypto.asymmetricDecrypt(crypto.b64_2_bytes(file.cCascRead.getValue), pk)
            Core log s"[DiscoveryProcessor] [Info] file " + file.addressing.getValue +
              " discovered K_F=" + crypto.bytes_2_b64(k_f) + " ! "
            Some(crypto.bytes_2_b64(k_f))
          }
        }

      def getWriteKeysForFile(file : PublicStructuralFileDatabaseEntry, kCascWritePrivStr : String) =
        {
          if (file.cCascWrite.getValue.count(c => c.equals('|')) != 3) {
            Core log s"[DiscoveryProcessor] [Error] file " + file.addressing.getValue + " has an invalid cCascWrite=" + file.cCascWrite + " ! "
            None
          }
          else {

            val Array(lengthStr, cipherDataStr, symmKeyCipherStr, ivStr) = file.cCascWrite.getValue.split('|')
            val length = lengthStr.toInt
            val cipherData = crypto.b64_2_bytes(cipherDataStr)
            val symmKeyCipher = crypto.b64_2_bytes(symmKeyCipherStr)
            val iv = crypto.b64_2_bytes(ivStr)

            val kCascReadPriv = crypto.string_2_privateKey(kCascWritePrivStr)
            val decryptedKeyByte = crypto.hybridDecrypt(cipherData, length, iv, symmKeyCipher, kCascReadPriv)
            val decryptedKey = new String(decryptedKeyByte)

            Core log s"[DiscoveryProcessor] [Info] file " + file.addressing.getValue +
              " discovered KWritePriv_F=" + decryptedKey + " ! "

            Some(decryptedKey)
          }
        }

      def getCascKKeysForDir(dir : PublicStructuralDirDatabaseEntry, kCascReadPrivStr : String) =
        {
          if (dir.cCascK.getValue.isEmpty() || dir.cCascK.getValue.equals("0")) {
            Core log s"[DiscoveryProcessor] [Error] dir " + dir.addressing.getValue + " has an invalid cCascK=" + dir.cCascK + " ! "
            None
          }
          else {
            val pk = crypto.string_2_privateKey(kCascReadPrivStr)
            val k_d_bytes = crypto.asymmetricDecrypt(crypto.b64_2_bytes(dir.cCascK.getValue), pk)
            val k_d = new String(k_d_bytes)
            Core log s"[DiscoveryProcessor] [Info] dir " + dir.addressing.getValue +
              " discovered K_D=" + k_d + " ! "
            val filename_path_version = decryptAes(dir.filename_path_version, k_d, dir.iv)

            val parts = filename_path_version.replaceAllLiterally("[", "").replaceAllLiterally("]", "").trim().split('|')
            val filename = parts.head
            Some(dir.addressing.getValue, k_d, filename)
          }
        }

      def getCascReadKeyForDir(dir : PublicStructuralDirDatabaseEntry, kCascReadPrivStr : String) =
        {
          if (dir.cCascRead.getValue.count(c => c.equals('|')) != 3) {
            Core log s"[DiscoveryProcessor] [Error] dir " + dir.addressing.getValue + " has an invalid cCascRead=" + dir.cCascRead + " ! "
            None
          }
          else {

            val Array(lengthStr, cipherDataStr, symmKeyCipherStr, ivStr) = dir.cCascRead.getValue.split('|')
            val length = lengthStr.toInt
            val cipherData = crypto.b64_2_bytes(cipherDataStr)
            val symmKeyCipher = crypto.b64_2_bytes(symmKeyCipherStr)
            val iv = crypto.b64_2_bytes(ivStr)

            val kCascReadPriv = crypto.string_2_privateKey(kCascReadPrivStr)

            val decryptedKeyByte = crypto.hybridDecrypt(cipherData, length, iv, symmKeyCipher, kCascReadPriv)
            val decryptedKey = new String(decryptedKeyByte)

            Core log s"[DiscoveryProcessor] [Info] dir " + dir.addressing.getValue +
              " discovered KCascReadPriv_D=" + decryptedKey + " ! "

            Some(decryptedKey)
          }
        }

      def getCascWriteKeyForDir(dir : PublicStructuralDirDatabaseEntry, kCascWritePrivStr : String) =
        {
          if (dir.cCascWrite.getValue.count(c => c.equals('|')) != 3) {
            Core log s"[DiscoveryProcessor] [Error] dir " + dir.addressing.getValue + " has an invalid cCascRead=" + dir.cCascRead + " ! "
            None
          }
          else {

            val Array(lengthStr, cipherDataStr, symmKeyCipherStr, ivStr) = dir.cCascWrite.getValue.split('|')
            val length = lengthStr.toInt
            val cipherData = crypto.b64_2_bytes(cipherDataStr)
            val symmKeyCipher = crypto.b64_2_bytes(symmKeyCipherStr)
            val iv = crypto.b64_2_bytes(ivStr)

            val kCascWritePriv = crypto.string_2_privateKey(kCascWritePrivStr)

            val decryptedKeyByte = crypto.hybridDecrypt(cipherData, length, iv, symmKeyCipher, kCascWritePriv)
            val decryptedKey = new String(decryptedKeyByte)

            Core log s"[DiscoveryProcessor] [Info] dir " + dir.addressing.getValue +
              " discovered KCascWritePriv_D=" + decryptedKey + " ! "

            Some(decryptedKey)
          }
        }

      def getFileReadKeysForDir(root : AbstractDirectory) = {
        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(root.infos('fullpath)))
        val privateKeyOption = root.keys().get(KeyDefs.KCascReadPriv)

        if (privateKeyOption.isEmpty || privateKeyOption.get.isEmpty()) {
          Core log s"[DiscoveryProcessor] [Error] directory $rootPath doesn't have a KCascReadPriv key"
          Set()
        }
        else {
          val privateKey = privateKeyOption.get;
          Core log s"[DiscoveryProcessor] -- found directory $rootPath, now looking for files with hash=$hashOfAboveDir."

          val entries = fileLookup(hashOfAboveDir)
          val discoveredReadKey = entries map (f => {
            val keyOpt = getReadKeysForFile(f, privateKey)
            keyOpt match {
              case Some(s : String) =>
                DBToolBox.addAndMergeFileKey(localDb, f.addressing.getValue, s, "")
                Some(f.addressing.getValue)
              case None => None
            }
          }) flatten

          discoveredReadKey
        }
      }

      def getFileWriteKeysForDir(root : AbstractDirectory) = {
        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(root.infos('fullpath)))
        val privateKeyOption = root.keys().get(KeyDefs.KCascWritePriv)

        if (privateKeyOption.isEmpty || privateKeyOption.get.isEmpty()) {
          Core log s"[DiscoveryProcessor] [Info] directory $rootPath doesn't have a KCascWritePriv key"
          Set()
        }
        else {
          val privateKey = privateKeyOption.get;
          Core log s"[DiscoveryProcessor] -- found directory $rootPath, now looking for files with hash=$hashOfAboveDir."

          val entries = fileLookup(hashOfAboveDir)
          val discoveredWriteKey = entries map (f => {
            val keyOpt = getWriteKeysForFile(f, privateKey)
            keyOpt match {
              case Some(s : String) =>
                DBToolBox.addAndMergeFileKey(localDb, f.addressing.getValue, "", s)
                Some(f.addressing.getValue)
              case None => None
            }
          }) flatten

          discoveredWriteKey
        }
      }

      def getDirCascadingKKeysForDir(root : AbstractDirectory) = {
        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(root.infos('fullpath)))
        val privateKeyOption = root.keys().get(KeyDefs.KCascReadPriv)

        if (privateKeyOption.isEmpty || privateKeyOption.get.isEmpty()) {
          Core log s"[DiscoveryProcessor] [Info] directory $rootPath doesn't have a KCascReadPriv key"
          Set[(String, String, String)]()
        }
        else {
          val privateKey = privateKeyOption.get;
          Core log s"[DiscoveryProcessor] -- found directory $rootPath, now looking for dir with hash=$hashOfAboveDir."

          val entries = dirLookup(hashOfAboveDir)
          val discoveredCascKKey = entries map (d => {
            val keyOpt = getCascKKeysForDir(d, privateKey)
            keyOpt match {
              case Some(s : (String, String, String)) =>
                DBToolBox.addAndMergeDirKey(localDb, d.addressing.getValue, s._2, "", "", "")
                Some(d.addressing.getValue, s._2, s._3)
              case None => None
            }
          }) flatten

          discoveredCascKKey
        }
      }

      def getDirCascadingReadKeysForDir(root : AbstractDirectory) = {
        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(root.infos('fullpath)))
        val privateKeyOption = root.keys().get(KeyDefs.KCascReadPriv)

        if (privateKeyOption.isEmpty || privateKeyOption.get.isEmpty()) {
          Core log s"[DiscoveryProcessor] [Info] directory $rootPath doesn't have a KCascReadPriv key"
          Set()
        }
        else {
          val privateKey = privateKeyOption.get;
          Core log s"[DiscoveryProcessor] -- found directory $rootPath, now looking for dir with hash=$hashOfAboveDir."

          val entries = dirLookup(hashOfAboveDir)
          val discoveredCascReadKey = entries map (d => {
            val keyOpt = getCascReadKeyForDir(d, privateKey)
            keyOpt match {
              case Some(s : String) =>
                DBToolBox.addAndMergeDirKey(localDb, d.addressing.getValue, "", "", s, "")
                Some(d.addressing.getValue)
              case None => None
            }
          }) flatten

          discoveredCascReadKey
        }
      }

      def getDirCascadingWriteKeysForDir(root : AbstractDirectory) = {
        val hashOfAboveDir = crypto.bytes_2_b64(crypto.hash(root.infos('fullpath)))
        val privateKeyOption = root.keys().get(KeyDefs.KCascWritePriv)

        if (privateKeyOption.isEmpty || privateKeyOption.get.isEmpty()) {
          Core log s"[DiscoveryProcessor] [Info] directory $rootPath doesn't have a KCascWritePriv key"
          Set()
        }
        else {
          val privateKey = privateKeyOption.get;
          Core log s"[DiscoveryProcessor] -- found directory $rootPath, now looking for dir with hash=$hashOfAboveDir."

          val entries = dirLookup(hashOfAboveDir)
          val discoveredWriteReadKey = entries map (d => {
            val keyOpt = getCascWriteKeyForDir(d, privateKey)
            keyOpt match {
              case Some(s : String) =>
                DBToolBox.addAndMergeDirKey(localDb, d.addressing.getValue, "", "", "", s)
                Some(d.addressing.getValue)
              case None => None
            }
          }) flatten

          discoveredWriteReadKey
        }
      }

      def initRecursion(rootPath : String) : (Set[String], Set[String]) = {
        Core log "[DiscoveryProcessor] Init local key harvesting in " + rootPath + "."

        val rootOption = Await.result(ask(Core.discoveryProcessor, new DiscoverAbstractRepo(rootPath)).mapTo[Option[AbstractDirectory]], Config.defaultTimeout)

        if (rootOption.isEmpty) {
          Core log s"[DiscoveryProcessor] [Error] directory $rootPath doesn't exists, do you have rights ?"
          (Set(), Set())
        }
        else {
          val root = rootOption.get
          val discoveredReadKey = getFileReadKeysForDir(root)
          val discoveredWriteKey = getFileWriteKeysForDir(root)
          val discoveredCascKKey = getDirCascadingKKeysForDir(root)
          val discoveredCascReadKey = getDirCascadingReadKeysForDir(root)
          val discoveredCascWriteKey = getDirCascadingWriteKeysForDir(root)

          val n1 = discoveredReadKey.count(p => true) + discoveredWriteKey.count(p => true)
          val n2 = discoveredCascReadKey.count(p => true) + discoveredCascWriteKey.count(p => true) //cascK# = cascRead#
          Core log "[DiscoveryProcessor] [Success] key harvesting finished, discovered " + n1 + "file keys, " + n2 + " dir keys."

          //recursion
          val hashOfThisDir = crypto.bytes_2_b64(crypto.hash(root.infos('fullpath)))
          val dirs = dirLookup(hashOfThisDir)

          var discoverReadKeyAccu = scala.collection.mutable.Set[String]()
          var discoverWriteKeyAccu = scala.collection.mutable.Set[String]()

          discoverReadKeyAccu ++= discoveredReadKey
          discoverReadKeyAccu ++= discoveredCascReadKey
          discoverWriteKeyAccu ++= discoveredWriteKey
          discoverWriteKeyAccu ++= discoveredCascWriteKey

          discoveredCascKKey.foreach(d => {
            val (newRead, newWrite) = initRecursion(d._3)
            discoverReadKeyAccu ++= newRead
            discoverWriteKeyAccu ++= newWrite
          })

          (discoverReadKeyAccu.toSet, discoverWriteKeyAccu.toSet)
        }
      }

      initRecursion(rootPath)

    }

}