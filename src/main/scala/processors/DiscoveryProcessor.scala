package main.scala.processors

import scala.Array.canBuildFrom
import scala.Option.option2Iterable
import scala.xml.XML

import akka.actor.{Actor, actorRef2Scala}
import main.scala.{Config, Core}
import main.scala.common.{DBToolBox, PathNormalization}
import main.scala.crypto.AbstractCryptoPrimitives
import main.scala.crypto.AbstractIdentity.VerifiedIdentity
import main.scala.crypto.KeyDefs
import main.scala.crypto.KeyDefs.KeyDefs
import main.scala.db.engine.{LocalKeyDirDatabaseEntry, LocalKeyFileDatabaseEntry, PublicMessageDatabaseEntry, PublicStructuralDirDatabaseEntry, PublicStructuralFileDatabaseEntry}
import main.scala.db.engine.AbstractDatabase
import main.scala.db.engine.DatabaseField.{DatabaseField2String, String2DatabaseField}
import main.scala.filesystem.{AbstractDirectory, AbstractFile}
import main.scala.filesystem.repo.AbstractRepo

/*
 * Input/Output commands
 */
case object DiscoverCurrentIdentity
case object DiscoverLocalKeys
case object DiscoverMessages
case object DiscoverPublicKeys
case object DiscoverPublicIdentities
case object DiscoverAllFirstVersionOfFiles
case object DiscoverMyIdentities
case object DiscoverLocallyStoredPublicIdentities
case object DiscoverAllReadableFiles
case class DiscoverAndDecryptMessage(addr : String)
case class DiscoverLocalFiles(path : String)
case class DiscoverAbstractRepo(path : String)
case class DiscoverIfHasBeenDeleted(path : String)
case class DiscoverPreviousVersion(path : String)
case class DiscoverFirstVersion(path : String)
case class DecryptFile(pathIn : String, pathOut : String)
case class DiscoverIdentity(publicKey : String)

class DiscoveredAllFiles {
  var files : scala.collection.immutable.Set[main.scala.filesystem.AbstractFile] = null;
  var directories : scala.collection.immutable.Set[main.scala.filesystem.AbstractDirectory] = null;
}

class DiscoveredPublicKeys {
  var filesKeys : Array[PublicStructuralFileDatabaseEntry] = null;
  var directoriesKeys : Array[PublicStructuralDirDatabaseEntry] = null;
}

class DiscoveredLocalKeys {
  var filesKeys : Array[LocalKeyFileDatabaseEntry] = null;
  var directoriesKeys : Array[LocalKeyDirDatabaseEntry] = null;
}

/**
  * Used to read and discover information about the repositories and the databases. It includes :
  * - reading all files (new files/new versions/deletions) and directories entries
  * - reading all keys
  * - doing the matching
  * - checking all signatures
  * In additions, it also performs :
  * - file decryption
  * - finding a specific version of a file, directory
  * - checking if a given file history contains a deletion
  * - listing public, accepted identities
  * - read a decrypt messages
  */
class DiscoveryProcessor(repoSource : AbstractRepo, repoRemote : AbstractRepo,
                         localDb : AbstractDatabase, remoteDb : AbstractDatabase,
                         crypto : AbstractCryptoPrimitives) extends Actor {

  private var lastDiscoveredFiles : Set[AbstractFile] = null
  private var lastDiscoveredDirs : Set[AbstractDirectory] = null

  def receive = {

    /*
     * Keys discovery
     */
    case DiscoverLocalKeys => {
      val v = new DiscoveredLocalKeys
      val r = this.getLocalKeys
      v.filesKeys = r._1
      v.directoriesKeys = r._2
      sender ! v
    }

    /*
     * Global files and directory discovery
     */
    case DiscoverPublicKeys => {
      val v = new DiscoveredPublicKeys
      val r = this.getPublicKeys
      v.filesKeys = r._1
      v.directoriesKeys = r._2
      sender ! v
    }

    case DiscoverAllReadableFiles => {
      val v = new DiscoveredAllFiles
      val r = this.getAllReadableFilesInRepo
      v.files = r._1
      v.directories = r._2
      sender ! v
    }

    case DiscoverAllFirstVersionOfFiles => {
      sender ! this.getAllFirstVersionOfFiles()
    }

    case d : DiscoverLocalFiles =>
      val path = PathNormalization.normalizeForInnerUse(d.path, true, true)
      refreshBufferedReableFilesInRepo
      refreshBufferedReableDirsInRepo
      sender ! this.getLocalFiles(path)

    case d : DiscoverAbstractRepo =>
      sender ! this.getAbstractDirectory(d.path)

    case d : DiscoverPreviousVersion =>
      sender ! this.getLastVersionForPath(d.path)

    case d : DiscoverFirstVersion =>
      sender ! this.getFirstVersionForPath(d.path)

    case d : DiscoverIfHasBeenDeleted =>
      sender ! this.getIfHasBeenDeletedPath(d.path)

    /*
     * File decryption 
     */
    case d : DecryptFile =>
      sender ! this.performDecryption(d.pathIn, d.pathOut)

    /*
     * Identities
     */
    case DiscoverPublicIdentities =>
      sender ! this.getPublicIdentities

    case DiscoverMyIdentities =>
      sender ! this.getMyIdentities

    case DiscoverLocallyStoredPublicIdentities =>
      sender ! this.getLocallyStoredPublicIdentities

    case DiscoverCurrentIdentity =>
      sender ! this.getCurrentIdentity()

    case d : DiscoverIdentity =>
      sender ! resolveIdentity(d.publicKey)

    /*
     * Messages
     */
    case DiscoverMessages =>
      sender ! this.getMyMessages()

    case c : DiscoverAndDecryptMessage =>
      sender ! this.decryptAndStoreMessage(c.addr)
  }

  //Used to buffer the abstract contents of the repos
  def getBufferedReadableFilesInRepo = {
    if (lastDiscoveredFiles == null)
      refreshBufferedReableFilesInRepo
    lastDiscoveredFiles
  }
  def getBufferedReadableDirsInRepo = {
    if (lastDiscoveredDirs == null)
      refreshBufferedReableDirsInRepo
    lastDiscoveredDirs
  }

  //refresh the buffers
  def refreshBufferedReableFilesInRepo = { lastDiscoveredFiles = getAllReadableFilesInRepo()._1 }
  def refreshBufferedReableDirsInRepo = { lastDiscoveredDirs = getAllReadableFilesInRepo()._2 }

  /**
    * Decrypts a string encrypted with AES, or return empty if empty
    */
  def decryptAes(data : String, k : String, iv : String) =
    {
      if (data.trim().equals(""))
        ""
      else
        new String(crypto.symmetricDecrypt(crypto.b64_2_bytes(data), crypto.b64_2_bytes(k), crypto.b64_2_bytes(iv))).trim()
    }

  /*
   *     Global file and directory discovery
   * ###########################################################
   */

  /**
    * Reads all public file and directory structural entries, all local file and directory keys, does the matching, checks the signatures, and instantiates a collection of
    * AbstractFile and AbstractDirectory
    */
  def getAllReadableFilesInRepo() =
    {

      /**
        * Dual signature check : Owner signature, and Version\inN signature (knowledge of KWritePriv)
        */
      def signVerifyFileVNAndOwner(addressing : String, addressingNonHashed : String, tagVn : String, publicKeyVn : String, tagOwner : String, publicKeyOwner : String, fileContentsHash : String) =
        {
          val hashSignInput = crypto.hash(addressingNonHashed + fileContentsHash)

          val s0 = crypto.verifySign(hashSignInput, crypto.b64_2_bytes(tagVn), crypto.string_2_publicKey(publicKeyVn))
          val s1 = crypto.verifySign(hashSignInput, crypto.b64_2_bytes(tagOwner), crypto.string_2_publicKey(publicKeyOwner))

          (s0 & s1)
        }

      /**
        * Single signature check : Containing' directory check for Version=0 (knowledge of KInitPriv)
        * Exception : always TRUE if file is in ROOT /
        */
      def signVerifyFileV0(addressing : String, addressingNonHashed : String, tag : String, publicKey : String, filepath_version_author : String, hashOfAboveDir : String, fileContentsHash : String) =
        {
          def isInRoot = {
            val parts = filepath_version_author.replaceAllLiterally("[", "").replaceAllLiterally("]", "").trim().split('|')
            val inRoot = parts.head.indexOf(Config.unix_dir_separator) == 0 && parts.head.lastIndexOf(Config.unix_dir_separator) == 0
            inRoot
          }

          if (isInRoot)
            true
          else {
            val signInput = addressingNonHashed + addressingNonHashed + fileContentsHash + hashOfAboveDir
            crypto.verifySign(crypto.hash(signInput), crypto.b64_2_bytes(tag), crypto.string_2_publicKey(publicKey))
          }
        }

      /**
        * Special file signature verification in case of deletion (the cipher file doesn't exists, only the record)
        */
      def signVerifyDeletion(addressingNonHashed : String, tag : String, publicKey : String) =
        {
          val signInput = addressingNonHashed
          crypto.verifySign(crypto.hash(signInput), crypto.b64_2_bytes(tag), crypto.string_2_publicKey(publicKey))
        }

      /**
        * Signature check for directories (record only)
        */
      def signVerifyDir(addressing : String, kInitPubD : String, kCascadeReadPub : String, kCascadeWritePub : String, tag : String, publicKey : String) =
        {
          val signInput = "[" + addressing + "|" + kInitPubD + "|" + kCascadeReadPub + "|" + kCascadeWritePub + "]"
          crypto.verifySign(crypto.hash(signInput), crypto.b64_2_bytes(tag), crypto.string_2_publicKey(publicKey))
        }

      /**
        * Transform a tuple (Structural info, Keys) into an AbstractFile, provided all information and signatures are correct.
        */
      def processFile(x : Tuple2[PublicStructuralFileDatabaseEntry, LocalKeyFileDatabaseEntry]) : Option[AbstractFile] =
        {
          val filepath_version_author = decryptAes(x._1.filename_path_version, x._2.K, x._1.iv)

          val version = filepath_version_author.split('|')(1).toInt

          //check signature, if invalid, ignore
          val res = if (x._1.isDeletion.getValue.equals("yes")) {
            val s0 = signVerifyDeletion(filepath_version_author, x._1.sign_vN_tag, x._1.sign_vN_publickey)
            val s1 = signVerifyDeletion(filepath_version_author, x._1.kwritepub_tag, x._1.KWritePub)
            s0 && s1
          }
          else if (version > 0) {
            val fileHash = crypto.hashB64(repoRemote.getInputStreamForFile(Config.unix_dir_separator + x._1.addressing.getValue))
            signVerifyFileVNAndOwner(x._1.addressing, filepath_version_author, x._1.sign_vN_tag, x._1.sign_vN_publickey, x._1.kwritepub_tag, x._1.KWritePub, fileHash)
          }
          else {
            val fileHash = crypto.hashB64(repoRemote.getInputStreamForFile(Config.unix_dir_separator + x._1.addressing.getValue))
            val s1 = signVerifyFileVNAndOwner(x._1.addressing, filepath_version_author, x._1.sign_vN_tag, x._1.sign_vN_publickey, x._1.kwritepub_tag, x._1.KWritePub, fileHash)
            val s2 = signVerifyFileV0(x._1.addressing, filepath_version_author, x._1.sign_v0_tag, x._1.sign_v0_publickey, filepath_version_author, x._1.hash_directory_path.getValue, fileHash)
            (s1 && s2)
          }

          if (res) {
            val metadata = decryptAes(x._1.metadata_aes, x._2.K, x._1.iv) + x._1.metadata_plain
            var keyMap = Map[KeyDefs, String]()
            keyMap += (KeyDefs.K_F -> x._2.K)
            keyMap += (KeyDefs.IV_F -> x._1.iv)
            keyMap += (KeyDefs.KWritePub_F -> x._1.KWritePub)
            keyMap += (KeyDefs.KWritePriv_F -> x._2.KWritePriv)
            val addr_filepaht_v_auth = x._1.addressing + "|" + filepath_version_author
            var resolvedIdentity = this.resolveIdentity(x._1.sign_vN_publickey)
            val abstractFile = new AbstractFile(addr_filepaht_v_auth, metadata, keyMap, x._1, x._2, resolvedIdentity, (x._1.isDeletion.getValue.equals("yes")))

            Core log "[Debug] " + x._1.KWritePub
            Core log "[Debug] instanciated file" + abstractFile.infos('addressing) + " with pubk " + abstractFile.keys().get(KeyDefs.KWritePub_F)
            Some(abstractFile)
          }
          else {
            Core log "[DiscoverProcessor] [Error] file " + x._1.addressing + " has an invalid signature !"
            None
          }
        }

      /**
        * Transform a tuple (Structural info, Keys) into an AbstractDirectory, provided all information and signatures are correct.
        */
      def processDir(x : Tuple2[PublicStructuralDirDatabaseEntry, LocalKeyDirDatabaseEntry]) : Option[AbstractDirectory] =
        {
          if (!x._2.K.getValue.isEmpty()) {
            val dirpath_timestamp_version_author = decryptAes(x._1.filename_path_version, x._2.K, x._1.iv)

            val res = signVerifyDir(x._1.addressing, x._1.KInitPubD, x._1.KCascadeReadPub, x._1.KCascadeWritePub, x._1.sign_v0_tag, x._1.sign_v0_publickey)
            if (res) {
              var keyMap = Map[KeyDefs, String]()
              keyMap += (KeyDefs.K_D -> x._2.K)
              keyMap += (KeyDefs.KInitPub_D -> x._1.KInitPubD)
              keyMap += (KeyDefs.KInitPriv_D -> x._2.KInitPriv)
              keyMap += (KeyDefs.KCascReadPub -> x._1.KCascadeReadPub)
              keyMap += (KeyDefs.KCascReadPriv -> x._2.KCascadeReadPriv)
              keyMap += (KeyDefs.KCascWritePub -> x._1.KCascadeWritePub)
              keyMap += (KeyDefs.KCascWritePriv -> x._2.KCascadeWritePriv)

              //TODO factor AbstractDir, etc
              var resolvedIdentity = this.resolveIdentity(x._1.sign_v0_publickey)
              val abstractDir = new AbstractDirectory(x._1.addressing + "|" + dirpath_timestamp_version_author, keyMap, x._1, x._2, resolvedIdentity)
              Some(abstractDir)
            }
            else {
              Core log "[DiscoverProcessor] [Error] dir " + x._1.addressing + " has an invalid signature !"
              None
            }
          }
          else
            None
        }

      /*
       * CORPUS
       */

      val availableFileKeys = localDb.getEntriesLKFDB()
      val availableDirKeys = localDb.getEntriesLKDDB()
      val allFileEntries = remoteDb.getEntriesPSFDB()
      val allDirEntries = remoteDb.getEntriesPSDDB()

      //do the mapping
      val fileEntriesWithKey = for (file <- allFileEntries; key <- availableFileKeys; if file == key) yield (file, key)
      val dirEntriesWithKey = for (dir <- allDirEntries; key <- availableDirKeys; if dir == key) yield (dir, key)

      val abstractFiles = (fileEntriesWithKey.map(x => processFile(x))).flatten.toSet
      val abstractDirs = (dirEntriesWithKey.map(x => processDir(x))).flatten.toSet +
        rootDirectory; //we add Root directory as we always have rights on it

      //buffer these lists
      lastDiscoveredFiles = abstractFiles
      lastDiscoveredDirs = abstractDirs

      (abstractFiles, abstractDirs)
    }

  /**
    * Root directory is a common record for everyone
    */
  def rootDirectory = {
    val addressing = Config.unix_dir_separator;
    val fullPath = Config.unix_dir_separator;
    val version = 0;
    val timestamp = 0;
    val identity = "0";

    var keyMap = Map[KeyDefs, String]()
    keyMap += (KeyDefs.K_D -> "0")
    keyMap += (KeyDefs.KInitPub_D -> "0")
    keyMap += (KeyDefs.KInitPriv_D -> "0")
    keyMap += (KeyDefs.KCascReadPub -> "0")
    keyMap += (KeyDefs.KCascReadPriv -> "0")
    keyMap += (KeyDefs.KCascWritePub -> "0")
    keyMap += (KeyDefs.KCascWritePriv -> "0")

    var privDatabaseEntry = new LocalKeyDirDatabaseEntry
    privDatabaseEntry.addressing = "ROOT"
    privDatabaseEntry.K = "0"
    privDatabaseEntry.KCascadeReadPriv = "0"
    privDatabaseEntry.KCascadeWritePriv = "0"
    privDatabaseEntry.KInitPriv = "0"

    var pubDatabaseEntry = new PublicStructuralDirDatabaseEntry
    pubDatabaseEntry.addressing = privDatabaseEntry.addressing
    pubDatabaseEntry.sign_v0_publickey = "0"
    pubDatabaseEntry.sign_v0_tag = "0"
    pubDatabaseEntry.cCascK = "0"
    pubDatabaseEntry.cCascRead = "0"
    pubDatabaseEntry.cCascWrite = "0"
    pubDatabaseEntry.KCascadeReadPub = "0"
    pubDatabaseEntry.KCascadeWritePub = "0"
    pubDatabaseEntry.KInitPubD = "0"

    //TODO add directory signature keys
    val strInfo = addressing + "|[" + fullPath + "|" + version + "|" + timestamp + "|" + identity + "]"
    new AbstractDirectory(strInfo, keyMap, pubDatabaseEntry, privDatabaseEntry, Some("0", "ROOT"))
  }

  /**
    * Finds the AbstractDirectory matching the path given
    */
  def getAbstractDirectory(path : String) : Option[AbstractDirectory] = getAbstractDirectory(path, getAllReadableFilesInRepo._2)

  def getAbstractDirectory(path : String, abstractDirs : Set[AbstractDirectory]) : Option[AbstractDirectory] =
    abstractDirs.filter(_.infos('fullpath).equals(path)).headOption

  /**
    * Finds the AbstractFile that is the very first version for a path
    */
  def getFirstVersionForPath(path : String) : Option[AbstractFile] = getFirstVersionForPath(path, getAllReadableFilesInRepo._1)

  def getFirstVersionForPath(path : String, abstractFiles : Set[AbstractFile]) : Option[AbstractFile] =
    {
      Core log s"[DiscoveryProcessor] Looking for first version of $path."
      var v1 : Option[AbstractFile] = None

      for (file <- abstractFiles) {
        if (file.infos('fullpath).equals(path) && file.infos('version).toInt == 1) {
          v1 = Some(file)
        }
      }
      v1
    }

  /**
    * Finds the AbstractFile that is the latest version for a path
    */
  def getLastVersionForPath(path : String) : Option[AbstractFile] = getLastVersionForPath(path, getAllReadableFilesInRepo._1)

  def getLastVersionForPath(path : String, abstractFiles : Set[AbstractFile]) : Option[AbstractFile] =
    {
      Core log s"[DiscoveryProcessor] Looking for previous version of $path."
      var max = -1;
      var maxFile : Option[AbstractFile] = None

      for (file <- abstractFiles) {
        if (file.infos('fullpath).equals(path) && file.infos('version).toInt > max) {
          max = file.infos('version).toInt;
          maxFile = Some(file)
        }
      }
      maxFile
    }

  /**
    * Returns a set with one AbstractFile for each distinct historyline (one version of each file)
    */
  def getAllFirstVersionOfFiles() =
    {
      val files = getAllReadableFilesInRepo._1
      files filter { f => f.infos('version).toInt == 1 } toSet
    }

  /**
    * Gets if a deletion was commited on a given history line.
    */
  def getIfHasBeenDeletedPath(path : String) : Boolean = getIfHasBeenDeletedPath(path, getAllReadableFilesInRepo._1)

  def getIfHasBeenDeletedPath(path : String, abstractFiles : Set[AbstractFile]) : Boolean =
    {
      Core log s"[DiscoveryProcessor] Looking for a potential deletion of $path."
      var deleted = false;

      for (file <- abstractFiles) {
        if (file.infos('fullpath).equals(path) && file.isDeletion) {
          deleted = true
          Core log s"[DiscoveryProcessor] -- found a deletion of $path, " + file + "."
        }
      }
      deleted
    }

  /**
    * Gets the local (non-encrypted) files for a given folder in the source repository
    */
  def getLocalFiles(relativeInnerPath : String) =
    {
      val files = repoSource.getFilesInDir(relativeInnerPath)
      files.map(f => {
        (f,
          this.getAbstractDirectory(f, lastDiscoveredDirs).isDefined,
          this.getLastVersionForPath(f, lastDiscoveredFiles).isDefined,
          getIfHasBeenDeletedPath(f, lastDiscoveredFiles))
      })
    }

  /*
   *     Decrypts a file towards an address in the source repository
   * ###########################################################
   */

  def performDecryption(addressingInput : String, relativeDestination : String) : Boolean =
    {
      Core log s"[DiscoveryProcessor] Trying to decrypt file addressing='$addressingInput', towards destination '$relativeDestination'."

      val abstractFiles = getAllReadableFilesInRepo._1.filter(f => f.infos('addressing).equals(addressingInput))
      val fileOption = abstractFiles.headOption

      if (!fileOption.isDefined) {
        Core log s"[DiscoveryProcessor] -- File not found. Do you have rights on $addressingInput ?"
        false
      }
      else {
        val file = fileOption.get

        val k = crypto.b64_2_bytes(file.keys().get(KeyDefs.K_F).get)
        val iv = crypto.b64_2_bytes(file.keys().get(KeyDefs.IV_F).get)

        val remotePath = PathNormalization.normalizeForInnerUse(file.infos('fullpath), true, false)

        def getTempName() = ("_decrypted_" + System.currentTimeMillis + "\\.")
        def replaceLast(s : String) = (a : String, b : String) => s.reverse.replaceFirst(a, b.reverse).reverse

        val destPath = if (relativeDestination.equals("*"))
          replaceLast(remotePath)("\\.", getTempName)
        else
          relativeDestination

        Core log s"[DiscoveryProcessor] -- gonna decrypt addressing='$addressingInput', towards destination '$destPath'."

        crypto.symmetricDecrypt(repoRemote.getInputStreamForFile(Config.unix_dir_separator + addressingInput), repoSource.getOutputStreamForFile(destPath), k, iv)
        Core log s"[DiscoveryProcessor] -- Done."
        true
      }
    }

  /*
   *     Records (local keys and public structural)
   * ###########################################################
   */

  def getLocalKeys() = (localDb.getEntriesLKFDB, localDb.getEntriesLKDDB)
  def getPublicKeys() = (remoteDb.getEntriesPSFDB, remoteDb.getEntriesPSDDB)

  /*
   *     Identities
   * ###########################################################
   */
  def getPublicIdentities() = remoteDb.getEntriesPIDB
  def getMyIdentities() = localDb.getEntriesPIDB.filterNot(e => e.private_key.getValue.isEmpty())
  def getLocallyStoredPublicIdentities() = localDb.getEntriesPIDB
  def getCurrentIdentity() = resolveIdentity(Core.identity.getPublicKeyString)

  /**
    * Matches a public key to an "owner String" if possible
    */
  def resolveIdentity(publicKey : String) =
    localDb.getEntriesPIDB filter { p => p.public_key.getValue.equals(publicKey) } map { e => (e.public_key.getValue, e.owner.getValue) } headOption

  /*
   *     Messages
   * ###########################################################
   */
  def getMyMessages() : Array[(PublicMessageDatabaseEntry, VerifiedIdentity, VerifiedIdentity)] = {

    def checkSign(m : PublicMessageDatabaseEntry) =
      {
        //sign
        val publicKey = crypto.string_2_publicKey(m.sign_pk.getValue)
        val input = crypto.hash(m.toString)
        val tag = crypto.b64_2_bytes(m.sign_tag.getValue)
        val res = crypto.verifySign(input, tag, publicKey)

        if (!res)
          Core log "[DiscoveryProcessor] [Error] Message " + m.getAddressing.getValue + " has an incorrect signature."
        res
      }

    val myIdsAddr = getMyIdentities map (p => p.public_key.getValue)
    val msg = remoteDb.getEntriesPMDB.filter(p => myIdsAddr.contains(p.to.getValue)) filter (p => checkSign(p))

    msg.sortBy(m => -m.timestamp.getValue.toLong) map (m => (m, resolveIdentity(m.from), resolveIdentity(m.to)))
  }

  /**
    * Decrypts a message, and stores the keys inside it in the local database
    */
  def decryptAndStoreMessage(msgAddr : String) =
    {
      val msgOption = remoteDb.getEntryPMDB(msgAddr)

      if (msgOption.isEmpty) {
        Core log s"[DiscoveryProcessor] tried to decrypt message $msgAddr, but can't find it."
        false
      }
      else {

        val msg = msgOption.get
        val idOption = getMyIdentities.filter(p => p.public_key == msg.to).headOption

        if (idOption.isEmpty) {
          Core log s"[DiscoveryProcessor] tried to decrypt message $msgAddr, but identity " + msg.to.getValue + " not found."
          false
        }
        else {
          val id = idOption.get

          val cipherAsymm = crypto.b64_2_bytes(msg.cipherAsymm.getValue)
          val cipherSymm = crypto.b64_2_bytes(msg.cipherSymm.getValue)
          val iv = crypto.b64_2_bytes(msg.iv.getValue)
          val dataLength = msg.contentLength.getValue.toInt

          val privateKey = crypto.string_2_privateKey(id.private_key.getValue)

          val plain = crypto.hybridDecrypt(cipherSymm, dataLength, iv, cipherAsymm, privateKey)
          val plainText = new String(plain);

          Core log s"[DiscoveryProcessor] [Debug] Decrypted message $msgAddr to text $plainText."

          msg.contentType.getValue match {
            case "lkfdbe" =>
              val e = LocalKeyFileDatabaseEntry.fromXml(XML.loadString(plainText))
              DBToolBox.addAndMergeFileKey(localDb, e)
              remoteDb.deleteEntry(msg)
              Core log s"[DiscoveryProcessor] [Success] Decrypted message $msgAddr to key $e."
              true
            case "lkddbe" =>
              val e = LocalKeyDirDatabaseEntry.fromXml(XML.loadString(plainText))
              DBToolBox.addAndMergeDirKey(localDb, e)
              remoteDb.deleteEntry(msg)
              Core log s"[DiscoveryProcessor] [Success] Decrypted message $msgAddr to key $e."
              true

            case _ =>
              Core log s"[DiscoveryProcessor] [Error] Decrypted message $msgAddr lead to unknown key type '" + msg.contentType.getValue + "'"
              false

          }
        }
      }
    }
}