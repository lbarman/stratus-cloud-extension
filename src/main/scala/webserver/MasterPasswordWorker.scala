package main.scala.webserver

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class MasterPasswordWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def receive = {
    case (event : HttpRequestEvent, action : String) =>
      {
        action match {
          case "clean-unprotected" =>
            {

              Core log s"[MasterPasswordWorker] Cleaning all unprotected keys."
              val n = Core.masterPasswordManager.deleteSensitiveInfo()
              val status = Core.masterPasswordManager.fileStatus()

              WebTemplate.rawAnswer(event, s"<p>[Success] OK, $n files deleted.</p>" +
                "<p>Current status of local key database : " + status._1 +
                " unprotected sensitive information, and " + status._2 + " protected information.</p>")
              context.stop(self)
            }
          case "status" =>
            {

              val n = Core.masterPasswordManager.fileStatus()
              WebTemplate.rawAnswer(event, s"<p>[Success] There is " + n._1 + " unprotected sensitive information, and " + n._2 + " protected information.</p>")
              context.stop(self)
            }

        }
      }

    case (event : HttpRequestEvent, action : String, password : String) =>

      {
        action match {

          case "encrypt-with" =>
            {
              Core log s"[MasterPasswordWorker] Encrypting with $password."
              val n = Core.masterPasswordManager.encryptAllKeysInSourceDb(password)
              val status = Core.masterPasswordManager.fileStatus()

              WebTemplate.rawAnswer(event, s"<p>[Success] OK, $n files encrypted.</p>" +
                "<p>Current status of local key database : " + status._1 +
                " unprotected sensitive information, and " + status._2 + " protected information.</p>")
              context.stop(self)
            }

          case "decrypt-with" =>
            {
              Core log s"[MasterPasswordWorker] Decrypting with $password."
              val n = Core.masterPasswordManager.decryptAllKeysInSourceDb(password)
              val status = Core.masterPasswordManager.fileStatus()

              WebTemplate.rawAnswer(event, s"<p>[Success] OK, $n files decrypted.</p>" +
                "<p>Current status of local key database : " + status._1 +
                " unprotected sensitive information, and " + status._2 + " protected information.</p>")
              context.stop(self)
            }

          case "test-password" =>
            {

              Core log s"[MasterPasswordWorker] Testing decryption with $password."
              val res = if (Core.masterPasswordManager.testKey(password))
                "<span class=\"successC\">correct</span>"
              else
                "<span class=\"failureC\">incorrect</span>"

              WebTemplate.rawAnswer(event, s"[Success] Test finished, password <em>$password</em> seems $res.")
              context.stop(self)
            }
        }
      }

    case event : HttpRequestEvent =>
      {
        val body = WebTemplate.createHtml(Map[String, String](), "masterpwd.template.html")

        val params = ("Master Password Manager", "<i class=\"fa fa-lock\"></i> Master Password Manager", "Protect your local keys", body)
        val html = WebTemplate.createHtml(params)

        event.response.contentType = "text/html"
        event.response.write(html)
        context.stop(self)
      }
  }
}