package main.scala.webserver

import org.mashupbots.socko.events.{HttpRequestEvent, HttpResponseStatus}

import akka.actor.Actor
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.common.FileToolBox

/**
  * Hello processor writes a greeting and stops.
  */
class ResourcesWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  lazy val defaultTemplate = "resources" + Config.osFileSeparator
  def resourcePath(resourceName : String) = defaultTemplate + resourceName

  def receive = {
    case (event : HttpRequestEvent, resource_name : String) =>
      {
        val file = resourcePath(resource_name);
        if (FileToolBox.fileExists(file)) {

          val ext = resource_name.substring(resource_name.lastIndexOf(".") + 1).toLowerCase

          ext match {
            case "js"   => event.response.contentType = "application/javascript"
            case "png"  => event.response.contentType = "image/png"
            case "jpg"  => event.response.contentType = "image/jpeg"
            case "jpeg" => event.response.contentType = "image/jpeg"
            case "css"  => event.response.contentType = "text/css"
            case _      => event.response.contentType = "application/octet-stream"
          }

          val contents = FileToolBox.readAllFrom(file)
          event.response.write(contents._2)
          context.stop(self)
        }
        else {

          Core log s"[ResourcesWorker] [Error] can't find resource $file. "
          event.response.write(HttpResponseStatus.NOT_FOUND)
          context.stop(self)

        }

      }
  }
}