package main.scala.webserver

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.Core.SuccessStatusWithMessage
import main.scala.Tests
import main.scala.common.PathNormalization
import main.scala.filesystem.{AbstractFile, WorkingCopyChangesWorkerDualChanges, WorkingCopyChangesWorkerGetAll}
import main.scala.processors.{CommitDeletionOfFile, CommitNewDir, CommitNewFile, CommitNewIdentity, CommitNewVersionOfFile, DiscoverIfHasBeenDeleted, DiscoverPreviousVersion, RejectPublicIdentity, SendKeyDir, SendKeyFile, StorePublicIdentity}
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class CommitWorker(headLess : Boolean) extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def answerWith(event : HttpRequestEvent, text : String) =
    {
      val params = ("Commit Processor", "<i class=\"fa fa-cloud-upload\"></i> Commit Processor", "Add, Delete, Change the repository", text)
      val html = WebTemplate.createHtml(params)

      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)
    }

  def receive = {

    case event : HttpRequestEvent =>
      val template = if (headLess) "commit.headless.template.html" else "commit.template.html"
      val body = WebTemplate.createHtml(Map[String, String](), template)

      val params = ("Commit Processor", "<i class=\"fa fa fa-cloud-upload\"></i> Commit Processor",
        "Send new files, new versions of files, deletions, and directories to the server.</span>", body)
      val html = WebTemplate.createHtml(params)
      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)

    case (event : HttpRequestEvent, action : String) =>
      {
        action match {
          case "repo-clean" =>
            {

              Core log "[CommitWorker] Cleaning the repositories... (all except source files)"
              Tests.repoClean;
              WebTemplate.rawAnswer(event, "[CommitProcessor] [Success] repositories cleaned.")
              Core log "[CommitWorker] -- Done."
              context.stop(self)

            }

          case "working-copy-changes" =>
            {
              val future : Future[WorkingCopyChangesWorkerDualChanges] = ask(Core.workingCopyChanges, WorkingCopyChangesWorkerGetAll).mapTo[WorkingCopyChangesWorkerDualChanges]
              future.onSuccess {
                case b : WorkingCopyChangesWorkerDualChanges =>
                  {
                    val buf = new StringBuilder()

                    val changes = b.changes.toList
                    val deletions = b.deletions.toList

                    val listChanges = WebHelper.listToHtml(changes, "fa-ul")
                    val listDeletions = WebHelper.listToHtml(deletions, "fa-ul")

                    buf ++= "<h2>Changed files</h2>"
                    buf ++= listChanges
                    buf ++= "<h2>Deleted files</h2>"
                    buf ++= listDeletions

                    event.response.contentType = "text/html"
                    event.response.write(buf.toString)
                    context.stop(self)
                  }
              }
              future.onFailure {
                case e : Exception =>
                  WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] PROCESSOR ERROR Can't contact WorkingCopyChanges actor, exception is $e.")
                  context.stop(self)
              }
            }

        }
      }

    case (event : HttpRequestEvent, action : String, arg1 : String, arg2 : String, arg3 : String) =>
      {
        action match {

          case "send-file-key" =>
            val onlyR = if (arg3 contains "-r-") true else false

            val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new SendKeyFile(arg1, arg2, onlyR)).mapTo[SuccessStatusWithMessage]
            future.onSuccess {
              case b : SuccessStatusWithMessage =>
                if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] key $arg2 was sent to $arg1.")
                else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                context.stop(self)
            }
            future.onFailure {
              case e : Exception =>
                WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR can't send key $arg2 to $arg1, error is $e.")
                context.stop(self)
            }

          case "send-dir-key" =>

            val rights = (
              arg3.contains("-r-"),
              arg3.contains("-w-"),
              arg3.contains("-cr-"),
              arg3.contains("-cw-")
            )

            val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new SendKeyDir(arg1, arg2, rights)).mapTo[SuccessStatusWithMessage]
            future.onSuccess {
              case b : SuccessStatusWithMessage =>
                if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] key $arg2 was sent to $arg1.")
                else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                context.stop(self)
            }
            future.onFailure {
              case e : Exception =>
                WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR can't send key $arg2 to $arg1, error is $e.")
                context.stop(self)
            }
        }
      }

    case (event : HttpRequestEvent, action : String, pathInput : String) =>

      {
        action match {
          case "commit-new-file" =>
            {
              val path = PathNormalization.normalizeForInnerUse(pathInput, true, false)
              val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new CommitNewFile(path)).mapTo[SuccessStatusWithMessage]
              future.onSuccess {
                case b : SuccessStatusWithMessage =>
                  if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] file $path committed to the repository.")
                  else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                  context.stop(self)
              }
              future.onFailure {
                case e : Exception =>
                  WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR file $path could not be send to the repository, error is $e.")
                  context.stop(self)
              }
            }

          case "commit-new-dir" =>
            {
              val path = PathNormalization.normalizeForInnerUse(pathInput, true, true)
              val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new CommitNewDir(path)).mapTo[SuccessStatusWithMessage]
              future.onSuccess {
                case b : SuccessStatusWithMessage =>
                  if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] directory $path committed to the repository.")
                  else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                  context.stop(self)
              }
              future.onFailure {
                case e : Exception =>
                  WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR directory $path could not be send to the repository, error is $e.")
                  context.stop(self)
              }
            }

          case "commit-new-version" =>
            {
              val path = PathNormalization.normalizeForInnerUse(pathInput, true, false)

              val prevVersionFuture : Future[Option[AbstractFile]] = ask(Core.discoveryProcessor, new DiscoverPreviousVersion(path)).mapTo[Option[AbstractFile]]
              val hasBeenDeletedFuture : Future[Boolean] = ask(Core.discoveryProcessor, new DiscoverIfHasBeenDeleted(path)).mapTo[Boolean]

              val aggFut = for {
                prevVersion <- prevVersionFuture
                hasBeenDeleted <- hasBeenDeletedFuture
              } yield (prevVersion, hasBeenDeleted)

              val res = Await.result(aggFut, Config.defaultTimeout)

              if (res._1.isEmpty) {
                WebTemplate.rawAnswer(event, s"[CommitProcessor] 403 PROCESSOR ERROR file $path could not be send to the repository, no previous version found.")
                context.stop(self)
              }
              else if (res._2) {
                WebTemplate.rawAnswer(event, s"[CommitProcessor] 403 PROCESSOR ERROR new version of $path could not be send to the repository, a deletion stopped the history line.")
                context.stop(self)
              }
              else {

                val version = res._1.get.infos('version)
                val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new CommitNewVersionOfFile(path, res._1.get)).mapTo[SuccessStatusWithMessage]
                future.onSuccess {
                  case b : SuccessStatusWithMessage =>
                    if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] version $version file $path committed to the repository.")
                    else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                    context.stop(self)
                }
                future.onFailure {
                  case e : Exception =>
                    WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR version $version file $path committed to the repository, error is $e.")
                    context.stop(self)
                }
              }
            }

          case "commit-deletion" =>
            {
              val path = PathNormalization.normalizeForInnerUse(pathInput, true, false)

              val prevVersionFuture : Future[Option[AbstractFile]] = ask(Core.discoveryProcessor, new DiscoverPreviousVersion(path)).mapTo[Option[AbstractFile]]
              val prevVersion = Await.result(prevVersionFuture, Config.defaultTimeout)

              if (prevVersion.isEmpty) {
                WebTemplate.rawAnswer(event, s"[CommitProcessor] 403 PROCESSOR ERROR file $path could not be send to the repository, no previous version found.")
                context.stop(self)
              }
              else {

                val version = prevVersion.get.infos('version)
                val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new CommitDeletionOfFile(path, prevVersion.get)).mapTo[SuccessStatusWithMessage]
                future.onSuccess {
                  case b : SuccessStatusWithMessage =>
                    if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] deletion of file $path committed to the repository.")
                    else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                    context.stop(self)
                }
                future.onFailure {
                  case e : Exception =>
                    WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR deletion of file $path committed to the repository, error is $e.")
                    context.stop(self)
                }
              }
            }

          case "commit-identity" =>
            {
              val claimedIdentity = pathInput

              val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new CommitNewIdentity(claimedIdentity)).mapTo[SuccessStatusWithMessage]
              future.onSuccess {
                case b : SuccessStatusWithMessage =>
                  if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] identity $claimedIdentity committed to the repository.")
                  else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                  context.stop(self)
              }
              future.onFailure {
                case e : Exception =>
                  WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR identity $claimedIdentity could not be send to the repository, error is $e.")
                  context.stop(self)
              }
            }

          case "accept-identity" =>
            {
              val addressing = pathInput

              val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new StorePublicIdentity(addressing)).mapTo[SuccessStatusWithMessage]
              future.onSuccess {
                case b : SuccessStatusWithMessage =>
                  if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] identity committed to the repository.")
                  else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                  context.stop(self)
              }
              future.onFailure {
                case e : Exception =>
                  WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR identity could not be send to the repository, error is $e.")
                  context.stop(self)
              }
            }

          case "reject-identity" =>
            {
              val addressing = pathInput

              val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new RejectPublicIdentity(addressing)).mapTo[SuccessStatusWithMessage]
              future.onSuccess {
                case b : SuccessStatusWithMessage =>
                  if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] identity rejected")
                  else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] " + b._2)
                  context.stop(self)
              }
              future.onFailure {
                case e : Exception =>
                  WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR identity could not be rejected, error is $e.")
                  context.stop(self)
              }
            }
        }
      }
  }
}