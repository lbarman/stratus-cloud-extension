package main.scala.webserver

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.filesystem.{WorkingCopyChangesWorkerDualChanges, WorkingCopyChangesWorkerGetAll}
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class FolderWatcherWorker extends Actor {
  def receive = {
    case event : HttpRequestEvent =>

      def answerWith(text : String) =
        {
          val params = ("Working Copy Changes", "<i class=\"fa fa-folder-open\"></i> Working Copy Changes", "Lists all changes", text)
          val html = WebTemplate.createHtml(params)

          event.response.contentType = "text/html"
          event.response.write(html)
          context.stop(self)
        }

      implicit val timeout = Timeout(Config.defaultTimeout)

      val future : Future[WorkingCopyChangesWorkerDualChanges] = ask(Core.workingCopyChanges, WorkingCopyChangesWorkerGetAll).mapTo[WorkingCopyChangesWorkerDualChanges]
      future.onSuccess {
        case result : WorkingCopyChangesWorkerDualChanges =>
          {

            val buf = new StringBuilder()
            val changes = result.changes
            val deletions = result.deletions

            val changesElemList = changes.map(f => "<i class=\"fa-li fa fa-plus\"></i>" + WebHelper.htmlentities(f.toString())).toList
            val deletionsElemList = deletions.map(f => "<i class=\"fa-li fa fa-minus\"></i>" + WebHelper.htmlentities(f.toString())).toList

            buf ++= "<h2>Changes</h2>"
            buf ++= WebHelper.listToHtml(changesElemList, "fa-ul")

            buf ++= "<h2>Deletions</h2>"
            buf ++= WebHelper.listToHtml(deletionsElemList, "fa-ul")
            answerWith(buf.toString)
          }
      }
      future.onFailure {
        case _ => answerWith("[WorkingCopyChanges] No infos from WorkingCopyChanges")
      }

  }
}