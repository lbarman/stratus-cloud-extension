package main.scala.webserver

import scala.Array.canBuildFrom
import scala.concurrent.Await

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.db.engine.PublicIdentityDatabaseEntry
import main.scala.processors.{DiscoverLocallyStoredPublicIdentities, DiscoverMyIdentities, DiscoverPublicIdentities}
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class IdentitiesWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def me = """<i class="fa fa-user"></i> Me"""

  def publicIdentityDisplay(e : PublicIdentityDatabaseEntry, accepted : Boolean, isMe : Boolean) = {
    val addr = e.addressing.getValue
    val acceptedText = if (accepted)
      s"""<span class="accepted_id"><i class="fa fa-check-square-o"></i> Accepted</span> <a class="identityReject" href="/commit/reject-identity/$addr"><i class="fa fa-minus-square"></i> Reject</a>"""
    else
      s"""<span class="not_accepted_id"><i class="fa fa-square-o"></i> Not accepted</span> <a class="identityAccept" href="/commit/accept-identity/$addr"><i class="fa fa-plus-square"></i> Accept</a>"""

    val meText = if (isMe) me else ""

    "<p><i class=\"fa-li fa fa-key\"></i>" + WebHelper.printTrustedIdentity(e.owner.getValue) + s"</b> $meText $acceptedText<br />" +
      "&nbsp;" + WebHelper.printDomain(e.pcName.getValue, e.pcNameReason.getValue) + "<br />" +
      "&nbsp;" + WebHelper.printTimestamp(e.published_on.getValue) + "<br />" +
      "&nbsp;" + WebHelper.printAddressing(e.addressing.getValue) +
      "</p>"
  }

  def publicIdentityDisplayToSend(e : PublicIdentityDatabaseEntry, accepted : Boolean, isMe : Boolean, keyAddr : String, isFile : Boolean, r : Boolean, w : Boolean, cr : Boolean, cw : Boolean) = {
    val addr = e.addressing.getValue
    val acceptedText = if (accepted)
      s"""<span class="accepted_id"><i class="fa fa-check-square-o"></i> Accepted</span> """
    else
      s"""<span class="not_accepted_id"><i class="fa fa-square-o"></i> Not accepted</span> """

    val link = if (isFile) "send-file-key" else "send-dir-key"
    val rwTag =
      if (isFile) {
        if (w) "-w-"
        else "-r-"
      }
      else {
        if (w) "-w-"
        else if (cr) "-cr-"
        else if (cw) "-cw-"
        else "-r-"
      }
    val meText = if (isMe) me else ""
    val rwText = rwTag.replaceAllLiterally("-", "").toUpperCase
    val sendText = s"""<a class="sendKey" href="/commit/$link/$addr/$keyAddr/$rwTag"><i class="fa fa-paper-plane"></i> Send him the <b>$rwText</b> key</a>"""

    "<p><i class=\"fa-li fa fa-key\"></i>" + WebHelper.printTrustedIdentity(e.owner.getValue) + s"</b> $meText $acceptedText $sendText<br />" +
      "&nbsp;" + WebHelper.printDomain(e.pcName.getValue, e.pcNameReason.getValue) + "<br />" +
      "&nbsp;" + WebHelper.printTimestamp(e.published_on.getValue) + "<br />" +
      "&nbsp;" + WebHelper.printAddressing(e.addressing.getValue) +
      "</p>"
  }

  def myIdentitiesDisplay(e : PublicIdentityDatabaseEntry) = {
    val addr = e.addressing.getValue
    val ownerName = e.owner.getValue
    val action = s"""<a class="identitySwitch" href="/public_identities/switch/$ownerName"><i class="fa fa-sign-in"></i> Use this one</a>&nbsp;""" +
      s"""<a class="identityDelete" href="/commit/reject-identity/$addr"><i class="fa fa-minus-square"></i> Delete</a>"""

    "<p><i class=\"fa-li fa fa-key\"></i>" + WebHelper.printTrustedIdentity(e.owner.getValue) + "</b> " + me + s" $action<br />" +
      "&nbsp;" + WebHelper.printDomain(e.pcName.getValue, e.pcNameReason.getValue) + "<br />" +
      "&nbsp;" + WebHelper.printTimestamp(e.published_on.getValue) + "<br />" +
      "&nbsp;" + WebHelper.printAddressing(e.addressing.getValue) +
      "</p>"
  }

  def receive = {

    case (event : HttpRequestEvent, action : String, arg1 : String, arg2 : String, arg3 : String) =>
      {
        action match {

          case "send-key-to-someone" =>
            val isFile = (arg1 == "file")
            val isR = (arg2.contains("-r-"))
            val isRW = (arg2.contains("-w-"))
            val isCR = (arg2.contains("-cr-"))
            val isCW = (arg2.contains("-cw-"))

            val priv_Identities = Await.result(ask(Core.discoveryProcessor, DiscoverLocallyStoredPublicIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val pub_Identities = Await.result(ask(Core.discoveryProcessor, DiscoverPublicIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val my_identities = Await.result(ask(Core.discoveryProcessor, DiscoverMyIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)

            val pidDisp = pub_Identities.map(e =>
              publicIdentityDisplayToSend(e, priv_Identities.contains(e), my_identities.contains(e), arg3, isFile, isR, isRW, isCR, isCW))
            val publicIds = WebHelper.listToHtml(pidDisp.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write(publicIds)
            context.stop(self)
        }
      }

    case (event : HttpRequestEvent, action : String, arg1 : String) =>
      {
        action match {

          case "switch" =>

            Core.reloadIdentity(arg1)

            WebTemplate.rawAnswer(event, "[IdentitiesWorker] [Success] Operation done.")
            context.stop(self)
        }
      }

    case (event : HttpRequestEvent, action : String) =>
      {
        action match {
          case "public-ids" =>
            val priv_Identities = Await.result(ask(Core.discoveryProcessor, DiscoverLocallyStoredPublicIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val pub_Identities = Await.result(ask(Core.discoveryProcessor, DiscoverPublicIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val myIdentities = Await.result(ask(Core.discoveryProcessor, DiscoverMyIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val pidDisp = pub_Identities.map(e => publicIdentityDisplay(e, priv_Identities.contains(e), myIdentities.contains(e)))
            val publicIds = WebHelper.listToHtml(pidDisp.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write("<h2>Public Identities</h2>" + publicIds)
            context.stop(self)

          case "accepted-ids" =>
            val priv_Identities = Await.result(ask(Core.discoveryProcessor, DiscoverLocallyStoredPublicIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val myIdentities = Await.result(ask(Core.discoveryProcessor, DiscoverMyIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val lidDisp = priv_Identities.map(e => publicIdentityDisplay(e, true, myIdentities.contains(e)))
            val idsAccepted = WebHelper.listToHtml(lidDisp.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write("<h2>Trusted Public Identities</h2>" + idsAccepted)
            context.stop(self)

          case "my-ids" =>
            val myIdentities = Await.result(ask(Core.discoveryProcessor, DiscoverMyIdentities).mapTo[Array[PublicIdentityDatabaseEntry]], Config.defaultTimeout)
            val midDisp = myIdentities map myIdentitiesDisplay
            val myIds = WebHelper.listToHtml(midDisp.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write("<h2>My Identities</h2>" + myIds)
            context.stop(self)
        }
      }

    case event : HttpRequestEvent =>

      val body = WebTemplate.createHtml(Map[String, String](), "identities.template.html")

      val params = ("Public Identities", "<i class=\"fa fa-users\"></i> Public Identities", "Lists all the public keys on the server; shows which ones you accepted locally", body)
      val html = WebTemplate.createHtml(params)

      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)
  }
}