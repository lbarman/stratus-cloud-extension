package main.scala.webserver

import scala.concurrent.Await

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.common.PathNormalization
import main.scala.filesystem.AbstractDirectory
import main.scala.processors.{DiscoverAllReadableFiles, DiscoveredAllFiles, HarvestNewFiles, HarvestTopDown}
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class HarvesterWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def displayAvailableDirs(event : HttpRequestEvent) {

    def dirDisplay(f : AbstractDirectory) =
      {
        "<i class=\"fa-li fa fa-folder-open\"></i><b>" +
          WebHelper.htmlentities(f.infos('fullpath)) +
          "</b>&nbsp;&nbsp;<a class=\"harvest\" href=\"/harvester/harvest-top-down/" +
          WebHelper.path2WebEncoded(f.infos('fullpath)) + "\"><i class=\"fa fa-sort-amount-asc\"></i> Harvest</a> (addressing=" + f.infos('addressing) + ")"
      }

    val entries = Await.result(ask(Core.discoveryProcessor, DiscoverAllReadableFiles).mapTo[DiscoveredAllFiles], Config.defaultTimeout)

    //get data
    val allDirs = entries.directories.toList filterNot { d => d.infos('fullpath).equals(Config.unix_dir_separator) }

    val dirsDisp = allDirs.map(dirDisplay)
    val notEmptyDirs = if (dirsDisp.length > 0) dirsDisp else List("--nothing--")

    val dirs = WebHelper.listToHtml(notEmptyDirs, "fa-ul")

    WebTemplate.rawAnswer(event, dirs)
    context.stop(self)
  }

  def receive = {

    case (event : HttpRequestEvent, action : String) =>
      action match {

        case "all-dirs" =>
          displayAvailableDirs(event)

        case "harvest-new-versions" =>
          {
            val number = Await.result(ask(Core.keyHarvester, HarvestNewFiles).mapTo[Int], Config.defaultTimeout)

            WebTemplate.rawAnswer(event, s"[HarvesterWorker] [Success] $number new keys found.")
            context.stop(self)
          }

      }

    case (event : HttpRequestEvent, action : String, pathInput : String) =>
      action match {
        case "harvest-top-down" =>
          {
            def addrDisplay(addr : String) =
              "<i class=\"fa-li fa fa-key\"></i><i class=\"fa fa-file\"></i>&nbsp;<b>" + addr + "</b>"

            val buf = new StringBuilder()
            val path = PathNormalization.normalizeForInnerUse(WebHelper.get2path(pathInput), true, true)
            val (readKeys, writeKeys) = Await.result(ask(Core.keyHarvester, new HarvestTopDown(path)).mapTo[(Set[String], Set[String])], Config.defaultTimeout)

            buf ++= "<h2>Read keys discovered for : </h2>"
            buf ++= WebHelper.listToHtml((readKeys map addrDisplay).toList, "fa-ul")

            buf ++= "<h2>Write keys discovered for : </h2>"
            buf ++= WebHelper.listToHtml((writeKeys map addrDisplay), "fa-ul")

            event.response.contentType = "text/html"
            event.response.write(buf.toString)
            context.stop(self)
          }
      }

    case event : HttpRequestEvent =>

      val body = WebTemplate.createHtml(Map[String, String](), "harvester.template.html")

      val params = ("Harvest Keys", "<i class=\"fa fa-sort-amount-asc\"></i> Harvest Keys",
        "Select a root directory to start harvesting top-down.</span>", body)
      val html = WebTemplate.createHtml(params)
      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)
  }
}