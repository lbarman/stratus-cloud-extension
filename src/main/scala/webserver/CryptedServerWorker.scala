package main.scala.webserver

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.processors.DecryptFile

/**
  * Hello processor writes a greeting and stops.
  */
class CryptedServerWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def decryptFile(event : HttpRequestEvent, action : String, addressing : String, dest : String = "*") {
    val future : Future[Boolean] = ask(Core.discoveryProcessor, new DecryptFile(addressing, dest)).mapTo[Boolean]
    future.onSuccess {
      case b : Boolean =>
        if (b) WebTemplate.rawAnswer(event, s"[CryptedServerWorker] [Success] file $addressing  decrypted into $dest.")
        else WebTemplate.rawAnswer(event, s"[CryptedServerWorker] [Error] PROCESSOR ERROR file $addressing could not be decrypted into $dest.")
        context.stop(self)
    }
    future.onFailure {
      case e : Exception =>
        WebTemplate.rawAnswer(event, s"[CryptedServerWorker] [Error] PROCESSOR ERROR file $addressing could not be decrypted into $dest., error is $e.")
        context.stop(self)
    }
  }

  def receive = {

    case (event : HttpRequestEvent, action : String, addressing : String, outputPath : String) =>
      {
        action match {
          case "decrypt-file" => decryptFile(event, action, addressing, outputPath)

        }
      }

    case (event : HttpRequestEvent, action : String, addressing : String) =>
      {
        action match {
          case "decrypt-file" => decryptFile(event, action, addressing)

        }
      }
  }
}