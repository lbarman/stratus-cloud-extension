package main.scala.webserver

import java.util.Date

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor

/**
  * Hello processor writes a greeting and stops.
  */
class HelloHandler extends Actor {
  def receive = {
    case event : HttpRequestEvent =>
      event.response.write("[SecureCloudRepo] Hello from the web server (" + new Date().toString + ")")
      context.stop(self)
  }
}