package main.scala.webserver

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.util.Timeout
import main.scala.Config
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class PageWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def receive = {

    case (event : HttpRequestEvent, action : String) =>
      {
        action match {
          case "getting-started" =>
            val body = WebTemplate.createHtml(Map[String, String](), "getting-started.template.html")

            val params = ("Getting Started", "<i class=\"fa fa-life-ring\"></i> Getting Started",
              "A quick introduction to get you up and running.</span>", body)
            val html = WebTemplate.createHtml(params)
            event.response.contentType = "text/html"
            event.response.write(html)
            context.stop(self)
        }
      }
  }
}