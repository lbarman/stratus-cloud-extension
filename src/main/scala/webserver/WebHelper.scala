package main.scala.webserver

import main.scala.Config
import main.scala.crypto.AbstractIdentity.VerifiedIdentity
import main.scala.db.engine.DatabaseField

object WebHelper {

  val PATH_PARTS_SEPARATOR = (">", "%3E")

  def pathEncodedToStandardPath(s : String) = get2path(s)

  def listToHtml(l : List[String]) : String =
    {
      val l2 = l.map("<li>" + _ + "</li>")
      "<ul>" + l2.mkString + "</ul>"
    }
  def listToHtml(l : Set[String]) : String =
    {
      val l2 = l.map("<li>" + _ + "</li>")
      "<ul>" + l2.mkString + "</ul>"
    }
  def listToHtml(l : List[String], cssClass : String) : String =
    {
      val l2 = l.map("<li>" + _ + "</li>")
      "<ul class=\"" + cssClass + "\">" + l2.mkString + "</ul>"
    }
  def listToHtml(l : Set[String], cssClass : String) : String =
    {
      val l2 = l.map("<li>" + _ + "</li>")
      "<ul class=\"" + cssClass + "\">" + l2.mkString + "</ul>"
    }

  def htmlentities(text : String) = org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(text).
    replaceAllLiterally("\\", "\\\\").replaceAll ("<", "&lt;").replaceAll (">", "&gt;").replaceAll ("\t", "")

  def path2Web(path : String) = path.replaceAllLiterally(Config.unix_dir_separator, PATH_PARTS_SEPARATOR._1)
  def path2WebEncoded(path : String) = path.replaceAllLiterally(Config.unix_dir_separator, PATH_PARTS_SEPARATOR._2)

  def get2path(params : String) =
    params.replaceAllLiterally(PATH_PARTS_SEPARATOR._2, Config.unix_dir_separator).
      replaceAllLiterally(PATH_PARTS_SEPARATOR._1, Config.unix_dir_separator).
      replaceAllLiterally("%20", " ")

  private[this] val dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy' at 'HH:mm:ss");

  def printFileRights(rights : String) : String =
    printFileRights(rights.contains("-r-"), rights.contains("-w-"))

  def printFileRights(r : Boolean, w : Boolean) =
    {
      val t = new StringBuilder()
      if (r) t ++= "<span class=\"canread\"><i class=\"fa fa-eye\"></i> <span class=\"powerUser\">R</span><span class=\"normalUser\">READ</span></span> "
      if (w) t ++= "<span class=\"canwrite\"><i class=\"fa fa-pencil\"></i> <span class=\"powerUser\">W</span><span class=\"normalUser\">WRITE</span></span> "
      t.toString
    }

  def printRWSend(isFile : Boolean, isR : Boolean, isW : Boolean, keyAddr : String) =
    {
      val fileText = if (isFile) "file" else "dir"
      val t = new StringBuilder()
      if (isR)
        t ++= "<a class=\"discoveryProcessor\" href=\"/public_identities/send-key-to-someone/" + fileText + "/-r-/" + keyAddr + "\">" +
          "<i class=\"fa fa-paper-plane\"></i> <span class=\"powerUser\">R</span><span class=\"normalUser\">Send READ</span></a> "

      if (isW)
        t ++= "<a class=\"discoveryProcessor\" href=\"/public_identities/send-key-to-someone/" + fileText + "/-w-/" + keyAddr + "\">" +
          "<i class=\"fa fa-paper-plane\"></i> <span class=\"powerUser\">R+W</span><span class=\"normalUser\">Send READ+WRITE</span></a> "
      t.toString
    }

  def printCRCWSend(isCR : Boolean, isCW : Boolean, keyAddr : String) =
    {
      val t = new StringBuilder()
      if (isCR)
        t ++= "<a class=\"discoveryProcessor\" href=\"/public_identities/send-key-to-someone/dir/-cr-/" + keyAddr + "\"><i class=\"fa fa-paper-plane\"></i> " +
          "<span class=\"powerUser\">R+CR</span><span class=\"normalUser\">READ+CascREAD</span></a> "

      if (isCW)
        t ++= "<a class=\"discoveryProcessor\" href=\"/public_identities/send-key-to-someone/dir/-cw-/" + keyAddr + "\"><i class=\"fa fa-paper-plane\"></i> " +
          "<span class=\"powerUser\">R+W+CR+CW</span><span class=\"normalUser\">READ+CascREAD+WRITE+CascWRITE</span></a> "
      t.toString
    }

  def printDirRights(rights : String) : String =
    printDirRights(rights.contains("-r-"), rights.contains("-w-"), rights.contains("-cr-"), rights.contains("-cw-"))

  def printDirRights(r : Boolean, w : Boolean, cr : Boolean, cw : Boolean) =
    {
      val t = new StringBuilder()
      if (r) t ++= "<span class=\"canread\"><i class=\"fa fa-eye\"></i> <span class=\"powerUser\">R</span><span class=\"normalUser\">READ</span></span> "
      if (w) t ++= "<span class=\"canwrite\"><i class=\"fa fa-pencil\"></i> <span class=\"powerUser\">W</span><span class=\"normalUser\">WRITE</span></span> "
      if (cr) t ++= "<span class=\"canread\"><i class=\"fa fa-eye\"></i> <span class=\"powerUser\">CR</span><span class=\"normalUser\">CascREAD</span></span> "
      if (cw) t ++= "<span class=\"canwrite\"><i class=\"fa fa-pencil\"></i> <span class=\"powerUser\">CW</span><span class=\"normalUser\">CascWRITE</span></span> "
      t.toString
    }

  def printDirRights(rw : Boolean) =
    "<span class=\"canread\"><i class=\"fa fa-eye\"></i>R</span> <span class=\"canwrite\"><i class=\"fa fa-pencil\"></i>W</span> "

  def printTimestamp(timestamp : String) =
    {
      val date = dateFormat.format(new java.util.Date(timestamp.toLong))
      s"""<span class="timestamp"><i class="fa fa-calendar"></i> $date</span>"""
    }

  def printDomain(domain : String, extraInfo : String) =
    {
      if (extraInfo == "OK")
        s"""<span class="domain"><i class="fa fa-desktop"></i> $domain</span>"""
      else
        s"""<span class="domain"><i class="fa fa-desktop"></i> $domain <span class="failureC">($extraInfo)</span></span>"""
    }

  def printAddressing(addressing : String) =
    {
      s"""<span class="addressing"><i class="fa fa-link"></i> $addressing&nbsp;&nbsp;</span>"""
    }

  def printPath(pathInput : String) =
    {
      val path = htmlentities(pathInput)
      s"""<span class="path"><i class="fa fa-compass"></i> $path</span>&nbsp;&nbsp;"""
    }

  def printKey(name : String, input : DatabaseField) =
    s"""<span class="keyDb"><i class="fa fa-key"></i> <span class="keyName">""" + name + ": </span>" + input.getValue + "</span>"

  def printIdentity(claimedOwnerPk : String, verifiedOwner : VerifiedIdentity) =
    {
      verifiedOwner match {
        case Some((pk : String, id : String)) =>
          if (pk == claimedOwnerPk)
            s"""<span class=\"trusted-id\"><i class=\"fa fa-check-circle-o\"></i> $id</span>"""
          else
            s"""<span class=\"unknown-id\"><i class=\"fa fa-exclamation-triangle\"></i> Wrong Identity '$pk' vs '$claimedOwnerPk'</span>"""

        case None =>
          "<span class=\"unknown-id\"><i class=\"fa fa-exclamation-triangle\"></i> Unknown Identity</span>"
      }
    }

  def printTrustedIdentity(verifiedOwner : String) =
    s"""<span class=\"trusted-id\"><i class=\"fa fa-check-circle-o\"></i> $verifiedOwner</span>"""

}