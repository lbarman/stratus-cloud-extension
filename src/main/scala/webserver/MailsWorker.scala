package main.scala.webserver

import scala.Array.canBuildFrom
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{ Config, Core }
import main.scala.Core.SuccessStatusWithMessage
import main.scala.crypto.AbstractIdentity.VerifiedIdentity
import main.scala.db.engine.PublicMessageDatabaseEntry
import main.scala.processors.{ DeleteMessage, DiscoverAndDecryptMessage, DiscoverMessages }
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class MailsWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def messageDisplay(value : Tuple3[PublicMessageDatabaseEntry, VerifiedIdentity, VerifiedIdentity]) =
    {
      val (e, from, to) = value
      val actions = "<b><a class=\"mailToOpen\" href=\"/mails/decrypt-message/" + e.addressing.getValue + "\"><i class=\"fa fa-inbox\"></i> Open</a></b>" +
        "&nbsp;&nbsp; <b><a class=\"deleteMail\" href=\"/mails/delete-message/" + e.addressing.getValue + "\"><i class=\"fa fa-times\"></i> Delete</a></b>"
      val mailType = if (e.contentType.getValue.equals("lkfdbe")) "<i class=\"fa fa-file\"></i> File" else "<i class=\"fa fa-folder\"></i> Directory"
      val rights = if (e.contentType.getValue.equals("lkfdbe")) WebHelper.printFileRights(e.contentKeys.getValue) else WebHelper.printDirRights(e.contentKeys.getValue)

      "<p><i class=\"fa-li fa fa-envelope\"></i><i>" + e.contentDesc.getValue + "</i> " + rights + "&nbsp;" + actions + "<br />" +
        "<span class=\"powerUser\"><b> " + mailType + "</b> " + e.contentName.getValue + "<br /></span>" +
        "<b>From : </b> " + WebHelper.printIdentity(e.from.getValue, from) + "<br />" +
        "<b>To : </b> " + WebHelper.printIdentity(e.to.getValue, to) + "<br />" +
        "&nbsp;" + WebHelper.printTimestamp(e.timestamp.getValue) +
        "</p>"
    }

  def receive = {

    case (event : HttpRequestEvent, action : String) =>
      {
        action match {

          case "inbox" =>

            val buf = new StringBuilder()

            val messagesIdFromIdTo = Await.result(ask(Core.discoveryProcessor, DiscoverMessages).mapTo[Array[(PublicMessageDatabaseEntry, Option[(String, String)], Option[(String, String)])]], Config.defaultTimeout)
            val messagesDisp = messagesIdFromIdTo map messageDisplay

            buf ++= WebHelper.listToHtml(messagesDisp.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write("<h2>Inbox</h2>" + buf.toString)
            context.stop(self)

          case "get-messages" =>
            val future : Future[Array[PublicMessageDatabaseEntry]] = ask(Core.discoveryProcessor, DiscoverMessages).mapTo[Array[PublicMessageDatabaseEntry]]
            future.onSuccess {
              case list : Array[PublicMessageDatabaseEntry] =>
                {
                  def displayMessage(m : PublicMessageDatabaseEntry) =
                    {
                      "<b>" + m.getAddressing.getValue + "</b><br />" +
                        "from: " + m.from.getValue + "<br />" +
                        "to: " + m.to.getValue + "<br />" +
                        "timestamp: " + m.timestamp.getValue
                    }

                  val msgText = list map displayMessage toList

                  val msgList = WebHelper.listToHtml(msgText, "fa-ul")

                  event.response.contentType = "text/html"
                  event.response.write("<h2>Message :</h2>" + msgList)
                  context.stop(self)
                }
            }
            future.onFailure {
              case e : Exception =>
                WebTemplate.rawAnswer(event, s"[DiscoveryProcessor] [Error] PROCESSOR ERROR read messages, exception is $e.")
                context.stop(self)
            }

        }
      }

    case (event : HttpRequestEvent, action : String, params : String) =>
      {
        action match {

          case "delete-message" =>
            val future : Future[SuccessStatusWithMessage] = ask(Core.commitProcessor, new DeleteMessage(params)).mapTo[SuccessStatusWithMessage]
            future.onSuccess {
              case b : SuccessStatusWithMessage =>
                {
                  if (b._1) WebTemplate.rawAnswer(event, s"[CommitProcessor] [Success] Mail deleted.")
                  else WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] Can't delete mail, reason is '" + b._2 + "'.")

                }
            }
            future.onFailure {
              case e : Exception =>
                WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] PROCESSOR ERROR delete message, exception is $e.")
                context.stop(self)
            }

          case "decrypt-message" =>
            val future : Future[Boolean] = ask(Core.discoveryProcessor, new DiscoverAndDecryptMessage(params)).mapTo[Boolean]
            future.onSuccess {
              case b : Boolean =>
                {
                  WebTemplate.rawAnswer(event, s"[DiscoveryProcessor] [Success] Mail opened; Key added to your local database.")

                }
            }
            future.onFailure {
              case e : Exception =>
                WebTemplate.rawAnswer(event, s"[DiscoveryProcessor] [Error] PROCESSOR ERROR decrypt message, exception is $e.")
                context.stop(self)
            }

        }
      }
    case event : HttpRequestEvent =>

      val body = WebTemplate.createHtml(Map[String, String](), "mails.template.html")

      val params = ("Mails", "<i class=\"fa fa-envelope\"></i> Mails", "Lists received keys", body)
      val html = WebTemplate.createHtml(params)

      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)
  }
}