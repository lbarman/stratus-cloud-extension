package main.scala.webserver

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.util.Timeout
import main.scala.Config
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class FileExplorerWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def receive = {
    case event : HttpRequestEvent =>
      {
        val body = WebTemplate.createHtml(Map[String, String](), "file-explorer.template.html")

        val params = ("File Explorer", "<i class=\"fa fa-life-ring\"></i> File Explorer",
          "A quick introduction to get you up and running.</span>", body)
        val html = WebTemplate.createHtml(params)
        event.response.contentType = "text/html"
        event.response.write(html)
        context.stop(self)

      }
  }
}