package main.scala.webserver

import org.mashupbots.socko.routes._
import org.mashupbots.socko.infrastructure.Logger
import org.mashupbots.socko.webserver.WebServer
import org.mashupbots.socko.webserver.WebServerConfig
import main.scala.Core
import akka.actor.ActorSystem
import akka.actor.Props

object ServerRouterCore {

  //
  // STEP #2 - Define Routes
  // Dispatch all HTTP GET events to a newly instanced `HelloHandler` actor for processing.
  // `HelloHandler` will `stop()` itself after processing each request.
  //
  val routes = Routes({
    case HttpRequest(request) => request match {

      // Defaults
      case GET(Path("/hello")) =>
        Core.actorSystem.actorOf(Props[HelloHandler]) ! request
      case PathSegments("logs" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props[LoggerDisplayWorker]) ! (request, action)
      case GET(Path("/logs")) =>
        Core.actorSystem.actorOf(Props[LoggerDisplayWorker]) ! request
      case GET(Path("/getting-started")) =>
        Core.actorSystem.actorOf(Props[PageWorker]) ! (request, "getting-started")

      //Dual view (Discover + Commit)
      case GET(Path("/file-explorer")) =>
        Core.actorSystem.actorOf(Props[FileExplorerWorker]) ! (request)

      //Read repository
      case PathSegments("repo-read" :: action :: path :: Nil) =>
        Core.actorSystem.actorOf(Props(new RepoReaderWorker(false))) ! (request, action, WebHelper.pathEncodedToStandardPath(path))
      case PathSegments("repo-read" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props(new RepoReaderWorker(false))) ! (request, action)
      case GET(Path("/repo-read")) =>
        Core.actorSystem.actorOf(Props(new RepoReaderWorker(false))) ! request

      //Read repository - headless
      case PathSegments("repo-read-headless" :: action :: path :: Nil) =>
        Core.actorSystem.actorOf(Props(new RepoReaderWorker(true))) ! (request, action, WebHelper.pathEncodedToStandardPath(path))
      case PathSegments("repo-read-headless" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props(new RepoReaderWorker(true))) ! (request, action)
      case GET(Path("/repo-read-headless")) =>
        Core.actorSystem.actorOf(Props(new RepoReaderWorker(true))) ! request

      //Harvest keys
      case PathSegments("harvester" :: action :: path :: Nil) =>
        Core.actorSystem.actorOf(Props[HarvesterWorker]) ! (request, action, WebHelper.pathEncodedToStandardPath(path))
      case PathSegments("harvester" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props[HarvesterWorker]) ! (request, action)
      case GET(Path("/harvester")) =>
        Core.actorSystem.actorOf(Props[HarvesterWorker]) ! request

      //Mails
      case PathSegments("mails" :: action :: param :: Nil) =>
        Core.actorSystem.actorOf(Props[MailsWorker]) ! (request, action, param)
      case PathSegments("mails" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props[MailsWorker]) ! (request, action)
      case GET(Path("/mails")) =>
        Core.actorSystem.actorOf(Props[MailsWorker]) ! request

      //Keys
      case PathSegments("keys" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props[KeysWorker]) ! (request, action)
      case GET(Path("/keys")) =>
        Core.actorSystem.actorOf(Props[KeysWorker]) ! request

      //Working Copy Changes
      case GET(Path("/changed")) =>
        Core.actorSystem.actorOf(Props[FolderWatcherWorker]) ! request

      //Commits
      case PathSegments("commit" :: action :: arg1 :: arg2 :: arg3 :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(false))) ! (request, action, arg1, arg2, arg3)
      case PathSegments("commit" :: action :: arg1 :: arg2 :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(false))) ! (request, action, arg1, arg2)
      case PathSegments("commit" :: action :: path :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(false))) ! (request, action, WebHelper.pathEncodedToStandardPath(path))
      case PathSegments("commit" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(false))) ! (request, action)
      case GET(Path("/commit")) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(false))) ! request

      //Commits - headless
      case PathSegments("commit-headless" :: action :: arg1 :: arg2 :: arg3 :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(true))) ! (request, action, arg1, arg2, arg3)
      case PathSegments("commit-headless" :: action :: arg1 :: arg2 :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(true))) ! (request, action, arg1, arg2)
      case PathSegments("commit-headless" :: action :: path :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(true))) ! (request, action, WebHelper.pathEncodedToStandardPath(path))
      case PathSegments("commit-headless" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(true))) ! (request, action)
      case GET(Path("/commit-headless")) =>
        Core.actorSystem.actorOf(Props(new CommitWorker(true))) ! request

      //MasterPassword
      case PathSegments("masterkey" :: action :: password :: Nil) =>
        Core.actorSystem.actorOf(Props[MasterPasswordWorker]) ! (request, action, password)
      case PathSegments("masterkey" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props[MasterPasswordWorker]) ! (request, action)
      case GET(Path("/masterkey")) =>
        Core.actorSystem.actorOf(Props[MasterPasswordWorker]) ! request

      //Identities
      case PathSegments("public_identities" :: action :: arg1 :: arg2 :: arg3 :: Nil) =>
        Core.actorSystem.actorOf(Props[IdentitiesWorker]) ! (request, action, arg1, arg2, arg3)
      case PathSegments("public_identities" :: action :: arg1 :: Nil) =>
        Core.actorSystem.actorOf(Props[IdentitiesWorker]) ! (request, action, arg1)
      case PathSegments("public_identities" :: action :: Nil) =>
        Core.actorSystem.actorOf(Props[IdentitiesWorker]) ! (request, action)
      case GET(Path("/public_identities")) =>
        Core.actorSystem.actorOf(Props[IdentitiesWorker]) ! request

      //Decrypt
      case PathSegments("decrypt" :: path :: output :: Nil) =>
        Core.actorSystem.actorOf(Props[CryptedServerWorker]) ! (request, "decrypt-file", path, WebHelper.pathEncodedToStandardPath(output))
      case PathSegments("decrypt" :: path :: Nil) =>
        Core.actorSystem.actorOf(Props[CryptedServerWorker]) ! (request, "decrypt-file", path)

      //resource
      case PathSegments("resources" :: name :: Nil) =>
        Core.actorSystem.actorOf(Props[ResourcesWorker]) ! (request, name)

      //Home
      case _ =>
        Core.actorSystem.actorOf(Props[HomeWorker]) ! request
    }
  })

  //
  // STEP #3 - Start and Stop Socko Web Server
  //
  def start() {
    val webServer = new WebServer(WebServerConfig(), routes, Core.actorSystem)
    webServer.start()

    Core log "[ServerRouterCore] Server started. Open your browser and navigate to http://localhost:8888"
  }

}