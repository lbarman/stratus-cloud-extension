package main.scala.webserver

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.util.Timeout
import main.scala.Config
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class HomeWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)
  def receive = {
    case event : HttpRequestEvent =>

      val buf = new StringBuilder()

      val params = ("Home", "<i class=\"fa fa-cloud\"></i> Stratus Home", "--", buf.toString)
      val html = WebTemplate.createHtml(params, "homepage.html")

      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)
  }
}