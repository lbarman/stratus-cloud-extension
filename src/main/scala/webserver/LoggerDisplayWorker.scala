package main.scala.webserver

import scala.Array.canBuildFrom
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.processors.LoggerWorkerPrintAll
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

class LoggerDisplayWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def receive = {
    case event : HttpRequestEvent =>
      val body = WebTemplate.createHtml(Map[String, String](), "logs.template.html")

      val params = ("Logger", "<i class=\"fa fa-align-left\"></i> Logger", "Lists all logs from the beginning of this instance", body)
      val html = WebTemplate.createHtml(params)

      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)

    case (event : HttpRequestEvent, action : String) =>
      action match {
        case "raw" =>
          def escape(s : String) = s.replaceAllLiterally("\"", "\\\"")
          def toJSON(messages : Array[String]) = "[\n" + (messages map { p => "\"" + escape(p) + "\"" }).mkString(",\n") + "]\n"

          def formatNicely(input : String) =
            {
              def colorLine(input : String) =
                if (input.contains("Error")) {
                  "<span class=\"failureC\">" + input + "</span>"
                }
                else if (input.contains("Debug")) {
                  "<span class=\"debugC\">" + input + "</span>"
                }
                else if (input.contains("Info")) {
                  "<span class=\"infoC\">" + input + "</span>"
                }
                else if (input.contains("Success")) {
                  "<span class=\"successC\">" + input + "</span>"
                }
                else {
                  input
                }

              def boldBrackets(input : String) =
                input.replaceAll("\\[", "<b>[").replaceAll("\\]", "]</b>")

              val lines = WebHelper.htmlentities(input).split('\n') map colorLine map boldBrackets
              lines mkString "<br />"
            }

          val future : Future[Array[String]] = ask(Core.logger, LoggerWorkerPrintAll).mapTo[Array[String]]
          future.onSuccess {
            case lines : Array[String] =>
              event.response.contentType = "text/html"
              event.response.write(toJSON(lines map formatNicely))
              context.stop(self)
          }
          future.onFailure {
            case _ =>
              event.response.contentType = "text/html"
              event.response.write("[\"-- No answer from logger--\"]")
              context.stop(self)
          }
      }

  }
}