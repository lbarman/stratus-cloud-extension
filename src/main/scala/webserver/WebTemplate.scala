package main.scala.webserver

import org.mashupbots.socko.events.HttpRequestEvent

import main.scala.{Config, Core}
import main.scala.common.FileToolBox

object WebTemplate {

  lazy val defaultDir = "resources" + Config.osFileSeparator
  lazy val defaultTemplate = defaultDir + "webserver_default_template.html"

  def regexFromField(f : String) = new util.matching.Regex("\\{\\{" + f + "\\}\\}")
  def tagFromField(f : String) = "{{" + f + "}}"

  def templateReader(template : String) =
    {
      val templatePath2Use =
        if (template.equals("[default]"))
          defaultTemplate
        else
          defaultDir + template;

      val res = FileToolBox.readAllFrom(templatePath2Use)

      if (!res._1) {
        Core log s"[WebTemplate] [Error] Can't read $templatePath2Use."
        "-- Error opening template --"
      }
      else {
        res._2
      }
    }

  def createHtml(params : WebTemplateInstance, templateFile : String = "[default]") =
    params.applyOnto(templateReader(templateFile))

  def createHtml(params : Map[String, String], templateFile : String) =
    {
      var data = templateReader(templateFile);
      params.foreach(p =>
        {
          data = data.replaceAllLiterally(tagFromField(p._1), p._2)
        })
      data
    }

  def rawAnswer(event : HttpRequestEvent, text : String) =
    {
      event.response.contentType = "text/html"
      var textOK = text.replaceAllLiterally("[Success]", "<span class=\"success\">[<i class=\"fa fa-check\"></i> Success]</span>")
      var textFail = textOK.replaceAllLiterally("[Error]", "<span class=\"failure\">[<i class=\"fa fa-exclamation-triangle\"></i> Error]</span>")

      var textTag = """(\[(\w+)\])""".r.replaceAllIn(textFail, """<span class=\"misctag\">$1</span>""")

      event.response.write(textTag)
    }

}

/*
 * Syntaxic sugar
 */

class WebTemplateInstance() {
  var file_title : String = _
  var page_title : String = _
  var page_desc : String = _
  var body : String = _

  def applyOnto(template : String) = {
    var t = template;
    t = WebTemplate.regexFromField("file_title").replaceFirstIn(t, file_title)
    t = WebTemplate.regexFromField("page_title").replaceFirstIn(t, page_title)
    t = WebTemplate.regexFromField("page_desc").replaceFirstIn(t, page_desc)
    t = t.replaceAllLiterally(WebTemplate.tagFromField("body"), body)
    t
  }
}

object WebTemplateInstance {

  implicit def Tuple2WebTemplateInstance(value : Tuple4[String, String, String, String]) =
    {
      val w = new WebTemplateInstance();
      w.file_title = value._1
      w.page_title = value._2
      w.page_desc = value._3
      w.body = value._4
      w
    }
}
