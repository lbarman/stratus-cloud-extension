package main.scala.webserver

import scala.Array.canBuildFrom
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{ Config, Core }
import main.scala.common.PathNormalization
import main.scala.crypto.AbstractIdentity.VerifiedIdentity
import main.scala.crypto.KeyDefs
import main.scala.filesystem.{ AbstractDirectory, AbstractFile }
import main.scala.processors.{ DiscoverAllReadableFiles, DiscoverCurrentIdentity, DiscoverLocalFiles, DiscoveredAllFiles }
import main.scala.webserver.WebTemplate.rawAnswer
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class RepoReaderWorker(headLess : Boolean) extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  var pathNormalized : String = _;

  def keyExistsAndNotNull(key : Option[String]) =
    (key.isDefined && !key.get.equals(""))

  def getID(f : AbstractFile) =
    f.infos('addressing).replaceAllLiterally("=", "").replaceAllLiterally("-", "")
  def getID(f : AbstractDirectory) =
    f.infos('addressing).replaceAllLiterally("=", "").replaceAllLiterally("-", "")

  def fileDisplay(f : AbstractFile) =
    {
      if (f.isDeletion) {
        "<span class=\"firstLine\"><i class=\"fa fa-caret-right\"></i>&nbsp;<b class=\"deletion\">" +
          //filename.ext (version)
          f.infos('filename) + "." + f.infos('extention) + "&nbsp;(v" + f.infos('version) + ")</b> " +
          //deletion tag
          "<i class=\"fa fa-recycle deletion\"></i>" +
          //owner
          "</span>, by " + WebHelper.printIdentity(f.infos('owner), f.vettedIdentity) +
          "&nbsp;<a href=\"#\" class=\"normalUser\" onClick=\"$('#extraInfo_" + getID(f) + "').toggle('fast');\">[more]</a>" + "<br />" +
          "<span class=\"extraInfo powerUser\" id=\"extraInfo_" + getID(f) + "\">" +
          //addressing, fullpath
          WebHelper.printAddressing(f.infos('addressing)) +
          //full path
          WebHelper.printPath(f.infos('fullpath)) +
          //timestamp
          WebHelper.printTimestamp(f.infos('timestamp)) + "</span>"
      }
      else {

        val isR = true
        val isW = keyExistsAndNotNull(f.keys().get(KeyDefs.KWritePriv_F))
        val RW = WebHelper.printFileRights(isR, isW)
        val send = WebHelper.printRWSend(true, isR, isW, f.infos('addressing))

        "<span class=\"firstLine\"><i class=\"fa fa-caret-right\"></i>&nbsp;<b>" +
          //filename.ext (version)
          f.infos('filename) + "." + f.infos('extention) + "&nbsp;(v" + f.infos('version) + ")</b>" +
          //download buttons
          "&nbsp;<a class=\"decryptFile\" href=\"/decrypt/" + f.infos('addressing) + "/" + WebHelper.path2WebEncoded(f.infos('fullpath)) +
          "\"><i class=\"fa fa-download\"></i> Get</a>&nbsp;" +
          //owner, RW buttons
          "</span>, by " + WebHelper.printIdentity(f.infos('owner), f.vettedIdentity) +
          "&nbsp;<a href=\"#\" class=\"normalUser\" onClick=\"$('#extraInfo_" + getID(f) + "').toggle('fast');return(false);\">[more]</a>" + "<br />" +
          "<span class=\"extraInfo powerUser\" id=\"extraInfo_" + getID(f) + "\">" +
          //addressing, fullpath
          WebHelper.printAddressing(f.infos('addressing)) +
          //full path
          WebHelper.printPath(f.infos('fullpath)) +
          //timestamp
          WebHelper.printTimestamp(f.infos('timestamp)) + "<br />" +
          "&nbsp; Rights: " + RW + "&nbsp;" + send + "</span>"
      }
    }

  def dirDisplay(f : AbstractDirectory) =
    {
      val isR = true
      val isW = keyExistsAndNotNull(f.keys().get(KeyDefs.KInitPriv_D))
      val isCR = keyExistsAndNotNull(f.keys().get(KeyDefs.KCascReadPriv)) && !f.infos('fullpath).equals(Config.unix_dir_separator)
      val isCW = keyExistsAndNotNull(f.keys().get(KeyDefs.KCascWritePriv)) && !f.infos('fullpath).equals(Config.unix_dir_separator)
      val RWCRCW = WebHelper.printDirRights(isR, isW, isCR, isCW)

      val send = if (!f.infos('fullpath).equals(Config.unix_dir_separator))
        WebHelper.printRWSend(false, isR, isW, f.infos('addressing))
      else ""
      val send2 = if (!f.infos('fullpath).equals(Config.unix_dir_separator))
        WebHelper.printCRCWSend(isCR, isCW, f.infos('addressing))
      else ""

      "<span class=\"firstLine\"><a class=\"discoveryProcessor\" href=\"/repo-read/dest-files/" +
        WebHelper.path2WebEncoded(f.infos('fullpath)) + "\"><i class=\"fa-li fa fa-folder-open\"></i><b>" +
        //relativized folderpath
        WebHelper.htmlentities(f.infos('fullpath).replaceAllLiterally(pathNormalized, "")) + "</b></a></span>" +
        //from
        "&nbsp;, by " + WebHelper.printIdentity(f.infos('owner), f.vettedIdentity) + "</span>" +
        "&nbsp;<a href=\"#\" class=\"normalUser\" onClick=\"$('#extraInfo_" + getID(f) + "').toggle('fast');return(false);\">[more]</a>" + "<br />" +
        "<span class=\"extraInfo powerUser\" id=\"extraInfo_" + getID(f) + "\">" +
        //addressing, fullpath
        WebHelper.printAddressing(f.infos('addressing)) +
        //full path
        WebHelper.printPath(f.infos('fullpath)) +
        //timestamp
        WebHelper.printTimestamp(f.infos('timestamp)) + "<br />" +
        "&nbsp; Rights: " + RWCRCW + "<span class=\"normalUser\"><br />&nbsp;</span>" + send + " " + send2 + "</span>"
    }

  def displayFilesFor(pathInput : String, event : HttpRequestEvent) = {
    val buf = new StringBuilder()

    pathNormalized = PathNormalization.normalizeForInnerUse(pathInput, true, true) //could be garbage, if pathInput = *

    val entries = Await.result(ask(Core.discoveryProcessor, DiscoverAllReadableFiles).mapTo[DiscoveredAllFiles], Config.defaultTimeout)

    //get data
    val allFiles = entries.files.toList
    val allDirs = entries.directories.toList

    //filter if needed
    val filesFiltered = if (pathInput.equals("*"))
      allFiles
    else
      allFiles.filter(f => PathNormalization.innerAbovePath(f.infos('fullpath), false).equals(pathNormalized))

    val allFilesGrouped = filesFiltered groupBy (f => f.infos('fullpath)) //group by same filename

    val dirsFiltered = if (pathInput.equals("*"))
      allDirs
    else
      allDirs.filter(
        !_.infos('fullpath).equals(Config.unix_dir_separator))
        .filter(f => PathNormalization.innerAbovePath(f.infos('fullpath), true).equals(pathNormalized))

    //store lists as ul li
    val nav = "<a class=\"discoveryProcessor\" href=\"/repo-read/dest-files/" + WebHelper.path2WebEncoded(pathNormalized) + "\"><i class=\"fa-li fa fa-folder-open\"></i><b>./</b></a>" ::
      "<a class=\"discoveryProcessor\" href=\"/repo-read/dest-files/" + WebHelper.path2WebEncoded(PathNormalization.innerAbovePath(pathNormalized, true)) + "\"><i class=\"fa-li fa fa-folder-open\"></i><b>../</b></a>" :: Nil
    buf ++= WebHelper.listToHtml(nav, "fa-ul filelist")
    buf ++= WebHelper.listToHtml(dirsFiltered.map(dirDisplay), "fa-ul filelist")

    //display same versions of the file grouped
    for (k <- allFilesGrouped.keys) {
      val theseFiles = allFilesGrouped.get(k).get.sortBy(f => -f.infos('timestamp).toLong)

      buf ++= "<ul class=\"fa-ul filelist\"><li><i class=\"fa-li fa fa-file\"></i><b>" + theseFiles(0).infos('filename) + "." +
        theseFiles(0).infos('extention) + "</b></li></ul>"

      buf ++= WebHelper.listToHtml(theseFiles.map(f => fileDisplay(f)), "fa-ul filelist")
    }

    event.response.contentType = "text/html"
    event.response.write(buf.toString)
    context.stop(self)
  }

  def receive = {
    case event : HttpRequestEvent =>
      {
        val template = if (headLess) "discovery.headless.template.html" else "discovery.template.html"
        val body = WebTemplate.createHtml(Map[String, String](), template)

        val params = ("Discovery Processor", "<i class=\"fa fa-folder-open\"></i> Discovery Processor", "Lists all files and directories of the repository", body)
        val html = WebTemplate.createHtml(params)

        event.response.contentType = "text/html"
        event.response.write(html)
        context.stop(self)
      }

    case (event : HttpRequestEvent, action) =>
      action match {

        case "current-id" =>
          val myId = Await.result(ask(Core.discoveryProcessor, DiscoverCurrentIdentity).mapTo[VerifiedIdentity], Config.defaultTimeout)
          rawAnswer(event, WebHelper.printIdentity(myId.get._1, myId))
      }

    case (event : HttpRequestEvent, action, pathInput : String) =>
      action match {

        case "source-files" =>
          {

            val path = PathNormalization.normalizeForInnerUse(pathInput, true, true)

            def getAction(f : Tuple4[String, Boolean, Boolean, Boolean]) =
              {
                //if is dir
                if (f._1.endsWith(Config.unix_dir_separator)) {
                  if (!f._2) //dir doesn't exist
                    " - <a class=\"commitProcessorUploadNewDir\" href=\"/commit/commit-new-dir/" +
                      WebHelper.path2WebEncoded(f._1) + "\"><i class=\"fa fa-cloud-upload\"></i> Send</a>"
                  else
                    ""
                }
                else {
                  if (f._4)
                    ""
                  else if (f._3) //previous version exists
                    " - <a class=\"commitProcessorUploadNewV\" href=\"/commit/commit-new-version/" +
                      WebHelper.path2WebEncoded(f._1) + "\"><i class=\"fa fa-cloud-upload\"></i> Send new version</a>&nbsp;" +
                      "<a class=\"commitProcessorUploadDelete\" href=\"/commit/commit-deletion/" +
                      WebHelper.path2WebEncoded(f._1) + "\"><i class=\"fa fa-ban\"></i> Send deletion</a>"
                  else
                    " - <a class=\"commitProcessorUploadNewFile\" href=\"/commit/commit-new-file/" +
                      WebHelper.path2WebEncoded(f._1) + "\"><i class=\"fa fa-cloud-upload\"></i> Send new file</a>"
                }
              }

            def entryDisplay(f : Tuple4[String, Boolean, Boolean, Boolean]) =
              {
                //._1 is filepath, ._2 is "dir exists on remote repo", ._3 is "file exists on remote repo", ._4 is "was deleted"
                if (f._1.endsWith(Config.unix_dir_separator)) {
                  "<a class=\"discoveryProcessor\" href=\"/repo-read/source-files/" +
                    WebHelper.path2WebEncoded(f._1) + "\"><i class=\"fa-li fa fa-folder-open\"></i><b>" +
                    WebHelper.htmlentities(f._1.replaceAllLiterally(path, "")) +
                    "</b></a>" + getAction(f)
                }
                else {
                  "<i class=\"fa-li fa fa-file\"></i> <b>" + WebHelper.htmlentities(f._1.replaceAllLiterally(path, "")) + "</b>" + getAction(f)
                }
              }

            val future : Future[Array[(String, Boolean, Boolean, Boolean)]] = ask(Core.discoveryProcessor, new DiscoverLocalFiles(path)).mapTo[Array[(String, Boolean, Boolean, Boolean)]]
            future.onSuccess {
              case list : Array[(String, Boolean, Boolean, Boolean)] =>

                val dotdotslash = "<a class=\"discoveryProcessor\" href=\"/repo-read/source-files/" +
                  WebHelper.path2WebEncoded(PathNormalization.innerAbovePath(path, true)) + "\"><i class=\"fa-li fa fa-folder-open\"></i><b>../</b></a> ";

                val sorted = list.sortBy(f => f._1.endsWith(Config.unix_dir_separator)) reverse
                val entriesDisp = dotdotslash +: (sorted map entryDisplay)
                val buf = new StringBuilder()
                val listEntries = WebHelper.listToHtml(entriesDisp.toList, "fa-ul")

                val myId = Await.result(ask(Core.discoveryProcessor, DiscoverCurrentIdentity).mapTo[VerifiedIdentity], Config.defaultTimeout)

                buf ++= "<p class=\"your-identity\">Your current identity : " + WebHelper.printIdentity(myId.get._1, myId) + "</p>"
                buf ++= listEntries

                WebTemplate.rawAnswer(event, buf.toString)
                context.stop(self)
            }
            future.onFailure {
              case e : Exception =>
                WebTemplate.rawAnswer(event, s"[CommitProcessor] [Error] 402 PROCESSOR ERROR can't read files in $pathInput, error is $e.")
                context.stop(self)
            }
          }
        case "dest-files" =>
          displayFilesFor(WebHelper.get2path(pathInput), event)
      }
  }
}