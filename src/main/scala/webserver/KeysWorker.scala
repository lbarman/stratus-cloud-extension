package main.scala.webserver

import scala.Array.canBuildFrom
import scala.concurrent.Await

import org.mashupbots.socko.events.HttpRequestEvent

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import main.scala.{Config, Core}
import main.scala.db.engine.{LocalKeyDirDatabaseEntry, LocalKeyFileDatabaseEntry, PublicStructuralDirDatabaseEntry, PublicStructuralFileDatabaseEntry}
import main.scala.processors.{DiscoverLocalKeys, DiscoverPublicKeys, DiscoveredLocalKeys, DiscoveredPublicKeys}
import main.scala.webserver.WebTemplateInstance.Tuple2WebTemplateInstance

/**
  * Hello processor writes a greeting and stops.
  */
class KeysWorker extends Actor {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def localKeyFileDisplay(e : LocalKeyFileDatabaseEntry) =
    "<p><i class=\"fa-li fa fa-caret-right\"></i><i class=\"fa fa-file\"></i>&nbsp;<b>" + e.addressing.getValue + "</b><br />" +
      WebHelper.printKey("K_F", e.K) + "<br />" +
      WebHelper.printKey("KWritePriv_F", e.KWritePriv) + "<br />" +
      "</p>"
  def localKeyDirDisplay(e : LocalKeyDirDatabaseEntry) =
    "<p><i class=\"fa-li fa fa-caret-right\"></i><i class=\"fa fa-folder\"></i>&nbsp;<b>" + e.addressing.getValue + "</b><br />" +
      WebHelper.printKey("K_D", e.K) + "<br />" +
      WebHelper.printKey("KInitPriv_D", e.KInitPriv) + "<br />" +
      WebHelper.printKey("KCascadeReadPriv_D", e.KCascadeReadPriv) + "<br />" +
      WebHelper.printKey("KCascadeWritePriv_D", e.KCascadeWritePriv) + "<br />" +
      "</p>"

  def publicStructuralFileDisplay(e : PublicStructuralFileDatabaseEntry) =
    "<p><i class=\"fa-li fa fa-caret-right\"></i><i class=\"fa fa-file\"></i>&nbsp;<b>" + e.addressing.getValue + "</b><br />" +
      WebHelper.printKey("IV_F", e.iv) + "<br />" +
      WebHelper.printKey("KWritePub_F", e.KWritePub) + "<br />" +
      WebHelper.printKey("KSignV0Pub_F", e.sign_v0_publickey) + "<br />" +
      WebHelper.printKey("KSignVNPub_F", e.sign_vN_publickey) + "<br />" +
      "</p>"
  def publicStructuralDirDisplay(e : PublicStructuralDirDatabaseEntry) =
    "<p><i class=\"fa-li fa fa-caret-right\"></i><i class=\"fa fa-folder\"></i>&nbsp;<b>" + e.addressing.getValue + "</b><br />" +
      WebHelper.printKey("IV_F", e.iv) + "<br />" +
      WebHelper.printKey("KInitPub_D", e.KInitPubD) + "<br />" +
      WebHelper.printKey("KCascadeReadPub_D", e.KCascadeReadPub) + "<br />" +
      WebHelper.printKey("KCascadeWritePub_D", e.KCascadeWritePub) + "<br />" +
      "</p>"

  def receive = {

    case (event : HttpRequestEvent, action : String) =>
      {
        action match {
          case "local" =>

            val buf = new StringBuilder()

            val keys = Await.result(ask(Core.discoveryProcessor, DiscoverLocalKeys).mapTo[DiscoveredLocalKeys], Config.defaultTimeout)
            val files = keys.filesKeys map localKeyFileDisplay
            val dirs = keys.directoriesKeys map localKeyDirDisplay
            buf ++= WebHelper.listToHtml(files.toList, "fa-ul")
            buf ++= WebHelper.listToHtml(dirs.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write("<h2>Local Keys</h2>" + buf.toString)
            context.stop(self)

          case "public" =>

            val buf = new StringBuilder()

            val keys = Await.result(ask(Core.discoveryProcessor, DiscoverPublicKeys).mapTo[DiscoveredPublicKeys], Config.defaultTimeout)

            val files = keys.filesKeys map publicStructuralFileDisplay
            val dirs = keys.directoriesKeys map publicStructuralDirDisplay

            buf ++= WebHelper.listToHtml(files.toList, "fa-ul")
            buf ++= WebHelper.listToHtml(dirs.toList, "fa-ul")

            event.response.contentType = "text/html"
            event.response.write("<h2>Public Keys</h2>" + buf.toString)
            context.stop(self)
        }
      }
    case event : HttpRequestEvent =>

      val body = WebTemplate.createHtml(Map[String, String](), "keys.template.html")

      val params = ("Key Database", "<i class=\"fa fa-database\"></i> Key Database", "Lists all the keys, used for the rights and the structure;", body)
      val html = WebTemplate.createHtml(params)

      event.response.contentType = "text/html"
      event.response.write(html)
      context.stop(self)
  }
}