package main.scala

import java.nio.file.Paths

import scala.collection.mutable.LinkedList

import akka.actor.{ ActorRef, ActorSystem, Props }
import filesystem.repo.LocalFileSystemRepo
import main.scala.crypto.{ AutomaticLocalIdentity, CryptoToolBox, MasterPasswordManager }
import main.scala.crypto.SHA256_AESCBC_RSAPSS_RSAOAEP_CryptoPrimitives
import main.scala.db.engine.FileBasedDatabase
import main.scala.filesystem.{ DirectoryWatcher, WorkingCopyChanges }
import main.scala.processors.{ CommitProcessor, DiscoveryProcessor, KeyHarvesterProcessor, LogsProcessor }
import main.scala.webserver.ServerRouterCore

object Core {
  type SuccessStatusWithMessage = (Boolean, String)

  println("Init...")
  var logger : ActorRef = null

  //just transmit to logger if it exists, otherwise store & transmit later
  val logBuffer : LinkedList[String] = new scala.collection.mutable.LinkedList()

  /**
    * Either does directly Core log msg, or if logger doesn't exist, buffer until so
    */
  def log(msg : String) =
    {
      println(msg)
      if (logger == null) {
        logBuffer append LinkedList(msg)
      }
      else {
        if (logBuffer.count(p => true) > 0) {
          logBuffer.foreach(s => Core.logger ! s)
          logBuffer.dropWhile(p => true)
        }
        Core.logger ! msg
      }
    }

  CryptoToolBox.init();

  //Models
  val repoSource = new LocalFileSystemRepo(Config.repo_source, Config.isUnix)
  val repoDest = new LocalFileSystemRepo(Config.repo_dest, Config.isUnix)
  val localDb = new FileBasedDatabase(Config.db_source, Config.isUnix)
  val remoteDb = new FileBasedDatabase(Config.db_dest, Config.isUnix)
  val crypto = new SHA256_AESCBC_RSAPSS_RSAOAEP_CryptoPrimitives()
  var identity = new AutomaticLocalIdentity(localDb, remoteDb, crypto)

  /**
    * Reloads the best local identity
    */
  def reloadIdentity(prefered : String) = {
    this.identity = new AutomaticLocalIdentity(localDb, remoteDb, crypto, prefered)
  }

  //Master encryption
  val masterPasswordManager = new MasterPasswordManager(crypto)

  //Actors
  val actorSystem = ActorSystem("SecureCloudActorSystem")
  val commitProcessor = actorSystem.actorOf(Props(new CommitProcessor(repoSource, repoDest, localDb, remoteDb, crypto)))
  val discoveryProcessor = actorSystem.actorOf(Props(new DiscoveryProcessor(repoSource, repoDest, localDb, remoteDb, crypto)))
  logger = actorSystem.actorOf(Props[LogsProcessor])
  val workingCopyChanges = actorSystem.actorOf(Props(new WorkingCopyChanges(repoSource)))
  val keyHarvester = actorSystem.actorOf(Props(new KeyHarvesterProcessor(repoSource, repoDest, localDb, remoteDb, crypto)))

  //directory watcher init
  val watcherThread = new Thread(new DirectoryWatcher(Paths.get(Config.repo_source), true))
  //watcherThread.start()

  //Preparing ActorSystem shutdown
  Runtime.getRuntime.addShutdownHook(new Thread {
    override def run { Core.actorSystem.shutdown() }
  })

  def main(args : Array[String]) =
    {
      println("Running main")
      //Take care of arguments

      if (args.length == 0) {
        println("""
			    Usage: program  --op OP_NAME
			  """)
      }

      var arglist = args.toList
      type OptionMap = Map[Symbol, Any]

      def nextOption(map : OptionMap, list : List[String]) : OptionMap = {
        if (list.isEmpty)
          return map

        def isSwitch(s : String) = (s(0) == '-')
        list match {
          case Nil => map
          case "--op" :: value :: tail =>
            nextOption(map ++ Map('op -> value), tail)
          /*
        case "--min-size" :: value :: tail =>
          nextOption(map ++ Map('minsize -> value.toInt), tail)
        case string :: opt2 :: tail if isSwitch(opt2) =>
          nextOption(map ++ Map('infile -> string), list.tail)
        case string :: Nil => nextOption(map ++ Map('infile -> string), list.tail)
        case option :: tail =>
          println("Unknown option " + option)
          exit(1)
          *
          */
          case "--file" :: value :: tail =>
            nextOption(map ++ Map('file -> value), tail)
          case "--anyinfo" :: value :: tail =>
            nextOption(map ++ Map('anyinfo -> value), tail)
          case _ => map
        }
      }

      val options = nextOption(Map(), arglist)
      if (!arglist.isEmpty)
        arglist = arglist.tail.tail
      val params = nextOption(Map(), arglist)

      println("#################################################################################################");
      println("-------------------------------------------------------------------------------------------------");

      val fileString = options.getOrElse('file, "").toString
      val anyInfo = options.getOrElse('anyinfo, "").toString

      options getOrElse ('op, 'NO_OP) match {
        case "test-db-ser"             => Tests.dbRandomReadWriteTest()
        case "test-file-rights"        => Tests.writeRightsTest()
        case "test-crypto-init"        => CryptoToolBox.init()
        case "test-crypto-algos"       => CryptoToolBox.printAlgorithms(s => if (!anyInfo.equals("")) s.contains(anyInfo) else true)
        case "test-crypto-algo-exists" => CryptoToolBox.cipherExists(anyInfo)
        case "test-sign-pss"           => Tests.signRsassaPssTest()
        case "test-rsa-oaep"           => Tests.rsaOaepTest()
        case "test-aes-ofb"            => Tests.bouncyCastleAesOFBTest()
        case "test-aes-cbc"            => Tests.bouncyCastleAesCBCTest()
        case "test-crypto-hybrid"      => Tests.cryptoHybridTest()
        case "test-dir-watch"          => Tests.directoryWatchTest()
        case "test-base-64"            => Tests.base64SafeTest()
        case "test-aes-file"           => Tests.aesFilesTest()
        case "test-lfs-repo"           => Tests.localFileSystemRepoTest()
        case "test-fb-db"              => Tests.fileBasedDatabaseTest()
        case "test-commit-new"         => Tests.fileCommitNewFileTest(fileString)
        case "test-commit-edit"        => Tests.fileCommitNewVersionTest(fileString)
        case "test-commit-del"         => Tests.fileCommitDeletionTest(fileString)
        case "test-commit-newdir"      => Tests.dirCommitTest(fileString)
        case "test-repo-read"          => Tests.repoReadTest()
        case "test-repo-clean"         => Tests.repoClean()
        case "test-benchmark"          => Tests.benchmark(fileString)
        case "test-master-enc"         => this.masterPasswordManager.encryptAllKeysInSourceDb(anyInfo)
        case "test-master-dec"         => this.masterPasswordManager.decryptAllKeysInSourceDb(anyInfo)
        case "test-master" =>
          this.masterPasswordManager.encryptAllKeysInSourceDb(anyInfo)
          this.masterPasswordManager.decryptAllKeysInSourceDb(anyInfo)
        case "test-master-key" =>
          this.masterPasswordManager.testKey(anyInfo)
        case "start-server" => ServerRouterCore.start()

        case 'NO_OP =>
          println("No operation provided, starting GUI Web server"); ServerRouterCore.start()
        //println("Please provide an operation using --op OP_NAME"); exit(0)
        case _ => println("Operation not matching"); exit(1)
      }

      println("-------------------------------------------------------------------------------------------------");
      println("#################################################################################################");
    }
}