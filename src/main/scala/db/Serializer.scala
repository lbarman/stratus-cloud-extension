package main.scala.db

import java.io.{File, FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import main.scala.common.FileToolBox

object Serializer {

  def serialize[T](fileName : String, obj : T) : Boolean =
    {
      if (!FileToolBox.fileWritable(fileName)) return false;

      var store : ObjectOutputStream = null
      try {
        store = new ObjectOutputStream(new FileOutputStream(new File(fileName), false))
        store.writeObject(obj)
        true
      }
      catch {
        case _ : Throwable => return false;
      }
      finally {
        store.close
      }
    }

  def deserialize[T](fileName : String) : Option[T] =
    {
      if (FileToolBox.fileExists(fileName)) {
        var in : ObjectInputStream = null
        var in_inner : FileInputStream = null
        try {
          in_inner = new FileInputStream(fileName)
          in = new ObjectInputStream(in_inner) {
            override def resolveClass(desc : java.io.ObjectStreamClass) : Class[_] = {
              try { Class.forName(desc.getName, false, getClass.getClassLoader) }
              catch { case ex : ClassNotFoundException => super.resolveClass(desc) }
            }
          }
          val obj = in.readObject().asInstanceOf[T]
          Some(obj)
        }
        catch {
          case e : Exception => None;
        }
        finally {
          if (in != null) in.close
          if (in_inner != null) in_inner.close
        }
      }
      else None
    }
}