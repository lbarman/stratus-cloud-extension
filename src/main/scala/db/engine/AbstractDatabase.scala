package main.scala.db.engine

trait AbstractDatabase {

  //database access
  def insertEntry(e : DatabaseEntry) : Boolean
  def deleteEntry(e : DatabaseEntry) : Boolean
  def getEntriesPIDB() : Array[PublicIdentityDatabaseEntry]
  def getEntriesPSFDB() : Array[PublicStructuralFileDatabaseEntry]
  def getEntriesPSDDB() : Array[PublicStructuralDirDatabaseEntry]
  def getEntriesLKFDB() : Array[LocalKeyFileDatabaseEntry]
  def getEntriesLKDDB() : Array[LocalKeyDirDatabaseEntry]
  def getEntriesPMDB() : Array[PublicMessageDatabaseEntry]

  def getEntryPIDB(addressing : String) : Option[PublicIdentityDatabaseEntry]
  def getEntryPSFDB(addressing : String) : Option[PublicStructuralFileDatabaseEntry]
  def getEntryPSDDB(addressing : String) : Option[PublicStructuralDirDatabaseEntry]
  def getEntryLKFBD(addressing : String) : Option[LocalKeyFileDatabaseEntry]
  def getEntryLKDBD(addressing : String) : Option[LocalKeyDirDatabaseEntry]
  def getEntryPMDB(addressing : String) : Option[PublicMessageDatabaseEntry]

}