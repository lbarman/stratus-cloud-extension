package main.scala.db.engine

import main.scala.db.engine.DatabaseField.{String2DatabaseField, Tuple2DatabaseField}

@scala.serializable
class PublicStructuralFileDatabaseEntry extends DatabaseEntry {

  //COMMON
  override val entryType : DatabaseField = ("Public Structural File DBE", FieldStatus.EntryType)

  var filename_path_version : DatabaseField = ""
  var isDeletion : DatabaseField = ("no", FieldStatus.Plain)

  // KEYS
  //K_F is stored in the Local Key File Database Entry, IV_F is here
  var iv : DatabaseField = ""

  var KWritePub : DatabaseField = ""
  var kwritepub_tag : DatabaseField = ""

  //metadata
  var metadata_aes : DatabaseField = ""
  var metadata_plain : DatabaseField = ""

  //discover repo structure
  var hash_directory_path : DatabaseField = ""
  var hash_version_one : DatabaseField = ""

  //signatures 
  var sign_v0_tag : DatabaseField = ""
  var sign_v0_publickey : DatabaseField = ""
  var sign_vN_tag : DatabaseField = ""
  var sign_vN_publickey : DatabaseField = ""

  //cascading rights
  var cCascRead : DatabaseField = ""
  var cCascWrite : DatabaseField = ""

  override def toString() : String = {

    val separator = " || ";
    isDeletion.getValue match {

      case "yes" =>
        entryType + separator + "DELETION" + separator +
          addressing + separator +
          isDeletion + separator +
          filename_path_version

      case _ =>
        entryType + separator + "FILE" + separator +
          addressing + separator +
          isDeletion + separator +
          filename_path_version + separator +
          hash_directory_path + separator +
          iv + separator +
          KWritePub + separator +
          metadata_aes + separator +
          metadata_plain + separator +
          sign_v0_tag + separator +
          sign_v0_publickey + separator +
          sign_vN_tag + separator +
          sign_vN_publickey + separator +
          cCascRead + separator +
          cCascWrite
    }
  }
  override def getAcronym() = "psfdbe"

}