package main.scala.db.engine

import main.scala.db.engine.DatabaseField.{String2DatabaseField, Tuple2DatabaseField}

@scala.serializable
class LocalKeyDirDatabaseEntry extends DatabaseEntry {

  override val entryType : DatabaseField = ("Local Key Dir DBE", FieldStatus.EntryType)

  //K_D, used for symmetric encrypt
  var K : DatabaseField = ""

  //KInitPriv_D, used to sign the first version of a file inside this directory
  var KInitPriv : DatabaseField = ""

  //KCascadeReadPriv_D, used to decrypt, inside this directory, the various K_F for each file, and the K_D, for each directory
  var KCascadeReadPriv : DatabaseField = ""

  //KCacadeWritePriv_D, used to decrypt, inside this directory, this various KWritePriv_F for each file, and the various KInitPriv_D for each directory
  var KCascadeWritePriv : DatabaseField = ""

  override def toString() : String = {
    val separator = " || ";
    entryType + separator +
      addressing + separator +
      K + separator +
      KInitPriv + separator +
      KCascadeReadPriv + separator +
      KCascadeWritePriv
  }
  override def getAcronym() = "lkddbe"

  def toXml = {
    <SecureCloudRepo>
      <addressing>{ addressing.getValue }</addressing>
      <k>{ K.getValue }</k>
      <kinitpriv>{ KInitPriv.getValue }</kinitpriv>
      <kcascadereadpriv>{ KCascadeReadPriv.getValue }</kcascadereadpriv>
      <kcascadewritepriv>{ KCascadeWritePriv.getValue }</kcascadewritepriv>
    </SecureCloudRepo>

  }
}

object LocalKeyDirDatabaseEntry extends DatabaseEntry {

  // (b) convert XML to a Stock
  def fromXml(node : scala.xml.Node) : LocalKeyDirDatabaseEntry = {
    val e = new LocalKeyDirDatabaseEntry

    e.addressing = ((node \ "addressing").text, FieldStatus.Hash)
    e.K = ((node \ "k").text, FieldStatus.AES_Key)
    e.KInitPriv = ((node \ "kinitpriv").text, FieldStatus.RSA_Private_Key)

    e.KCascadeReadPriv = ((node \ "kcascadereadpriv").text, FieldStatus.RSA_Private_Key)
    e.KCascadeWritePriv = ((node \ "kcascadewritepriv").text, FieldStatus.RSA_Private_Key)

    e
  }
}