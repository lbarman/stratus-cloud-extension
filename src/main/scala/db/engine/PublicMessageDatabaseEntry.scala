package main.scala.db.engine

import main.scala.db.engine.DatabaseField.{String2DatabaseField, Tuple2DatabaseField}

@scala.serializable
class PublicMessageDatabaseEntry extends DatabaseEntry {

  override val entryType : DatabaseField = ("Public Message DBE", FieldStatus.EntryType)

  var from : DatabaseField = ""
  var to : DatabaseField = ""

  var contentType : DatabaseField = ""
  var contentName : DatabaseField = ""
  var contentDesc : DatabaseField = ""
  var contentKeys : DatabaseField = ""

  var iv : DatabaseField = ""
  var contentLength : DatabaseField = ""
  var cipherSymm : DatabaseField = ""
  var cipherAsymm : DatabaseField = ""
  var timestamp : DatabaseField = ""

  var sign_tag : DatabaseField = ""
  var sign_pk : DatabaseField = ""

  override def toString() : String = {
    val separator = " || ";
    entryType + separator +
      from + separator +
      to + separator +
      contentType + separator +
      contentName + separator +
      contentDesc + separator +
      contentKeys + separator +
      contentLength + separator +
      iv + separator +
      cipherSymm + separator +
      cipherAsymm + separator +
      timestamp
  }
  override def getAcronym() = "pmdbe"

}