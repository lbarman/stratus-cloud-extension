package main.scala.db.engine

import main.scala.db.engine.DatabaseField.{String2DatabaseField, Tuple2DatabaseField}

@scala.serializable
class LocalKeyFileDatabaseEntry extends DatabaseEntry {

  override val entryType : DatabaseField = ("Local Key File DBE", FieldStatus.EntryType)

  //K_F, used for AES_CBC
  var K : DatabaseField = ""

  //KWritePriv_F, used for signature
  var KWritePriv : DatabaseField = ""

  override def toString() : String = {
    val separator = " || ";
    entryType + separator +
      addressing + separator +
      K + separator +
      KWritePriv
  }
  override def getAcronym() = "lkfdbe"

  def toXml = {
    <SecureCloudRepo>
      <addressing>{ addressing.getValue }</addressing>
      <k>{ K.getValue }</k>
      <kwritepriv>{ KWritePriv.getValue }</kwritepriv>
    </SecureCloudRepo>
  }
}

object LocalKeyFileDatabaseEntry extends DatabaseEntry {

  // (b) convert XML to a Stock
  def fromXml(node : scala.xml.Node) : LocalKeyFileDatabaseEntry = {
    val e = new LocalKeyFileDatabaseEntry

    e.addressing = ((node \ "addressing").text, FieldStatus.Hash)
    e.K = ((node \ "k").text, FieldStatus.AES_Key)
    e.KWritePriv = ((node \ "kwritepriv").text, FieldStatus.RSA_Private_Key)

    e
  }
}
