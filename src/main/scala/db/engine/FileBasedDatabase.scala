package main.scala.db.engine

import java.io.File

import scala.Array.canBuildFrom
import scala.Option.option2Iterable

import main.scala.Core
import main.scala.common.FileToolBox
import main.scala.db.Serializer

class FileBasedDatabase(var rootFolder : String, val isUnix : Boolean = false) extends AbstractDatabase {

  //normalize path
  if (isUnix && !rootFolder.endsWith("/")) rootFolder += '/';
  if (!isUnix && !rootFolder.endsWith("\\")) rootFolder += '\\';

  //create folders if needed
  val rootFolderFile = new File(rootFolder);
  try {
    if (!rootFolderFile.isDirectory()) {
      rootFolderFile.mkdirs();
    }
  }
  catch {
    case _ : Throwable => None;
  }
  finally {
    assert(rootFolderFile.isDirectory())
  }

  def absolutePath(subpath : String) = rootFolder + subpath

  def insertEntry(e : DatabaseEntry) : Boolean =
    {
      if (!new File(rootFolder).isDirectory()) {
        false
      }
      else {
        val v = "[FileBased-DB] Writing new entry in " + absolutePath(e.getAcronym + "_" + e.getAddressing().getValue) + " ===> \n" + e + "\n[EOF]"
        Core log v

        Serializer.serialize(absolutePath(e.getAcronym + "_" + e.getAddressing().getValue), e);
      }
    }

  def deleteEntry(e : DatabaseEntry) : Boolean =
    {
      if (!new File(rootFolder).isDirectory()) {
        false
      }
      else {
        val path = absolutePath(e.getAcronym + "_" + e.getAddressing().getValue)

        if (FileToolBox.fileExists(path) && FileToolBox.fileWritable(path)) {
          Core log "[FileBased-DB] Deleting entry " + absolutePath(e.getAcronym + "_" + e.getAddressing().getValue)
          new File(path).delete()
        }
        else {
          Core log "[FileBased-DB] [Error] Can't delete entry " + absolutePath(e.getAcronym + "_" + e.getAddressing().getValue) + ", insufficient rights"
          false
        }
      }
    }

  def getPathToEntry(prefixAndAddr : String) : String = absolutePath(prefixAndAddr)

  def getEntriesInDir(prefix : String) =
    if (new File(rootFolder).list() == null)
      Array.fill(0)("")
    else
      (new File(rootFolder)).list().filter(_.startsWith(prefix))

  def getEntriesPIDB() : Array[PublicIdentityDatabaseEntry] =
    getEntriesInDir("pidbe_").map(x => Serializer.deserialize(absolutePath(x))).filter(!_.isEmpty).flatten

  def getEntriesPSFDB() : Array[PublicStructuralFileDatabaseEntry] =
    getEntriesInDir("psfdbe_").map(x => Serializer.deserialize(absolutePath(x))).filter(!_.isEmpty).flatten

  def getEntriesPSDDB() : Array[PublicStructuralDirDatabaseEntry] =
    getEntriesInDir("psddbe_").map(x => Serializer.deserialize(absolutePath(x))).filter(!_.isEmpty).flatten

  def getEntriesLKFDB() : Array[LocalKeyFileDatabaseEntry] =
    getEntriesInDir("lkfdbe_").map(x => Serializer.deserialize(absolutePath(x))).filter(!_.isEmpty).flatten

  def getEntriesLKDDB() : Array[LocalKeyDirDatabaseEntry] =
    getEntriesInDir("lkddbe_").map(x => Serializer.deserialize(absolutePath(x))).filter(!_.isEmpty).flatten

  def getEntriesPMDB() : Array[PublicMessageDatabaseEntry] =
    getEntriesInDir("pmdbe_").map(x => Serializer.deserialize(absolutePath(x))).filter(!_.isEmpty).flatten

  def getEntryPIDB(addressing : String) : Option[PublicIdentityDatabaseEntry] =
    getEntriesPIDB.filter(e => e.addressing.getValue == addressing).headOption

  def getEntryPSFDB(addressing : String) : Option[PublicStructuralFileDatabaseEntry] =
    getEntriesPSFDB.filter(e => e.addressing.getValue == addressing).headOption

  def getEntryPSDDB(addressing : String) : Option[PublicStructuralDirDatabaseEntry] =
    getEntriesPSDDB.filter(e => e.addressing.getValue == addressing).headOption

  def getEntryLKFBD(addressing : String) : Option[LocalKeyFileDatabaseEntry] =
    getEntriesLKFDB.filter(e => e.addressing.getValue == addressing).headOption

  def getEntryLKDBD(addressing : String) : Option[LocalKeyDirDatabaseEntry] =
    getEntriesLKDDB.filter(e => e.addressing.getValue == addressing).headOption

  def getEntryPMDB(addressing : String) : Option[PublicMessageDatabaseEntry] =
    getEntriesPMDB.filter(e => e.addressing.getValue == addressing).headOption
}