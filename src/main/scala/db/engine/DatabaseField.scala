package main.scala.db.engine

object FieldStatus extends Enumeration {
  type FieldStatus = Value
  val EntryType, Plain, AES, RSA, Hash, AES_Key, RSA_Public_Key, RSA_Private_Key, Hybrid = Value
}

import FieldStatus._

@scala.serializable
class DatabaseField(value : String = "", status : FieldStatus = FieldStatus.Plain) {

  override def toString() = "{" + status + ": " + value + "}";
  def getValue() = value;
  def getStatus() = status;

  override def equals(o : Any) = o match {
    case that : DatabaseField => that.toString().equals(this.toString())
    case _                    => false
  }
  override def hashCode = this.toString().hashCode

}

object DatabaseField {
  implicit def String2DatabaseField(value : String) : DatabaseField = new DatabaseField(value, FieldStatus.Plain)
  implicit def DatabaseField2String(df : DatabaseField) : String = if (df == null) "" else df.getValue()
  implicit def Tuple2DatabaseField(both : Tuple2[String, FieldStatus]) : DatabaseField = new DatabaseField(both._1, both._2)
  implicit def DatabaseField2Tuple(df : DatabaseField) : Tuple2[String, FieldStatus] = (df.getValue(), df.getStatus())
}