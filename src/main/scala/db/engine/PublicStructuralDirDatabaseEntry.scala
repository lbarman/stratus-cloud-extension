package main.scala.db.engine

import main.scala.db.engine.DatabaseField.{String2DatabaseField, Tuple2DatabaseField}

@scala.serializable
class PublicStructuralDirDatabaseEntry extends DatabaseEntry {

  //COMMON
  override val entryType : DatabaseField = ("Public Structural Dir DBE", FieldStatus.EntryType)

  var filename_path_version : DatabaseField = ""

  //K""D is stored in the Local Key Dir Database Entry, IV""D is here
  var iv : DatabaseField = ""

  //discover repo structure
  var hash_directory_path : DatabaseField = ""

  //metadata
  var metadata_aes : DatabaseField = ""
  var metadata_plain : DatabaseField = ""

  //inner keys
  var KInitPubD : DatabaseField = ""
  var KCascadeReadPub : DatabaseField = ""
  var KCascadeWritePub : DatabaseField = ""

  //signature 
  var sign_v0_tag : DatabaseField = ""
  var sign_v0_publickey : DatabaseField = ""

  //cascading rights (from above)
  var cCascK : DatabaseField = ""
  var cCascRead : DatabaseField = ""
  var cCascWrite : DatabaseField = ""

  override def toString() : String = {

    val separator = " || ";

    entryType + separator + "DIRECTORY" + separator +
      addressing + separator +
      filename_path_version + separator +
      hash_directory_path + separator +
      iv + separator +
      metadata_aes + separator +
      metadata_plain + separator +
      KInitPubD + separator +
      KCascadeReadPub + separator +
      KCascadeWritePub + separator +
      sign_v0_tag + separator +
      sign_v0_publickey + separator +
      cCascK + separator +
      cCascRead + separator +
      cCascWrite
  }
  override def getAcronym() = "psddbe"

}