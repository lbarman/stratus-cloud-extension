package main.scala.db.engine

import main.scala.db.engine.DatabaseField.Tuple2DatabaseField

@scala.serializable
class DatabaseEntry() {

  val entryType : DatabaseField = ("Abstract DBE", FieldStatus.EntryType)
  var addressing : DatabaseField = _

  def getAddressing() = addressing
  def getAcronym() = "de"

  override def equals(o : Any) = o match {
    case that : DatabaseEntry => that.addressing.equals(this.addressing)
    case _                    => false
  }
  override def hashCode = addressing.hashCode

  override def toString() : String = {
    val separator = " || ";
    entryType + separator +
      addressing
  }
}