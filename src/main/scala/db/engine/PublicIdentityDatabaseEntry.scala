package main.scala.db.engine

import main.scala.db.engine.DatabaseField.{String2DatabaseField, Tuple2DatabaseField}

@scala.serializable
class PublicIdentityDatabaseEntry extends DatabaseEntry {

  override val entryType : DatabaseField = ("Public Identity DBE", FieldStatus.EntryType)
  var owner : DatabaseField = ""
  var pcName : DatabaseField = ""
  var pcNameReason : DatabaseField = ""
  var published_on : DatabaseField = ""
  var public_key : DatabaseField = ""
  var private_key : DatabaseField = ""
  var signature : DatabaseField = ""

  override def toString() : String = {
    val separator = " || ";
    entryType + separator +
      addressing + separator +
      owner + separator +
      published_on + separator +
      public_key + separator +
      private_key + separator +
      signature
  }
  override def getAcronym() = "pidbe"

}