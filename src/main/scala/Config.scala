package main.scala

import java.awt.Desktop
import java.io.File

import scala.concurrent.duration.DurationInt
import scala.xml.XML

import main.scala.common.FileToolBox

object Config {

  var loadedConfig = new Config

  //path
  def unix_dir_separator = loadedConfig.unix_dir_separator
  def win_dir_separator = loadedConfig.win_dir_separator

  def isUnix = loadedConfig.isUnix

  //crypto
  def refreshRandomnessEveryXCalls = loadedConfig.refreshRandomnessEveryXCalls
  def aesKeySizeBits = loadedConfig.aesKeySizeBits
  def aesIVSizeBits = loadedConfig.aesIVSizeBits
  def signKeySizeBits = loadedConfig.signKeySizeBits

  //repos
  def repo_source = loadedConfig.repo_source
  def repo_dest = loadedConfig.repo_dest
  def db_source = loadedConfig.db_source
  def db_dest = loadedConfig.db_dest

  //timeout
  def defaultTimeout = loadedConfig.defaultTimeout

  /*
	 * Init
	 */

  def tryConfig() =
    {
      val dirList = List(repo_source, repo_dest, db_source, db_dest)
      val wrongDirs = dirList map { d => (d, FileToolBox.dirExists(d)) } filter { p => p._2 == false } map { p => p._1 }

      if (wrongDirs.length == 0) {
        (true, Nil)
      }
      else {
        (false, wrongDirs)
      }
    }

  var configLoaded = tryReadConfig()
  var configOK = tryConfig()
  while (!configLoaded || !configOK._1) {
    def editFile(path : String) {
      try {

        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
          val cmd = "rundll32 url.dll,FileProtocolHandler " + new File(path).getCanonicalPath();
          Runtime.getRuntime().exec(cmd);
        }
        else {
          Desktop.getDesktop().edit(new File(path));
        }
      }
      catch {
        case e : Throwable =>
          "Editing isn't supported on this system. Please edit manually " + path
      }
    }

    println("++++++++++++++++++++++++++++")
    println("Interaction Required")
    println("++++++++++++++++++++++++++++")
    println()
    val text =
      if (!configLoaded) "Can't load configuration. Would you like to create it, and edit it with a text editor now ? Otherwise the program will stop."
      else if (!configOK._1) {
        "Your configuration seems invalid; At least one of the indicated path does not exist, or is not writable. Would you like to open a text editor now ?" +
          "Otherwise the program will stop." +
          "Unaccessible Directories : " +
          configOK._2 mkString ", "

      }
      else "All good now, press R to retry one last time."

    println(text)
    println("Y: yes, edit config, R: retry, N: quit")
    val input = System.console().readLine();

    if (input.trim().toLowerCase().equals("y")) {
      if (!FileToolBox.fileExists(configFilePath)) {
        saveConfig(loadedConfig)
      }
      editFile(configFilePath)
      println("Press any key when it's done.")
      System.console().readLine();
    }
    else if (input.trim().toLowerCase().equals("r")) {
      configLoaded = tryReadConfig()
      configOK = tryConfig()
    }
    else {
      println("Your call.")
      println("Exiting....")
      System.exit(1)
    }
  }

  /*
	 * Operations
	 */

  def getCurrentDirectory = new java.io.File(".").getCanonicalPath
  def osFileSeparator = File.separator
  def configFilePath = getCurrentDirectory + osFileSeparator + "config.xml"

  //serialize config
  def saveConfig() : Boolean = saveConfig(this.loadedConfig)

  def saveConfig(e : Config) : Boolean = {
    println("[Config] Writing config here : " + configFilePath + " ===> \n" + e + "\n[EOF]")

    val p = new java.io.PrintWriter(configFilePath)
    try {
      p.write(e.toXml.toString)
      true
    }
    catch {
      case e : Exception => false
    }
    finally {
      p.close()
    }
  }

  //delete config
  def deleteConfig() : Boolean =
    {
      if (FileToolBox.fileExists(configFilePath) && FileToolBox.fileWritable(configFilePath)) {
        println("[Config] Deleting config " + configFilePath)
        new File(configFilePath).delete()
      }
      else {
        Core log ("[Config] [Error] Can't delete config " + configFilePath + ", insufficient rights.")
        false
      }
    }

  //gets the configuration
  def tryReadConfig() : Boolean = {
    if (FileToolBox.fileExists(configFilePath)) {

      try {

        val testTxtSource = scala.io.Source.fromFile(configFilePath)
        val text = testTxtSource.mkString
        testTxtSource.close()

        loadedConfig = Config.fromXml(XML.loadString(text))
        //println("[Config] Loaded config is '" + loadedConfig + "'.")
        true
      }
      catch {
        case e : Exception =>
          Core log "[Config] [Info] Can't read config file " + configFilePath + " (Exception :'" + e + "'), default fallback mode."
          false
      }
    }
    else {
      false
    }
  }

  // (b) convert XML to a Stock
  def fromXml(node : scala.xml.Node) : Config = {
    val config = new Config

    config.isUnix = (node \ "isUnix").text.toBoolean

    config.aesKeySizeBits = (node \ "aesKeySizeBits").text.toInt
    config.aesIVSizeBits = (node \ "aesIVSizeBits").text.toInt
    config.signKeySizeBits = (node \ "signKeySizeBits").text.toInt
    config.refreshRandomnessEveryXCalls = (node \ "refreshRandomnessEveryXCalls").text.toInt

    config.repo_source = (node \ "repoSource").text
    config.repo_dest = (node \ "repoDest").text
    config.db_source = (node \ "dbSource").text
    config.db_dest = (node \ "dbDest").text

    config.defaultTimeout = (node \ "defaultTimeout").text.toInt seconds

    Core log "[Config][Info] configuration loaded :" + config.toString

    config
  }
}

@scala.serializable
class Config() {

  //path
  val unix_dir_separator = "/"
  val win_dir_separator = "\\"
  var isUnix = false

  //crypto
  var aesKeySizeBits = 128
  var aesIVSizeBits = 128
  var signKeySizeBits = 1024
  var refreshRandomnessEveryXCalls = 10

  //repos
  var repo_source = """REPO_SOURCE""";
  var repo_dest = """REPO_DEST""";
  var db_source = """DB_SOURCE""";
  var db_dest = """DB_DEST""";

  //timeout
  var defaultTimeout = (1 * 60) seconds

  def toXml = {
    val timeout = defaultTimeout.toSeconds
    <SecureCloudRepo>
      <isUnix>{ false }</isUnix>
      <refreshRandomnessEveryXCalls>{ refreshRandomnessEveryXCalls }</refreshRandomnessEveryXCalls>
      <aesKeySizeBits>{ aesKeySizeBits }</aesKeySizeBits>
      <aesIVSizeBits>{ aesKeySizeBits }</aesIVSizeBits>
      <signKeySizeBits>{ signKeySizeBits }</signKeySizeBits>
      <repoSource>{ repo_source }</repoSource>
      <repoDest>{ repo_dest }</repoDest>
      <dbSource>{ db_source }</dbSource>
      <dbDest>{ db_dest }</dbDest>
      <defaultTimeout>{ timeout }</defaultTimeout>
    </SecureCloudRepo>
  }

  override def toString() = toXml.toString

}