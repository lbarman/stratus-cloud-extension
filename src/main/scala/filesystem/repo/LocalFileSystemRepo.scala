package main.scala.filesystem.repo

import java.io.{ BufferedInputStream, BufferedOutputStream, File, FileInputStream, FileOutputStream }
import java.nio.file.{ Files, Paths }

import scala.Array.canBuildFrom
import scala.collection.immutable.Stream.consWrapper

import main.scala.Config
import main.scala.common.PathNormalization

class LocalFileSystemRepo(var rootFolder : String, val isUnix : Boolean = false) extends AbstractRepo {

  //normalize path
  if (isUnix && !rootFolder.endsWith(Config.unix_dir_separator)) rootFolder += Config.unix_dir_separator;
  if (!isUnix && !rootFolder.endsWith(Config.win_dir_separator)) rootFolder += Config.win_dir_separator;

  //create folders if needed
  val rootFolderFile = new File(rootFolder);
  try {
    if (!rootFolderFile.isDirectory()) {
      rootFolderFile.mkdirs();
    }
  }
  catch {
    case _ : Throwable => None;
  }
  finally {
    assert(rootFolderFile.isDirectory())
  }

  /**
    * Converts a file in the LocalFileSystem to a path we use within the app
    */
  def pathLFSToInApp(f : String) =
    {
      var work = f

      work = work.replace(rootFolder, "")

      //we use unix slashes in the app
      work = work.replaceAllLiterally(Config.win_dir_separator, Config.unix_dir_separator)
      work = work.replaceAllLiterally(Config.osFileSeparator, Config.unix_dir_separator)

      //add heading /
      work = Config.unix_dir_separator + work

      work
    }

  /**
    * Converts a path within the app to a path in the LocalFileSystem
    */
  def pathInAppToLFS(f : String) =
    {
      var work = f

      //removing heading /
      assert(work.startsWith(Config.unix_dir_separator))
      work = work.substring(1)

      //changes all separator to OS separator

      work =
        if (isUnix) work.replaceAllLiterally(Config.win_dir_separator, Config.unix_dir_separator)
        else work.replaceAllLiterally(Config.unix_dir_separator, Config.win_dir_separator)

      rootFolder + work
    }

  //interacting with folders
  private def getFileTreeForDir(absoluteRootDirectory : File) : Stream[File] =
    absoluteRootDirectory #:: (
      if (absoluteRootDirectory.isDirectory) absoluteRootDirectory.listFiles().toStream.flatMap(getFileTreeForDir)
      else Stream.empty)

  def getFileTreeForDir(relativeRootDirectory : String) : Stream[String] =
    {
      getFileTreeForDir(new File(pathInAppToLFS(relativeRootDirectory))).map((f : File) =>
        {
          val inAppPath = pathLFSToInApp(f.getAbsolutePath)
          PathNormalization.normalizeForInnerUse(inAppPath, true, f.isDirectory)
        })
    }

  def getFilesInDir(relativeRootDirectory : String) : Array[String] =
    {
      val f = new File(pathInAppToLFS(relativeRootDirectory))
      if (!f.exists() && f.isDirectory())
        Array[String]()
      else
        f.listFiles() map ((f : File) => {
          val inAppPath = pathLFSToInApp(f.getAbsolutePath)
          PathNormalization.normalizeForInnerUse(inAppPath, true, f.isDirectory)
        })
    }

  def doesFileExists(relativeInnerPath : String) : Boolean =
    Files.exists(Paths.get(pathInAppToLFS(relativeInnerPath)))

  def getInputStreamForFile(relativeInnerPath : String) =
    new BufferedInputStream(new FileInputStream(pathInAppToLFS(relativeInnerPath)));

  def getOutputStreamForFile(relativeInnerPath : String) =
    {
      val pathLFS = pathInAppToLFS(relativeInnerPath)
      new File(pathLFS.substring(0, pathLFS.lastIndexOf(Config.osFileSeparator) + 1)).mkdirs()
      new BufferedOutputStream(new FileOutputStream(pathLFS))
    }

}