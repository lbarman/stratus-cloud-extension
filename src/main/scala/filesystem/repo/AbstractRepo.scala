package main.scala.filesystem.repo

import java.io.{InputStream, OutputStream}

trait AbstractRepo {

  //paths
  def pathLFSToInApp(f : String) : String
  def pathInAppToLFS(f : String) : String

  //file access
  def doesFileExists(inAppPath : String) : Boolean
  def getInputStreamForFile(inAppPath : String) : InputStream
  def getOutputStreamForFile(inAppPath : String) : OutputStream

  //structure explorer
  def getFileTreeForDir(inAppPath : String) : Stream[String]
  def getFilesInDir(relativeRootDirectory : String) : Array[String]

}