/**
  * dirwatcher.scala
  *
  * Uses the Java 7 WatchEvent filesystem API from within Scala.
  * Adapted from:
  *  http://download.oracle.com/javase/tutorial/essential/io/examples/WatchDir.java
  *
  * @author Chris Eberle <eberle1080@gmail.com>
  * @version 0.1
  *
  * LBARMAN: found here : https://gist.github.com/eberle1080/1241375
  */
package main.scala.filesystem

import java.io.IOException
import java.nio.file.{FileVisitResult, Files, LinkOption, Path, SimpleFileVisitor, StandardWatchEventKinds, WatchEvent, WatchKey}
import java.nio.file.attribute.BasicFileAttributes

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.mutable.HashMap
import scala.util.control.Breaks.{break, breakable}

import akka.actor.actorRef2Scala
import main.scala.Core
import main.scala.common.FileToolBox

class DirectoryWatcher(val path : Path, val recursive : Boolean) extends Runnable {

  val watchService = path.getFileSystem().newWatchService()
  val keys = new HashMap[WatchKey, Path]
  val directories = new scala.collection.mutable.HashSet[String]();
  var trace = false

  /**
    * Print an event
    */
  def printEvent(event : WatchEvent[_]) : Unit = {
    val kind = event.kind
    val event_path = event.context().asInstanceOf[Path]
    if (kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
      println("Entry created: " + event_path)
    }
    else if (kind.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
      println("Entry deleted: " + event_path)
    }
    else if (kind.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
      println("Entry modified: " + event_path)
    }
  }

  /**
    * Register a particular file or directory to be watched
    */
  def register(dir : Path) : Unit = {
    val key = dir.register(watchService,
      StandardWatchEventKinds.ENTRY_CREATE,
      StandardWatchEventKinds.ENTRY_MODIFY,
      StandardWatchEventKinds.ENTRY_DELETE)

    if (trace) {
      val prev = keys.getOrElse(key, null)
      if (prev == null) {
        println("register: " + dir)
      }
      else {
        if (!dir.equals(prev)) {
          println("update: " + prev + " -> " + dir)
        }
      }
    }

    keys(key) = dir
    directories += dir.toAbsolutePath.toString
  }

  /**
    * Makes it easier to walk a file tree
    */
  implicit def makeDirVisitor(f : (Path) => Unit) = new SimpleFileVisitor[Path] {
    override def preVisitDirectory(p : Path, attrs : BasicFileAttributes) = {
      f(p)
      FileVisitResult.CONTINUE
    }
  }

  /**
    *  Recursively register directories
    */
  def registerAll(start : Path) : Unit = {
    Files.walkFileTree(start, (f : Path) => {
      register(f)
    })
  }

  /**
    * The main directory watching thread
    */
  override def run() : Unit = {
    try {
      if (recursive) {
        registerAll(path)
      }
      else {
        register(path)
      }
      Core log "[DirectoryWatcher] Scanning .... Done."

      trace = true

      breakable {
        while (true) {

          Core log "[DirectoryWatcher] Service running..."

          val key = watchService.take()
          val dir = keys.getOrElse(key, null)

          if (dir != null) {
            key.pollEvents().asScala.foreach(event => {
              val kind = event.kind

              if (kind != StandardWatchEventKinds.OVERFLOW) {
                printEvent(event)
                val name = event.context().asInstanceOf[Path]
                var child = dir.resolve(name)

                //resolve path
                val dirPath = key.watchable().asInstanceOf[Path]
                val context = event.context().asInstanceOf[Path]
                val fullPath = dirPath.resolve(context).toAbsolutePath

                //transmit the information to WCC
                if (event.kind.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
                  //in deletion cases, we need to differentiate from files and directories
                  Core log s"[DirectoryWorker] sending new deletion event $fullPath, isDir=" + directories.contains(fullPath.toString) + " to WCC"

                  if (directories.contains(fullPath.toString))
                    Core.workingCopyChanges ! new DirDeletionEvent(fullPath.toString)
                  else
                    Core.workingCopyChanges ! new FileDeletionEvent(fullPath.toString)

                }
                else if (event.kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
                  //in creation cases, it's straightforward
                  Core log s"[DirectoryWorker] sending new creation event $fullPath, isDir=" + FileToolBox.dirExists(fullPath.toString) + " to WCC"

                  if (FileToolBox.dirExists(fullPath.toString))
                    Core.workingCopyChanges ! new DirCreationEvent(fullPath.toString)
                  else
                    Core.workingCopyChanges ! new FileCreationOrModificationEvent(fullPath.toString)
                }

                else if (event.kind.equals(StandardWatchEventKinds.ENTRY_MODIFY) && !directories.contains(fullPath.toString)) {
                  //in modification cases, we're only interested in files
                  Core log s"[DirectoryWorker] sending new modification event $fullPath to WCC"
                  Core.workingCopyChanges ! new FileCreationOrModificationEvent(fullPath.toString)
                }

                //When a directory is deleted, remove its key
                if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
                  try {
                    if (directories.contains(fullPath.toAbsolutePath.toString)) {
                      directories -= fullPath.toAbsolutePath.toString
                      keys.remove(key)
                    }
                  }
                  catch {
                    case ioe : IOException => println("IOException: " + ioe)
                    case e : Exception =>
                      println("Exception: " + e)
                      break
                  }
                }

                //when a directory is created, add it to the watchlist
                if (recursive && (kind == StandardWatchEventKinds.ENTRY_CREATE)) {
                  try {
                    if (Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS)) {
                      registerAll(child);
                    }
                  }
                  catch {
                    case ioe : IOException => println("IOException: " + ioe)
                    case e : Exception =>
                      println("Exception: " + e)
                      break
                  }
                }
              }
            })

          }
          else {
            Core log "[DirectoryWatcher] [Error] Watchkey not recognized !"
          }

          if (!key.reset()) {
            keys.remove(key);
            if (keys.isEmpty) {
              break
            }
          }
        }
      }
      Core log "[DirectoryWatcher] Shutting down."

    }
    catch {
      case ie : InterruptedException => println("InterruptedException: " + ie)
      case ioe : IOException         => println("IOException: " + ioe)
      case e : Exception             => println("Exception: " + e)
    }
  }
}