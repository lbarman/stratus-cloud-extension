package main.scala.filesystem

import scala.Array.canBuildFrom

import org.apache.commons.io.FilenameUtils

import main.scala.crypto.KeyDefs.KeyDefs
import main.scala.db.engine.{LocalKeyFileDatabaseEntry, PublicStructuralFileDatabaseEntry}

class AbstractFile(addr_path_name_version_ext : String,
                   metadata : String,
                   keys_input : Map[KeyDefs, String],
                   publicFileKeyBDE : PublicStructuralFileDatabaseEntry,
                   localFileKeyBDE : LocalKeyFileDatabaseEntry,
                   acceptedIdentity : Option[(String, String)],
                   deletionTag : Boolean = false) {

  def isDeletion = this.deletionTag;

  def splitPathAndFileName(input : String) : Array[String] = Array(FilenameUtils.getFullPath(input), FilenameUtils.getBaseName(input), FilenameUtils.getExtension(input))

  def vettedIdentity = acceptedIdentity
  def keys() = keys_input
  def version() = infos('version)
  def signatureTag() = ""
  def signaturePK() = ""
  def publicDatabaseEntry() = publicFileKeyBDE
  def privateDatabaseEntry() = localFileKeyBDE

  def explodePathVersionExtField(input : String) = {
    val parts = input.replaceAllLiterally("[", "").replaceAllLiterally("]", "").trim().split('|')
    Array('addressing, 'fullpath, 'version, 'timestamp, 'owner, 'filepath, 'filename, 'extention).zip(parts ++ splitPathAndFileName(parts(1))).toMap
  }

  val infos = explodePathVersionExtField(addr_path_name_version_ext)

  def isKeyEmpty(key : KeyDefs) = keys().get(key).isEmpty || keys().get(key).get.isEmpty()

  def isKeyEmptyOrZero(key : KeyDefs) = isKeyEmpty(key) || keys().get(key).get.equals("0")

  override def toString() = "[" + infos.map(x => x._1.name + "=" + x._2).mkString(", ") + "]<<" + metadata + ">>"
}