package main.scala.filesystem

import scala.Array.canBuildFrom

import org.apache.commons.io.FilenameUtils

import main.scala.crypto.KeyDefs.KeyDefs
import main.scala.db.engine.{LocalKeyDirDatabaseEntry, PublicStructuralDirDatabaseEntry}

class AbstractDirectory(addr_path : String,
                        keys_input : Map[KeyDefs, String],
                        publicDirKeyBDE : PublicStructuralDirDatabaseEntry,
                        localDirKeyDBE : LocalKeyDirDatabaseEntry,
                        acceptedIdentity : Option[(String, String)]) {

  def splitPathAndFileName(input : String) : Array[String] = Array(FilenameUtils.getFullPath(input), FilenameUtils.getBaseName(input), FilenameUtils.getExtension(input))

  def keys() = keys_input

  def vettedIdentity = acceptedIdentity
  def publicDatabaseEntry() = publicDirKeyBDE
  def privateDatabaseEntry() = localDirKeyDBE

  def explodePathVersionExtField(input : String) = {
    val parts = input.replace("[", "").replace("]", "").split('|')
    Array('addressing, 'fullpath, 'version, 'timestamp, 'owner).zip(parts ++ splitPathAndFileName(parts(1))).toMap
  }

  val infos = explodePathVersionExtField(addr_path)

  def isKeyEmpty(key : KeyDefs) = keys().get(key).isEmpty || keys().get(key).get.isEmpty()

  def isKeyEmptyOrZero(key : KeyDefs) = isKeyEmpty(key) || keys().get(key).get.equals("0")

  override def toString() = "[" + infos.map(x => x._1.name + "=" + x._2).mkString(", ") + "]"
}