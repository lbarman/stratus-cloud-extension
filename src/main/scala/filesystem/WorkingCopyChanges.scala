package main.scala.filesystem

import akka.actor.{Actor, actorRef2Scala}
import main.scala.Core
import main.scala.common.PathNormalization
import main.scala.filesystem.repo.AbstractRepo

class WorkingCopyChanges(repoSource : AbstractRepo) extends Actor {
  val changedFiles = new scala.collection.mutable.HashSet[String]();
  val removedFiles = new scala.collection.mutable.HashSet[String]();

  def receive = {

    case WorkingCopyChangesWorkerReset => reSet()

    case WorkingCopyChangesWorkerGetChanges => {
      val v = new WorkingCopyChangesWorkerChanges()
      v.data = changedFiles.clone()
      sender ! v
    }

    case WorkingCopyChangesWorkerGetDeletions => {
      val v = new WorkingCopyChangesWorkerChanges()
      v.data = removedFiles.clone()
      sender ! v
    }

    case WorkingCopyChangesWorkerGetAll => {
      val v = new WorkingCopyChangesWorkerDualChanges()
      v.changes = changedFiles.clone()
      v.deletions = removedFiles.clone()
      sender ! v
    }

    case WorkingCopyChangesWorkerPrintAll => sender ! getAll()

    case e : FileCreationOrModificationEvent =>
      val path = PathNormalization.normalizeForInnerUse(repoSource.pathLFSToInApp(e.path), true, false)
      Core log "[WorkingCopyChanges] Changed file " + path
      removedFiles -= path
      changedFiles += path
      Core log getAll()

    case e : DirCreationEvent =>
      val path = PathNormalization.normalizeForInnerUse(repoSource.pathLFSToInApp(e.path), true, true)
      Core log "[WorkingCopyChanges] Created dir " + path
      removedFiles -= path
      changedFiles += path
      Core log getAll()

    case e : FileDeletionEvent =>
      val path = PathNormalization.normalizeForInnerUse(repoSource.pathLFSToInApp(e.path), true, false)
      Core log "[WorkingCopyChanges] Deleted file " + path
      changedFiles -= path
      removedFiles += path
      Core log getAll()

    case e : DirDeletionEvent =>
      val path = PathNormalization.normalizeForInnerUse(repoSource.pathLFSToInApp(e.path), true, true)
      Core log "[WorkingCopyChanges] Deleted dir " + path
      changedFiles -= path
      removedFiles += path
      Core log getAll()
  }

  def getChangesAndReset() = {
    val changed = changedFiles.clone();
    val removed = removedFiles.clone();

    reSet();
    (changed, removed)
  }

  def reSet() {
    Core log "[WorkingCopyChanges] Reset."

    changedFiles.clear();
    removedFiles.clear();
  }

  def getChanges() = { val buf = new StringBuilder(); changedFiles.foreach(f => buf ++= f + "\n"); buf.toString }
  def getSuppressions() = { val buf = new StringBuilder(); removedFiles.foreach(f => buf ++= f + "\n"); buf.toString }

  def getAll() = {
    val buf = new StringBuilder()
    buf ++= "[WorkingCopyChanges]\n"
    buf ++= "Changed Files :\n"
    buf ++= getChanges()
    buf ++= "Deleted Files : \n"
    buf ++= getSuppressions()
    buf ++= "---"
    buf.toString
  }
}
class WorkingCopyChangesWorkerChanges {
  var data : scala.collection.mutable.HashSet[String] = null;
}
class WorkingCopyChangesWorkerDualChanges {
  var changes : scala.collection.mutable.HashSet[String] = null;
  var deletions : scala.collection.mutable.HashSet[String] = null;
}

case object WorkingCopyChangesWorkerGetAll
case object WorkingCopyChangesWorkerGetChanges
case object WorkingCopyChangesWorkerGetDeletions
case object WorkingCopyChangesWorkerPrintAll
case object WorkingCopyChangesWorkerReset
case object WorkingCopyChangesWorkerNewChange

case class FileDeletionEvent(path : String)
case class DirDeletionEvent(path : String)
case class FileCreationOrModificationEvent(path : String)
case class DirCreationEvent(path : String)