package main.scala.crypto

import java.io.{BufferedInputStream, BufferedOutputStream, File, FileInputStream, FileOutputStream}

import main.scala.{Config, Core}
import main.scala.common.FileToolBox
import main.scala.db.Serializer
import main.scala.db.engine.{LocalKeyDirDatabaseEntry, LocalKeyFileDatabaseEntry, PublicIdentityDatabaseEntry}

class MasterPasswordManager(crypto : AbstractCryptoPrimitives) {

	var masterPassword : String = "this_is_a_dummy_password"

	def stringToKey(s : String) = {
		val k = crypto.hash(s)
		val k128b = k.slice(0, 128) //todo : change this !
		k128b
	}

	def masterPasswordBytes = stringToKey(masterPassword)

	def inputStream(path : String) = new BufferedInputStream(new FileInputStream(path));
	def outputStream(path : String) = new BufferedOutputStream(new FileOutputStream(path));

	def sourceDb() =
		if (Config.db_source.endsWith(Config.osFileSeparator))
			Config.db_source
		else
			Config.db_source + Config.osFileSeparator

	def getPathEncrypted(path : String) : String =
		{
			val filename = path.replaceAllLiterally(sourceDb, "")
			sourceDb + "enc_" + filename
		}

	def getPathIV(path : String) : String =
		{
			val filename = path.replaceAllLiterally(sourceDb, "")
			sourceDb + "iv_" + filename
		}

	def getPathDecrypted(path : String) : String =
		{
			val filename = path.replaceAllLiterally(sourceDb, "")
			sourceDb + filename.replaceFirst("enc_", "")
		}

	def getIVFromCipher(path : String) = getPathIV(getPathDecrypted(path))

	def isValidFile(path : String) = {
		val isDirKeyOpt = Serializer.deserialize[LocalKeyDirDatabaseEntry](path)
		val isFileKeyOpt = Serializer.deserialize[LocalKeyFileDatabaseEntry](path)
		val isIdKeyOpt = Serializer.deserialize[PublicIdentityDatabaseEntry](path)

		val isDirKey = try {
			isDirKeyOpt.get.entryType
			true
		}
		catch {
			case _ : Exception => false
		}

		val isFileKey = try {
			isFileKeyOpt.get.entryType
			true
		}
		catch {
			case _ : Exception => false
		}

		val isIdKey = try {
			isIdKeyOpt.get.entryType
			true
		}
		catch {
			case _ : Exception => false
		}

		(isDirKey || isFileKey || isIdKey)
	}

	def encryptFile(path : String, deleteAfter : Boolean = false) : Boolean = encryptFile(path, this.masterPassword, deleteAfter)

	def encryptFile(path : String, key : String, deleteAfter : Boolean) =
		{
			if (FileToolBox.fileReadable(path) && FileToolBox.fileWritable(path)) {

				val iv = crypto.newSymmetricKey(Config.aesKeySizeBits)
				val cipherPath = getPathEncrypted(path)
				val ivPath = getPathIV(path)

				var res1 = false;
				val res2 = FileToolBox.writeDataIn(ivPath, crypto.bytes_2_b64(iv))
				var res3 = true

				val in = inputStream(path)
				val out = outputStream(cipherPath)

				try {
					res1 = crypto.symmetricEncrypt(in, out, stringToKey(key), iv)
				}
				catch {
					case e : Exception => false
				}
				finally {
					in.close()
					out.close()
				}

				if (deleteAfter) res3 = new File(path).delete()

				res1 & res2 & res3
			}
			else
				false
		}

	def decryptFile(cipherPath : String) : Boolean = decryptFile(cipherPath, this.masterPassword)

	def decryptFile(cipherPath : String, key : String) =
		{
			val ivPath = getIVFromCipher(cipherPath)
			if (FileToolBox.fileReadable(cipherPath) && FileToolBox.fileWritable(cipherPath) &&
				FileToolBox.fileReadable(ivPath) && FileToolBox.fileWritable(ivPath)) {

				val iv64 = FileToolBox.readAllFrom(ivPath)._2
				val iv = crypto.b64_2_bytes(iv64)

				val plainPath = getPathDecrypted(cipherPath)

				val in = inputStream(cipherPath)
				val out = outputStream(plainPath)

				try {
					crypto.symmetricDecrypt(in, out, stringToKey(key), iv)
				}
				catch {
					case e : Exception => false
				}
				finally {
					in.close()
					out.close()
				}

			}
			else
				false
		}

	def isDBEntry(f : File) = !f.getName.startsWith("enc_") && !f.getName.startsWith("iv_")
	def isCipherFile(f : File) = f.getName.startsWith("enc_")

	def fileStatus() =
		{
			val entries = new File(Config.db_source).listFiles()
			val countPlain = entries filter isDBEntry count { p => true }
			val countEnc = entries filter isCipherFile count { p => true }

			(countPlain, countEnc)

		}

	def encryptAllKeysInSourceDb(key : String) =
		{
			var count = 0
			val entries = new File(Config.db_source).listFiles()
			entries filter isDBEntry foreach { e =>
				{
					if (encryptFile(e.getAbsolutePath, key, true))
						count += 1
				}
			}
			count
		}

	def decryptAllKeysInSourceDb(key : String) =
		{
			var count = 0
			val entries = new File(Config.db_source).listFiles()
			entries filter isCipherFile foreach { e =>
				{
					if (decryptFile(e.getAbsolutePath, key))
						count += 1
				}
			}
			count
		}

	def deleteSensitiveInfo() =
		{
			var count = 0
			val entries = new File(Config.db_source).listFiles()
			entries filter isDBEntry foreach { e =>
				{
					try {
						if (e.delete())
							count += 1
					}
				}
			}
			count
		}

	def testKey(key : String) = {

		Core log "[MasterPasswordManager] Gonna try to decrypt with key " + key
		val entries = new File(Config.db_source).listFiles()
		val file = entries filter isCipherFile headOption

		file match {
			case None => false
			case Some(f : File) =>
				val p = f.getAbsolutePath
				decryptFile(p, key)
				val res = isValidFile(getPathDecrypted(p))

				val f2 = new File(getPathDecrypted(p))
				f2.delete()
				res
		}
	}
}