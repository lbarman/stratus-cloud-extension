package main.scala.crypto

import java.io.File
import java.security.SecureRandom

import main.scala.{Config, Core}

object Randomness {

  /*
   * Classic secure randomness
   */
  private[this] val random = newSecRnd();
  private[this] var numberAccess = 0;

  /**
    * Every Config.refreshRandomnessEveryXCalls, we add some randomness to the random generator
    */
  def newAccess() {
    numberAccess += 1;

    if (numberAccess > Config.refreshRandomnessEveryXCalls) {
      numberAccess = 0;
      this.refreshRandomSeed()
    }
  }

  def currentSecRnd() = this.random

  def newSecRnd() : SecureRandom = {
    val s = SecureRandom.getInstance("SHA1PRNG")
    val t = Array.fill[Byte](16)(0); //force initialization
    s.nextBytes(t)
    Core log "[Randomness] New seed."
    s
  }
  //called periodically (each commit in CommitProcessor)
  private def refreshRandomSeed() = {
    val rndSeed = newSecRnd().generateSeed(20)
    random.setSeed(rndSeed);
    numberAccess = 0;
    Core log "[Randomness] Seed refreshed."
  }

  /*
	 * Other randomnesses
	 */
  def randomString(alphabet : String)(n : Int) : String =
    {
      newAccess()
      Stream.continually(random.nextInt(alphabet.size)).map(alphabet).take(n).mkString
    }

  def randomAlphanumericString(n : Int) =
    randomString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")(n)

  def randomNonAlphanumericString(n : Int) =
    randomString(" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~")(n)

  def randomAlphanumericStringOfMaxLength(n : Int) = randomAlphanumericString(random.nextInt(n))

  def randomAlphanumericStringOfMaxLengthAndMinLength(min : Int, max : Int) = randomAlphanumericString(min + random.nextInt(max - min))

  def randomNumber(max : Integer) : Integer =
    {
      newAccess()
      random.nextInt(max)
    }

  def randomNumberBetween(min : Integer, max : Integer) : Integer = min + randomNumber(max - min)

  def randomBytes(n : Integer) = {
    newAccess()
    val array = Array.fill[Byte](n)(0)
    random.nextBytes(array)
    array
  }

  def randomSentence(n_words : Integer, min_length : Integer, max_length : Integer) =
    (for (e <- (1 to n_words)) yield randomAlphanumericStringOfMaxLengthAndMinLength(min_length, max_length)).mkString(" ")

  def randomUniqueFileName(dir : String, basename : String, ext : String) : String =
    {
      var tempFileName = "";
      do {
        tempFileName = dir + basename + Randomness.randomAlphanumericString(8) + ext;
      } while (new File(tempFileName).exists())
      tempFileName
    }
}