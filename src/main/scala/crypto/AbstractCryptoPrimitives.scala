package main.scala.crypto

import java.io.{InputStream, OutputStream}
import java.security.{KeyPair, PrivateKey, PublicKey}

trait AbstractCryptoPrimitives {

  def init()

  /*
   * CONVERSIONS
   */

  def b64_2_bytes(input : String) : Array[Byte] = CryptoToolBox.base64safeToByteArray(input)
  def bytes_2_b64(input : Array[Byte]) : String = CryptoToolBox.byteArrayToBase64Safe(input)

  def publicKey_2_string(pk : PublicKey) = bytes_2_b64(CryptoToolBox.publicKey2byteArrayX509(pk : PublicKey))
  def privateKey_2_string(pk : PrivateKey) = bytes_2_b64(CryptoToolBox.privateKey2byteArrayPKCS8(pk : PrivateKey))

  def string_2_publicKey(pk : String) = CryptoToolBox.byteArray2publicKeyX509(b64_2_bytes(pk));
  def string_2_privateKey(pk : String) = CryptoToolBox.byteArray2privateKeyPKCS8(b64_2_bytes(pk));

  /*
   * HASHING 
   */

  def hash(input : Array[Byte]) : Array[Byte]
  def hash(input : String) : Array[Byte]
  def hash(input : InputStream) : Array[Byte]

  def hashB64(input : String) : String = this.bytes_2_b64(hash(input))
  def hashB64(input : Array[Byte]) : String = this.bytes_2_b64(hash(input))
  def hashB64(input : InputStream) : String = this.bytes_2_b64(hash(input))

  /*
   * ASYMMETRIC ENCRYPTION
   */

  def newAsymmetricKey(bitSize : Integer) : KeyPair
  def asymmetricEncrypt(input : Array[Byte], publicKey : PublicKey) : Array[Byte]
  def asymmetricDecrypt(input : Array[Byte], privateKey : PrivateKey) : Array[Byte]

  /*
   * SIGNATURE
   */

  def newSignatureKey(bitSize : Integer) : KeyPair
  def sign(data : Array[Byte], privateKey : PrivateKey) : Array[Byte]
  def verifySign(data : Array[Byte], sigBytes : Array[Byte], publicKey : PublicKey) : Boolean

  /*
   * SYMMETRIC ENCRYPTION
   */

  def newSymmetricKey(bitSize : Integer) : Array[Byte]
  def symmetricEncrypt(data : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte]
  def symmetricDecrypt(data : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte]

  def symmetricEncrypt(fis : InputStream, fos : OutputStream, key : Array[Byte], iv : Array[Byte]) : Boolean
  def symmetricDecrypt(fis : InputStream, fos : OutputStream, key : Array[Byte], iv : Array[Byte]) : Boolean

  /*
   * HYBRID ENCRYPTION
   */
  def newHybridKeyPair(symmKeyBitLength : Integer, asymmKeyBitLength : Integer) : (Array[Byte], KeyPair) =
    (newSymmetricKey(symmKeyBitLength), newAsymmetricKey(asymmKeyBitLength))

  def hybridEncrypt(data : Array[Byte], pk : PublicKey, symmKeyBitLength : Integer, ivSizeBits : Integer) : (Array[Byte], Array[Byte], Array[Byte]) = {
    val symmKey = newSymmetricKey(symmKeyBitLength)
    val iv = newSymmetricKey(ivSizeBits)
    val cipherData = symmetricEncrypt(data, symmKey, iv); //encrypt long data with symmetric (e.g. AES)
    val symmKeyCipher = asymmetricEncrypt(symmKey, pk) //encrypt symmetric key with asymmetric (e.g. AES key with RSA)
    (cipherData, iv, symmKeyCipher)
  }

  def hybridEncrypt(data : Array[Byte], pk : PublicKey, symmKeyBitLength : Integer) : (Array[Byte], Array[Byte], Array[Byte]) = {
    val symmKey = newSymmetricKey(symmKeyBitLength)
    val symmIV = newSymmetricKey(symmKeyBitLength)
    val cipherData = symmetricEncrypt(data, symmKey, symmIV); //encrypt long data with symmetric (e.g. AES)
    val symmKeyCipher = asymmetricEncrypt(symmKey, pk) //encrypt symmetric key with asymmetric (e.g. AES key with RSA)
    (cipherData, symmKeyCipher, symmIV)
  }

  def hybridEncrypt(data : Array[Byte], symmIV : Array[Byte], pk : PublicKey, symmKeyBitLength : Integer) : (Array[Byte], Array[Byte]) = {
    val symmKey = newSymmetricKey(symmKeyBitLength)
    val cipherData = symmetricEncrypt(data, symmKey, symmIV); //encrypt long data with symmetric (e.g. AES)
    val symmKeyCipher = asymmetricEncrypt(symmKey, pk) //encrypt symmetric key with asymmetric (e.g. AES key with RSA)
    (cipherData, symmKeyCipher)
  }

  def hybridEncrypt(data : Array[Byte], symmIV : Array[Byte], hybridKeyPair : (Array[Byte], PublicKey)) : (Array[Byte], Array[Byte]) = {
    val cipherData = symmetricEncrypt(data, hybridKeyPair._1, symmIV); //encrypt long data with symmetric (e.g. AES)
    val symmKeyCipher = asymmetricEncrypt(hybridKeyPair._1, hybridKeyPair._2) //encrypt symmetric key with asymmetric (e.g. AES key with RSA)
    (cipherData, symmKeyCipher)
  }

  def hybridDecrypt(data : Array[Byte], symmIV : Array[Byte], keyData : Array[Byte], pk : PrivateKey) : Array[Byte] = {
    val privateKey = asymmetricDecrypt(keyData, pk) //decrypt symmetric key with asymmetric (e.g. AES key with RSA)
    val plainData = symmetricDecrypt(data, privateKey, symmIV); //decrypt cipher data with symmetric (e.g. AES)
    plainData
  }

  def hybridDecrypt(data : Array[Byte], dataLength : Int, symmIV : Array[Byte], keyData : Array[Byte], pk : PrivateKey) : Array[Byte] = {
    val privateKey = asymmetricDecrypt(keyData, pk) //decrypt symmetric key with asymmetric (e.g. AES key with RSA)
    val plainData = symmetricDecrypt(data, privateKey, symmIV); //decrypt cipher data with symmetric (e.g. AES)

    //truncate too long data
    if (plainData.length > dataLength) {
      val resized = Array.fill[Byte](dataLength)(0)
      Array.copy(plainData, 0, resized, 0, dataLength)
      resized
    }
    else {
      plainData
    }
  }
}