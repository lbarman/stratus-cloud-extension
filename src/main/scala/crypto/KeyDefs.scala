package main.scala.crypto

object KeyDefs extends Enumeration {
	type KeyDefs = Value
	val K_F, IV_F, KWritePub_F, KWritePriv_F, K_D, KInitPub_D, KInitPriv_D, KCascReadPub, KCascReadPriv, KCascWritePub, KCascWritePriv = Value
}