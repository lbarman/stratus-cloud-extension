package main.scala.crypto

import java.io.{InputStream, OutputStream}
import java.security.{KeyPair, PrivateKey, PublicKey, Security}

import org.bouncycastle.jce.provider.BouncyCastleProvider

class SHA256_AESCBC_RSAPSS_RSAOAEP_CryptoPrimitives extends AbstractCryptoPrimitives {

	def init() {
		Security.addProvider(new BouncyCastleProvider());
	}

	/*
   * HASHING - SHA256
   */

	def hash(input : Array[Byte]) : Array[Byte] = CryptoToolBox.sha256(input)
	def hash(input : String) : Array[Byte] = CryptoToolBox.sha256(input)
	def hash(input : InputStream) : Array[Byte] = CryptoToolBox.sha256_file(input)

	/*
   * ASYMMETRIC ENCRYPTION, RSA OAEP
   */

	def newAsymmetricKey(bitSize : Integer) : KeyPair = CryptoToolBox.generate_encrypt_keypair(bitSize)
	def asymmetricEncrypt(input : Array[Byte], publicKey : PublicKey) : Array[Byte] = CryptoToolBox.encrypt_rsa_oaep(input, publicKey)
	def asymmetricDecrypt(input : Array[Byte], privateKey : PrivateKey) : Array[Byte] = CryptoToolBox.decrypt_rsa_oaep(input, privateKey)

	/*
   * SIGNATURE, RSA PSS
   */

	def newSignatureKey(bitSize : Integer) : KeyPair = CryptoToolBox.generate_sign_keypair(bitSize)
	def sign(data : Array[Byte], privateKey : PrivateKey) : Array[Byte] = CryptoToolBox.sign_rsassa_pss(data, privateKey)
	def verifySign(data : Array[Byte], sigBytes : Array[Byte], publicKey : PublicKey) : Boolean = CryptoToolBox.verify_rsassa_pss(data, sigBytes, publicKey)

	/*
   * SYMMETRIC ENCRYPTION, AES-CBC
   */

	def newSymmetricKey(bitSize : Integer) : Array[Byte] = Randomness.randomBytes(bitSize / 8)
	def symmetricEncrypt(data : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte] = CryptoToolBox.encrypt_aes_cbc(data, key, iv)
	def symmetricDecrypt(data : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte] = CryptoToolBox.decrypt_aes_cbc(data, key, iv)
	def symmetricEncrypt(fis : InputStream, fos : OutputStream, key : Array[Byte], iv : Array[Byte]) : Boolean = CryptoToolBox.encrypt_aes_cbc_file(fis, fos, key, iv)
	def symmetricDecrypt(fis : InputStream, fos : OutputStream, key : Array[Byte], iv : Array[Byte]) : Boolean = CryptoToolBox.decrypt_aes_cbc_file(fis, fos, key, iv)
}