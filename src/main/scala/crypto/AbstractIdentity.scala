package main.scala.crypto

import main.scala.db.engine.DatabaseField.String2DatabaseField
import main.scala.db.engine.PublicIdentityDatabaseEntry

class AbstractIdentity() {

	def getIDString() = "undefined_identity"
	def getID() = {
		val v = new PublicIdentityDatabaseEntry
		v.owner = "undefined_identity"
		v.addressing = "0x00"
		v
	}
	def getPublicKeyString() = "0"
	def getPrivateKeyString() = "0"

}

object AbstractIdentity {
	type VerifiedIdentity = Option[(String, String)]

}