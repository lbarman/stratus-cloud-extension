package main.scala.crypto

import akka.util.Timeout
import main.scala.Config
import main.scala.db.engine.{FieldStatus, PublicIdentityDatabaseEntry}
import main.scala.db.engine.AbstractDatabase
import main.scala.db.engine.DatabaseField.{DatabaseField2String, String2DatabaseField, Tuple2DatabaseField}

class AutomaticLocalIdentity(localDb : AbstractDatabase, remoteDb : AbstractDatabase,
                             crypto : AbstractCryptoPrimitives, preferedIdentity : String = "(.+)") extends AbstractIdentity {

  implicit val timeout = Timeout(Config.defaultTimeout)

  val myIdentities = localDb.getEntriesPIDB.filterNot(e => e.private_key.getValue.isEmpty())
  val regex = preferedIdentity.r
  val matchingIds = myIdentities.filter(id => regex.findFirstIn(id.owner.getValue).isDefined)

  val definitiveId =
    if (matchingIds.count(p => true) > 0)
      matchingIds.head
    else if (myIdentities.count(p => true) > 0)
      myIdentities.head
    else
      commitNewAnonymousIdentity

  def commitNewAnonymousIdentity() =
    {
      val pidbe = new PublicIdentityDatabaseEntry
      val timestamp = System.currentTimeMillis().toString

      pidbe.owner = "anon_" + Randomness.randomAlphanumericString(10)
      pidbe.published_on = timestamp
      val pcinfo = ""
      pidbe.pcName = "UNKNOWN"
      pidbe.pcNameReason = "ANONYMOUS"

      val signInput = "[" + pidbe.owner.getValue + "||" + timestamp + "||||]"
      val signKey = crypto.newSignatureKey(Config.signKeySizeBits)

      pidbe.addressing = (crypto.bytes_2_b64(crypto.hash(signInput)), FieldStatus.Hash)

      val tag = crypto.sign(crypto.hash(signInput), signKey.getPrivate())
      pidbe.signature = (crypto.bytes_2_b64(tag), FieldStatus.RSA)
      pidbe.public_key = (crypto.publicKey_2_string(signKey.getPublic()), FieldStatus.RSA_Public_Key)

      //important : write public without the private key !
      pidbe.private_key = ("", FieldStatus.RSA_Private_Key)
      assert(pidbe.private_key.getValue.equals(""))
      remoteDb.insertEntry(pidbe)

      pidbe.private_key = (crypto.privateKey_2_string(signKey.getPrivate()), FieldStatus.RSA_Private_Key)

      //now write with private key
      localDb.insertEntry(pidbe)

      pidbe
    }

  override def getIDString() = definitiveId.owner
  override def getID() = definitiveId
  override def getPublicKeyString() = definitiveId.public_key
  override def getPrivateKeyString() = definitiveId.private_key

}