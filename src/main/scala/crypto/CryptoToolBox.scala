package main.scala.crypto

import java.io.{InputStream, OutputStream}
import java.security.{DigestInputStream, KeyFactory, KeyPair, KeyPairGenerator, MessageDigest, PrivateKey, PublicKey, Security, Signature}
import java.security.spec.{PKCS8EncodedKeySpec, X509EncodedKeySpec}

import org.bouncycastle.crypto.engines.AESEngine
import org.bouncycastle.crypto.modes.{CBCBlockCipher, OFBBlockCipher}
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher
import org.bouncycastle.crypto.params.{KeyParameter, ParametersWithIV}
import org.bouncycastle.jce.provider.BouncyCastleProvider

import javax.crypto.{Cipher, CipherInputStream, CipherOutputStream}
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

object CryptoToolBox {

  def init() {
    Security.addProvider(new BouncyCastleProvider());

    //printAlgorithms(s => s.startsWith("Cipher.") && s.contains("AES") && s.contains("CBC"));
  }

  def printAlgorithms(filter : (String => Boolean)) {
    val providers = Security.getProviders();

    for (i <- 0 to providers.length - 1) {
      val it = providers(i).keySet().iterator();

      while (it.hasNext()) {
        val v = it.next().toString();
        if (filter(v)) {
          println(providers(i).getName() + " : " + v);
        }
      }
    }
  }

  def cipherExists(cipher : String) = {
    try {
      val v = Cipher.getInstance(cipher)
      println("Algo " + cipher + " was found in provider " + v.getProvider())
    }
    catch {
      case e : Exception =>
        println("Algo " + cipher + " was NOT found")
    }
  }

  /*
   * Base 64
   */

  def byteArrayToBase64(input : Array[Byte]) : String = new String(org.apache.commons.codec.binary.Base64.encodeBase64(input));
  def ba2b64(input : Array[Byte]) : String = byteArrayToBase64(input) //alias

  def base64ToByteArray(input : String) : Array[Byte] = org.apache.commons.codec.binary.Base64.decodeBase64(input);
  def b64toba(input : String) : Array[Byte] = base64ToByteArray(input) //alias

  /*
   * FOLDER_SAFE Base 64
   * (because on windows / can't appear in a folder name). This replaces the / in the B64 version by _, and + by -
   */

  def byteArrayToBase64Safe(input : Array[Byte]) : String = byteArrayToBase64(input).replaceAllLiterally("+", "-").replaceAllLiterally("/", "_")
  def base64safeToByteArray(input : String) : Array[Byte] = base64ToByteArray(input.replaceAllLiterally("-", "+").replaceAllLiterally("_", "/"))

  /*
   * HASHING 
   */

  def sha256(input : Array[Byte]) : Array[Byte] = {
    val md = MessageDigest.getInstance("SHA-256");
    md.update(input);
    md.digest();
  }

  def sha256(input : String) : Array[Byte] = {
    val md = MessageDigest.getInstance("SHA-256");
    md.update(input.getBytes("UTF-8"));
    md.digest();
  }

  def sha256_file(input : InputStream) : Array[Byte] =
    {
      val BUFFER_SIZE = 16384;
      val md = MessageDigest.getInstance("SHA-256");
      try {
        val dis = new DigestInputStream(input, md);
        val buffer = new Array[Byte](BUFFER_SIZE);

        while (dis.read(buffer) != -1) {
          //nothing to do here
        }
        dis.close();
      }
      finally {
        input.close();
      }
      md.digest();
    }

  /*
   * (Public, Private) KEY PAIR SERIALIZATION; X509, PCKS8
   */

  def publicKey2byteArrayX509(pk : PublicKey) : Array[Byte] = {
    val x509ks = new X509EncodedKeySpec(pk.getEncoded());
    x509ks.getEncoded()
  }

  def privateKey2byteArrayPKCS8(pk : PrivateKey) : Array[Byte] = {
    val pkcsKeySpec = new PKCS8EncodedKeySpec(pk.getEncoded());
    pkcsKeySpec.getEncoded()
  }

  def byteArray2publicKeyX509(publicKeyArray : Array[Byte]) : PublicKey = {
    val keyFactory = KeyFactory.getInstance("RSA", "BC");
    val pkSpec = new X509EncodedKeySpec(publicKeyArray);
    keyFactory.generatePublic(pkSpec);
  }

  def byteArray2privateKeyPKCS8(privateKeyArray : Array[Byte]) : PrivateKey = {
    val keyFactory = KeyFactory.getInstance("RSA", "BC");
    val privKeySpec = new PKCS8EncodedKeySpec(privateKeyArray);
    keyFactory.generatePrivate(privKeySpec);
  }

  /*
   * ASYMMETRIC ENCRYPTION - RSA OAEP PKCS1, with SHA1, MGF1
   */

  def generate_encrypt_keypair(keySize : Integer) : KeyPair = {
    val keyGen = KeyPairGenerator.getInstance("RSA", "BC");
    keyGen.initialize(keySize, Randomness.newSecRnd())
    keyGen.generateKeyPair();
  }

  def encrypt_rsa_oaep(input : Array[Byte], publicKey : PublicKey) : Array[Byte] =
    {
      val rsa = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");
      rsa.init(Cipher.ENCRYPT_MODE, publicKey, Randomness.newSecRnd());
      rsa.doFinal(input);
    }

  def decrypt_rsa_oaep(cipher : Array[Byte], privateKey : PrivateKey) : Array[Byte] =
    {
      val rsa = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");
      rsa.init(Cipher.DECRYPT_MODE, privateKey);
      rsa.doFinal(cipher);
    }

  /*
   * SIGNATURE, RSASSA - PSS, PKCS1
   */

  def generate_sign_keypair(keySize : Integer) : KeyPair = {
    val keyGen = KeyPairGenerator.getInstance("RSA", "BC");
    keyGen.initialize(keySize, Randomness.currentSecRnd)
    keyGen.generateKeyPair();
  }

  def sign_rsassa_pss(data : Array[Byte], privateKey : PrivateKey) : Array[Byte] = {
    val signature = Signature.getInstance("RSASSA-PSS", "BC");
    signature.initSign(privateKey);
    signature.update(data);
    signature.sign();
  }

  def verify_rsassa_pss(data : Array[Byte], sigBytes : Array[Byte], publicKey : PublicKey) : Boolean = {
    val signature = Signature.getInstance("RSASSA-PSS", "BC");
    signature.initVerify(publicKey);
    signature.update(data);
    signature.verify(sigBytes);
  }

  /*
   * SYMMETRIC ENCRYPTION, AES CBC
   */

  def decrypt_aes_cbc(cipher : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte] =
    {
      val aes = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESEngine()));
      val ivAndKey = new ParametersWithIV(new KeyParameter(key), iv);
      aes.init(false, ivAndKey); //first param is "forEncryption", here false since we're decrypting
      cipherDataCBC(aes, cipher)
    }

  def encrypt_aes_cbc(plain : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte] =
    {
      val aes = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESEngine()));
      val ivAndKey = new ParametersWithIV(new KeyParameter(key), iv);
      aes.init(true, ivAndKey); //first param is "forEncryption", here true since we're encrypting
      cipherDataCBC(aes, plain);
    }

  private def cipherDataCBC(cipher : PaddedBufferedBlockCipher, data : Array[Byte]) : Array[Byte] = {

    var outBuf : Array[Byte] = new Array[Byte](cipher.getOutputSize(data.length));

    val outputLen = cipher.processBytes(data, 0, data.length, outBuf, 0);

    try {
      cipher.doFinal(outBuf, outputLen);
      outBuf
    }
    catch {
      case _ : Throwable =>
        throw new Exception("AES-CBC-256 Crypto Error");
        System.err.println("AES-CBC-256 Crypto Error");
        System.exit(1);
        outBuf
    }
  }

  /*
   * SYMMETRIC ENCRYPTION, AES OFB
   */

  def decrypt_aes_ofb(cipher : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte] =
    {
      val aes = new PaddedBufferedBlockCipher(new OFBBlockCipher(new AESEngine(), 128));
      val ivAndKey = new ParametersWithIV(new KeyParameter(key), iv);
      aes.init(false, ivAndKey); //first param is "forEncryption", here false since we're decrypting
      cipherDataOFB(aes, cipher)
    }

  def encrypt_aes_ofb(plain : Array[Byte], key : Array[Byte], iv : Array[Byte]) : Array[Byte] =
    {
      val aes = new PaddedBufferedBlockCipher(new OFBBlockCipher(new AESEngine(), 128));
      val ivAndKey = new ParametersWithIV(new KeyParameter(key), iv);
      aes.init(true, ivAndKey); //first param is "forEncryption", here true since we're encrypting
      cipherDataOFB(aes, plain);
    }

  def padArrayWith0BytesAtTheEndUntil(arr : Array[Byte], size : Integer) : Array[Byte] = {
    val res = Array.fill[Byte](size)(0);
    System.arraycopy(arr, 0, res, 0, arr.length);
    res
  }

  private def cipherDataOFB(cipher : PaddedBufferedBlockCipher, data : Array[Byte]) : Array[Byte] = {

    var outBuf : Array[Byte] = new Array[Byte](cipher.getOutputSize(data.length));

    val outputLen = cipher.processBytes(data, 0, data.length, outBuf, 0);

    try {
      cipher.doFinal(outBuf, outputLen);
      outBuf
    }
    catch {
      case _ : Throwable =>
        throw new Exception("AES-OFB-256 Crypto Error");
        System.err.println("AES-OFB-256 Crypto Error");
        System.exit(1);
        outBuf
    }
  }

  /*
   * AES CBC 256 for files
   */

  def encrypt_aes_cbc_file(fis : InputStream, fos : OutputStream, key : Array[Byte], iv : Array[Byte]) = {

    val cipher = Cipher.getInstance("PBEWITHSHA256AND256BITAES-CBC-BC");

    val secretKeySpec = new SecretKeySpec(key, "AES");
    val ivParameterSpec = new IvParameterSpec(iv);
    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

    var cos : CipherOutputStream = null;
    try {
      cos = new CipherOutputStream(fos, cipher);
      var data = new Array[Byte](1024);
      var read = fis.read(data);
      while (read != -1) {
        cos.write(data, 0, read);
        read = fis.read(data);
      }
      cos.flush();
      true
    }
    catch {
      case e : Exception => false
    }
    finally {
      if (cos != null) {
        cos.close();
      }
      if (fos != null) {
        fos.close();
      }
      if (fis != null) {
        fis.close();
      }
    }
  }
  def decrypt_aes_cbc_file(fis : InputStream, fos : OutputStream, key : Array[Byte], iv : Array[Byte]) = {

    val cipher = Cipher.getInstance("PBEWITHSHA256AND256BITAES-CBC-BC");

    val secretKeySpec = new SecretKeySpec(key, "AES");
    val ivParameterSpec = new IvParameterSpec(iv);
    cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

    var cis : CipherInputStream = null;
    try {
      cis = new CipherInputStream(fis, cipher);
      var data = new Array[Byte](1024);
      var read = cis.read(data);
      while (read != -1) {
        fos.write(data, 0, read);
        read = cis.read(data);
      }
      true
    }
    catch {
      case e : Exception => false
    }
    finally {
      if (cis != null) {
        cis.close();
      }
      if (fos != null) {
        fos.close();
      }
      if (fis != null) {
        fis.close();
      }
    }
  }
}