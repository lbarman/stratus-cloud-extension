package main.scala.common

import java.io.File
import java.nio.file.{Files, Paths}

import scala.io.Codec.string2codec

import main.scala.{Config, Core}

object FileToolBox {

  def fileExists(fileName : String) : Boolean = {
    val f = new File(fileName);
    f.exists && f.isFile
  }
  def dirExists(dirName : String) : Boolean = {
    val d = new File(dirName);
    d.exists && d.isDirectory
  }

  def fileWritable(fileName : String) : Boolean =
    {
      val f = new File(fileName);
      ((!f.isDirectory() && f.canWrite) || (!f.isDirectory() && new File(f.getParent()).canWrite()))
    }

  def fileReadable(fileName : String) : Boolean =
    {
      val f = new File(fileName)
      (f.exists() && (!f.isDirectory()) && f.canRead())
    }

  def fileDirectlyInside(filePath : String, folderPath : String) =
    {
      val f = PathNormalization.normalizeForInnerUse(filePath, false, false)
      val d = PathNormalization.normalizeForInnerUse(folderPath, false, true)

      val isInside = f.startsWith(d)
      val justInside = f.replaceAllLiterally(d, "").count(p => Config.unix_dir_separator.charAt(0).equals(p)) == 0
      isInside && justInside
    }

  def dirDirectlyInside(filePath : String, folderPath : String) =
    {
      val d_inner = PathNormalization.normalizeForInnerUse(filePath, false, true)
      val d_outer = PathNormalization.normalizeForInnerUse(folderPath, false, true)

      val d_inner_without_end_separator = d_inner.substring(0, d_inner.length() - 1)

      val isInside = d_inner_without_end_separator.startsWith(d_outer)
      val justInside = (d_inner_without_end_separator.replaceAllLiterally(d_outer, "").
        count(p => Config.unix_dir_separator.charAt(0).equals(p)) == 0)
      isInside && justInside
    }

  def writeDataIn(path : String, data : String) = {
    Core log "[FileToolBox] Writing data here : " + path + " ===> \n" + data + "\n[EOF]"

    if (fileWritable(path)) {
      val p = new java.io.PrintWriter(path, "UTF-8")
      try {
        p.write(data)
        true
      }
      catch {
        case e : Exception => false
      }
      finally {
        p.close()
      }
    }
    else {
      false
    }
  }

  def readAllFrom(path : String) =
    {
      var testTxtSource : scala.io.BufferedSource = null;
      try {

        testTxtSource = scala.io.Source.fromFile(path)("UTF-8")
        val text = testTxtSource.mkString

        (true, text)
      }
      catch {
        case e : Exception => (false, "")
      }
      finally {
        testTxtSource.close()
      }
    }

  def readAllBytesFrom(pathInput : String) = {

    try {

      val path = Paths.get(pathInput);
      val data = Files.readAllBytes(path);

      (true, data)
    }
    catch {
      case e : Exception => (false, Array.fill[Byte](0)(0))
    }
  }

  def writeDataBytesIn(pathInput : String, data : Array[Byte]) = {

    try {

      val path = Paths.get(pathInput);
      Files.write(path, data);

      true
    }
    catch {
      case e : Exception => false
    }
  }
}