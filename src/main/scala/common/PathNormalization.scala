package main.scala.common

import main.scala.{Config, Core}

object PathNormalization {

  /*
   * INNER PATH Rules :
   * 1) Always UNIX_DIRECTORY_SEPARATOR
   * 2) Directories always end with UNIX_DIRECTORY_SEPARATOR
   * 3) RELATIVE PATH always start with UNIX DIRECTORY_SEPARATOR
   */
  def normalizeForInnerUse(path : String, isRelative : Boolean, isDir : Boolean) =
    {
      //Core log s"[PathNormalization] normalizeForInnerUse:: Received $path, with args (isRelative=$isRelative, isDir=$isDir)"
      var tempString = path;

      //rule 1
      tempString = tempString.replaceAllLiterally(Config.win_dir_separator, Config.unix_dir_separator)
      tempString = tempString.replaceAllLiterally(Config.unix_dir_separator, Config.unix_dir_separator)

      //rule 2
      if (isDir && !tempString.endsWith(Config.unix_dir_separator)) {
        tempString += Config.unix_dir_separator
      }

      //rule 3
      if (isRelative && !tempString.startsWith(Config.unix_dir_separator)) {
        tempString = Config.unix_dir_separator + tempString
      }

      //Sanity check
      if (tempString.contains(Config.unix_dir_separator * 2)) {
        Core log s"[PathNormalization][Error] normalizeForInnerUse:: Input was $path, output $tempString, but it contains illegal char"
      }
      //Core log s"[PathNormalization] normalizeForInnerUse:: Outputting $tempString."
      tempString
    }

  def innerAbovePath(path : String, isDir : Boolean) = {

    //Core log s"[PathNormalization] innerAbovePath:: Received $path, with args (isDir=$isDir)"
    var work = normalizeForInnerUse(path, true, isDir)

    //if work is /, returns /
    if (work.equals(Config.unix_dir_separator)) {
      //Core log s"[PathNormalization] innerAbovePath:: Outputting $work."
      work
    }
    else {

      //now work has format : /xxx/yyy/zzz if file, or /xxx/yyy/zzz/ if dir

      //if dir, remove last /
      if (isDir)
        work = work.substring(0, work.length() - 1)

      //lastSepIndex is either 0 (case work= /xxx ), or positive
      val lastSepIndex = work.lastIndexOf(Config.unix_dir_separator)

      work = work.substring(0, lastSepIndex + 1)

      //Core log s"[PathNormalization] innerAbovePath::Outputting $work."
      work
    }
  }
}