package main.scala.common

import akka.actor.actorRef2Scala
import main.scala.Core
import main.scala.db.engine.{ FieldStatus, LocalKeyDirDatabaseEntry, LocalKeyFileDatabaseEntry }
import main.scala.db.engine.AbstractDatabase
import main.scala.db.engine.DatabaseField.Tuple2DatabaseField

/**
  * Adds "smartly" some keys to the local database; if previous key existed, don't overwrite.
  * Returns the number of changed fields (number of effective keys added)
  */
object DBToolBox {

  /**
    * Adds "smartly" some file keys to the local database; if previous key existed, don't overwrite.
    * Returns the number of changed fields (number of effective keys added)
    */
  def addAndMergeFileKey(localDb : AbstractDatabase, addressing : String, k : String = "", kWritePriv : String = "") : Int =
    {
      var count = 0;

      Core.logger ! s"[DBToolBox] Gonna add information to file's key $addressing."

      val opt = localDb.getEntriesLKFDB.filter(e => e.addressing.getValue.equals(addressing)).headOption
      val newEntry = new LocalKeyFileDatabaseEntry
      newEntry.addressing = (addressing, FieldStatus.Hash)
      opt match {
        case Some(e : LocalKeyFileDatabaseEntry) =>

          Core.logger ! s"[DBToolBox] Old entry was : \n " + e
          newEntry.K =
            if (e.K.getValue.isEmpty()) {
              if (!k.isEmpty())
                count += 1
              (k, FieldStatus.AES_Key)
            }
            else {
              (e.K.getValue, FieldStatus.AES_Key)
            }
          newEntry.KWritePriv =
            if (e.KWritePriv.getValue.isEmpty()) {
              if (!kWritePriv.isEmpty())
                count += 1
              (kWritePriv, FieldStatus.RSA_Private_Key)
            }
            else {
              (e.KWritePriv.getValue, FieldStatus.RSA_Private_Key)
            }
          localDb.deleteEntry(e)
        case None =>

          if (!k.isEmpty())
            count += 1
          if (!kWritePriv.isEmpty())
            count += 1

          newEntry.K = (k, FieldStatus.AES_Key)
          newEntry.KWritePriv = (kWritePriv, FieldStatus.RSA_Private_Key)
      }
      localDb.insertEntry(newEntry)
      count
    }

  /**
    * Adds "smartly" some directory keys to the local database; if previous key existed, don't overwrite.
    * Returns the number of changed fields (number of effective keys added)
    */
  def addAndMergeDirKey(localDb : AbstractDatabase, addressing : String, k_d : String = "", kInitPriv : String = "", kCascReadPriv : String = "", kCascWritePriv : String = "") : Int =
    {
      var count = 0;
      Core.logger ! s"[DBToolBox] Gonna add information to directory's key $addressing."

      val opt = localDb.getEntriesLKDDB.filter(e => e.addressing.getValue.equals(addressing)).headOption
      val newEntry = new LocalKeyDirDatabaseEntry
      newEntry.addressing = (addressing, FieldStatus.Hash)
      opt match {
        case Some(e : LocalKeyDirDatabaseEntry) =>
          {

            Core.logger ! s"[DBToolBox] Old entry was : \n " + e

            newEntry.KInitPriv =
              if (e.KInitPriv.getValue.isEmpty()) {
                if (!kInitPriv.isEmpty())
                  count += 1
                (kInitPriv, FieldStatus.RSA_Private_Key)
              }
              else
                (e.KInitPriv.getValue, FieldStatus.RSA_Private_Key)

            newEntry.KCascadeReadPriv =
              if (e.KCascadeReadPriv.getValue.isEmpty()) {
                if (!kCascReadPriv.isEmpty())
                  count += 1
                (kCascReadPriv, FieldStatus.RSA_Private_Key)
              }
              else
                (e.KCascadeReadPriv.getValue, FieldStatus.RSA_Private_Key)

            newEntry.KCascadeWritePriv =
              if (e.KCascadeWritePriv.getValue.isEmpty()) {
                if (!kCascWritePriv.isEmpty())
                  count += 1
                (kCascWritePriv, FieldStatus.RSA_Private_Key)
              }
              else {
                (e.KCascadeWritePriv.getValue, FieldStatus.RSA_Private_Key)
              }

            newEntry.K =
              if (e.K.getValue.isEmpty()) {
                if (!k_d.isEmpty())
                  count += 1
                (k_d, FieldStatus.AES_Key)
              }
              else
                (e.K.getValue, FieldStatus.AES_Key)

            localDb.deleteEntry(e)

            Core.logger ! s"[DBToolBox] New entry is : \n " + newEntry
          }

        case None =>
          {
            if (!kInitPriv.isEmpty())
              count += 1
            if (!kCascReadPriv.isEmpty())
              count += 1
            if (!kCascWritePriv.isEmpty())
              count += 1
            if (!kCascWritePriv.isEmpty())
              count += 1

            newEntry.KCascadeReadPriv = (kCascReadPriv, FieldStatus.RSA_Private_Key)
            newEntry.KCascadeWritePriv = (kCascWritePriv, FieldStatus.RSA_Private_Key)
            newEntry.KInitPriv = (kInitPriv, FieldStatus.RSA_Private_Key)
            newEntry.K = (k_d, FieldStatus.AES_Key)
          }
      }

      localDb.insertEntry(newEntry)
      count
    }

  def addAndMergeFileKey(localDb : AbstractDatabase, e : LocalKeyFileDatabaseEntry) : Unit =
    addAndMergeFileKey(localDb, e.getAddressing.getValue, e.K.getValue, e.KWritePriv.getValue)

  def addAndMergeDirKey(localDb : AbstractDatabase, e : LocalKeyDirDatabaseEntry) : Unit =
    addAndMergeDirKey(localDb, e.getAddressing.getValue, e.K.getValue, e.KInitPriv.getValue, e.KCascadeReadPriv.getValue, e.KCascadeWritePriv.getValue)

}