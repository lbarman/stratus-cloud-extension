package main.scala.common

import scala.xml.{Elem, Node, NodeSeq}

import main.scala.Core

object XMLToolBox {
  def stripTag(xml : Node, tagToStrip : String) : Node =
    {
      Core log s"[XMLToolBox] Stripping $tagToStrip from " + compactString(xml)
      val newXml = new scala.xml.transform.RewriteRule {
        override def transform(n : scala.xml.Node) : scala.xml.Node = n match {
          case e : Elem =>
            if (e.label == tagToStrip)
              e copy (child = NodeSeq.Empty)
            else
              e copy (child = e.child flatMap (this transform))
          case other => other
        }
      } transform xml

      Core log s"[XMLToolBox] Result is " + compactString(newXml)
      newXml
    }

  def multiStripTag(xml : Node, tagsToStrip : Array[String]) : Node =
    {
      var work = xml;
      tagsToStrip filterNot { _.isEmpty } foreach { t => work = stripTag(work, t) }
      work
    }

  def compactString(xml : Node) : String =
    xml.toString.replaceAll("\t", "").replaceAll("(\\s+)", "")
}