package main.scala

import java.io.{BufferedInputStream, BufferedOutputStream, File, FileInputStream, FileOutputStream}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import filesystem.repo.LocalFileSystemRepo
import main.scala.common.FileToolBox
import main.scala.crypto.{CryptoToolBox, Randomness, SHA256_AESCBC_RSAPSS_RSAOAEP_CryptoPrimitives}
import main.scala.db.Serializer
import main.scala.db.engine.{FileBasedDatabase, PublicStructuralFileDatabaseEntry}
import main.scala.db.engine.DatabaseField.String2DatabaseField
import main.scala.filesystem.AbstractFile
import main.scala.processors.{CommitDeletionOfFile, CommitNewDir, CommitNewFile, CommitNewVersionOfFile, DiscoverAllReadableFiles, DiscoverPreviousVersion, DiscoveredAllFiles}

object Tests {

  implicit val timeout = Timeout(Config.defaultTimeout)

  def futurBooleanAssert(f : Future[Any],
                         success : String = "Test finished (RES=TRUE).",
                         falseReceived : String = "Test finished with RES=FALSE.",
                         failure : String = "Test failed.",
                         noBoolean : String = "Test failed (not a boolean).") {
    f.onSuccess {
      case b : Boolean =>
        if (b)
          println(s"[Success] $success.")
        else
          println(s"[Failure] $falseReceived.")
      case _ => println(s"[Failure] $noBoolean.")
    }
    f.onFailure {
      case _ => println(s"[Failure] $failure.")
    }
  }

  def benchmark(path_input : String) {
    println("Starting Benchmarks")
    println("---------------------------------")

    val f = new File(path_input)

    if (!f.exists()) {
      println("Aborted, file does not exists.")
    }
    else {
      //File hash
      val mbs = (f.length().toDouble / (1024 * 1024))
      println("File : " + f.getAbsolutePath.toString + f" ($mbs%2.2f MBs)")

      {

        println("Testing SHA-256 (10 iterations)")
        val n = 10;
        val startTime = System.currentTimeMillis();
        for (i <- 0 to n) {
          println(s"Iteration $i...")
          var input : FileInputStream = null;
          try {
            input = new FileInputStream(f);
            CryptoToolBox.sha256_file(input)
          }
          finally {
            if (input != null)
              input.close();
          }
        }
        val endTime = System.currentTimeMillis();
        val diff = endTime - startTime
        val average = diff.toDouble / n / 1000
        println(f"Average time : $average%2.2f seconds")
        println
      }

      {
        //AES-CBC
        println("Testing AES-256 (10 iterations)")
        val n = 10;
        val startTime = System.currentTimeMillis();
        val temp = File.createTempFile("tempfile", ".tmp");

        for (i <- 0 to n) {
          println(s"Iteration $i...")
          var input : FileInputStream = null;
          var output : FileOutputStream = null;
          try {
            input = new FileInputStream(f);
            output = new FileOutputStream(temp);
            val key = Randomness.randomBytes(256 / 8)
            val iv = Randomness.randomBytes(128 / 8)
            CryptoToolBox.encrypt_aes_cbc_file(input, output, key, iv)
          }
          finally {
            if (input != null)
              input.close();
            temp.delete()
          }
        }
        val endTime = System.currentTimeMillis();
        val diff = endTime - startTime
        val average = diff.toDouble / n / 1000
        println(f"Average time : $average%2.2f seconds")
        println
      }
    }

    Core.commitProcessor ! new CommitNewDir("""A:\repos\source_files\interesting_dir\""")

    println("---------------------------------")
    println("Test finished.")
  }

  def repoClean() {

    println("---------------------------------")
    println("Cleaning Repo Dirs")
    println("---------------------------------")

    def deleteRecursively(f : File) : Boolean = {
      if (f.isDirectory) f.listFiles match {
        case null =>
        case xs   => xs foreach deleteRecursively
      }
      f.delete()
    }
    def mkDir(dir : File) {
      try {
        if (!dir.isDirectory()) {
          dir.mkdirs();
        }
      }
      catch {
        case _ : Throwable => None;
      }
    }

    val files = Config.repo_dest :: Config.db_source :: Config.db_dest :: Nil

    files map (f => new File(f)) foreach (f => { deleteRecursively(f); mkDir(f) })
  }

  def dirCommitTest(path_input : String) {
    println("Starting Commit-Dir Test")
    println("---------------------------------")

    Core.commitProcessor ! new CommitNewDir("""A:\repos\source_files\interesting_dir\""")

    println("---------------------------------")
    println("Test finished.")
  }

  def repoReadTest() {
    println("Starting Repo-Read Test")

    println()

    val future : Future[DiscoveredAllFiles] = ask(Core.discoveryProcessor, DiscoverAllReadableFiles).mapTo[DiscoveredAllFiles]
    future.onSuccess {
      case res : DiscoveredAllFiles =>
        println("--------------FILES--------------")
        res.files foreach println
        println()

        println("--------------DIRS --------------")
        res.directories foreach println
        println("---------------------------------")
        println()

        println("Test finished.")
    }
    future.onFailure {
      case _ =>
        println("Error : Actor Failure")
        println()

        println("Test finished.")
    }
  }

  def fileCommitNewFileTest(path_input : String) {

    println("Starting File-Commit Test")
    println("---------------------------------")

    val path = if (path_input.equals("")) """A:\repos\source_files\interesting_dir\texte.txt""" else path_input

    val future = ask(Core.discoveryProcessor, new CommitNewFile(path)).mapTo[Boolean]
    futurBooleanAssert(future)

    println("---------------------------------")
    println("Test finished.")
  }

  def fileCommitNewVersionTest(path_input : String) {

    println("Starting File-Commit Test")
    println("---------------------------------")

    val path = if (path_input.equals("")) """A:\repos\source_files\texte.txt""" else path_input
    val prevVersionFuture = (Core.discoveryProcessor ? new DiscoverPreviousVersion(path)).mapTo[Option[AbstractFile]]

    prevVersionFuture.onSuccess {
      case prevVOption : Option[AbstractFile] =>

        prevVOption match {
          case Some(abstractFile) =>
            {
              val boolFuture = (Core.commitProcessor ? new CommitNewVersionOfFile(path, abstractFile)).mapTo[Boolean]
              futurBooleanAssert(boolFuture)
            }
          case _ => println("[Failure] Test failure.")
        }

      case _ => println("[Failure] Test failure.")
    }

    println("---------------------------------")
    println("Test finished.")
  }

  def fileCommitDeletionTest(path_input : String) {

    println("Starting File-Delete Test")
    println("---------------------------------")

    val path = if (path_input.equals("")) """A:\repos\source_files\texte.txt""" else path_input
    val prevVersionFuture = (Core.discoveryProcessor ? new DiscoverPreviousVersion(path)).mapTo[Option[AbstractFile]]

    prevVersionFuture.onSuccess {
      case prevVOption : Option[AbstractFile] =>

        prevVOption match {
          case Some(abstractFile) =>
            {
              val boolFuture = (Core.commitProcessor ? new CommitDeletionOfFile(path, abstractFile)).mapTo[Boolean]
              futurBooleanAssert(boolFuture)
            }
          case _ => println("[Failure] Test failure.")
        }

      case _ => println("[Failure] Test failure.")
    }

    println("---------------------------------")
    println("Test finished.")
  }

  def localFileSystemRepoTest() {
    println("Starting LFS-Repo Test")
    println("---------------------------------")

    val rootFolder = "A:\\Java Workspace" //without ending \\
    val lfsWin = new LocalFileSystemRepo(rootFolder, false);

    assert(lfsWin.rootFolder.equals(rootFolder + "\\"))

    println("1. " + lfsWin.pathInAppToLFS("/someString/ninja.ext") + " should be equals to " + ("A:\\Java Workspace\\someString\\ninja.ext"))
    assert(lfsWin.pathInAppToLFS("/someString/ninja.ext").equals("A:\\Java Workspace\\someString\\ninja.ext"))

    println("2. " + lfsWin.pathInAppToLFS("/someString/ninja/") + " should be equals to " + "A:\\Java Workspace\\someString\\ninja\\")
    assert(lfsWin.pathInAppToLFS("/someString/ninja/").equals("A:\\Java Workspace\\someString\\ninja\\"))

    println("3. " + lfsWin.pathLFSToInApp("A:\\Java Workspace\\someString\\ninja\\") + " should be equals to " + ("/someString/ninja/"))
    assert(lfsWin.pathLFSToInApp("A:\\Java Workspace\\someString\\ninja\\").equals("/someString/ninja/"))

    println("4. " + lfsWin.pathLFSToInApp("A:\\Java Workspace\\someString\\ninja.ext") + " should be equals to " + ("/someString/ninja.ext"))
    assert(lfsWin.pathLFSToInApp("A:\\Java Workspace\\someString\\ninja.ext").equals("/someString/ninja.ext"))

    println("Printing contents...")
    val stream = lfsWin.getFileTreeForDir("/TowerDefense v2/");
    stream foreach println
    val stream2 = lfsWin.getFilesInDir("/TowerDefense v2/");
    stream2 foreach println

    val rootFolderUnix = "/usr/bin/repos" //without ending /
    val lfsUnix = new LocalFileSystemRepo(rootFolderUnix, true);
    assert(lfsUnix.rootFolder.equals(rootFolderUnix + "/"))

    println("5. " + lfsUnix.pathInAppToLFS("/someString/ninja.ext") + " should be equals to " + ("/usr/bin/repos/someString/ninja.ext"))
    assert(lfsUnix.pathInAppToLFS("/someString/ninja.ext").equals("/usr/bin/repos/someString/ninja.ext"))

    println("6. " + lfsUnix.pathInAppToLFS("/someString/ninja/") + " should be equals to " + "/usr/bin/repos/someString/ninja/")
    assert(lfsUnix.pathInAppToLFS("/someString/ninja/").equals("/usr/bin/repos/someString/ninja/"))

    println("7. " + lfsUnix.pathLFSToInApp("/usr/bin/repos/someString/ninja/") + " should be equals to " + ("/someString/ninja/"))
    assert(lfsUnix.pathLFSToInApp("/usr/bin/repos/someString/ninja/").equals("/someString/ninja/"))

    println("8. " + lfsUnix.pathLFSToInApp("/usr/bin/repos/someString/ninja.ext") + " should be equals to " + ("/someString/ninja.ext"))
    assert(lfsUnix.pathLFSToInApp("/usr/bin/repos/someString/ninja.ext").equals("/someString/ninja.ext"))

    println("---------------------------------")
    println("Test finished.")
  }

  def aesFilesTest() {

    println("Starting AES-CBC Test")
    println("---------------------------------")

    val file1 = "A:\\Downloads\\300 Rise of an Empire [2014]\\300 Rise of an Empire.avi";
    val file2 = "A:\\Downloads\\300 Rise of an Empire [2014]\\Crypt.avi";
    val file3 = "A:\\Downloads\\300 Rise of an Empire [2014]\\300.2.avi";

    val fis = new BufferedInputStream(new FileInputStream(file1));
    val fos = new BufferedOutputStream(new FileOutputStream(file2));

    val keySize = 16; //bytes = 256 bits
    val key = Randomness.randomBytes(keySize);
    val iv = Randomness.randomBytes(keySize);

    CryptoToolBox.encrypt_aes_cbc_file(fis, fos, key, iv);
    println("Halfway through");

    val fis2 = new BufferedInputStream(new FileInputStream(file2));
    val fos2 = new BufferedOutputStream(new FileOutputStream(file3));

    CryptoToolBox.decrypt_aes_cbc_file(fis2, fos2, key, iv);

    println("Done, hashing");
    //compare files
    val f1 = CryptoToolBox.ba2b64(CryptoToolBox.sha256_file(new FileInputStream(file1)));
    val f2 = CryptoToolBox.ba2b64(CryptoToolBox.sha256_file(new FileInputStream(file3)));

    assert(f1 == f2);

    println("---------------------------------")
    println("Test finished.")
  }

  def directoryWatchTest() {
    println("Starting Directory-Watch Test")
    println("---------------------------------")

    Core.watcherThread.join();

    println("---------------------------------")
    println("Test finished.")
  }
  def cryptoHybridTest() {
    println("Starting HYBRID-ENCRYPTION Test")
    println("---------------------------------")

    val symKeySize = 256 //in bits
    val asymKeySize = 2048 //in bits
    val step = 4;
    val hashSize = 160
    val maxSize = asymKeySize - 2 - 2 * hashSize

    val crypto = new SHA256_AESCBC_RSAPSS_RSAOAEP_CryptoPrimitives()

    println("Current keysizes are " + symKeySize + ", " + asymKeySize + " bits")

    for (messageSize <- 1 to (2 * (maxSize / 8) / step)) {

      val kp = crypto.newAsymmetricKey(asymKeySize)

      val randomString = Randomness.randomAlphanumericString(messageSize * step); //RandomGenerator.randomSentence(5, 5, 30);
      val plain = randomString.getBytes("UTF-8");
      println("Testing with message of size = " + plain.length)

      val cipher = crypto.hybridEncrypt(plain, kp.getPublic(), symKeySize, 16 * 8);
      val plain2 = crypto.hybridDecrypt(cipher._1, cipher._2, cipher._3, kp.getPrivate());

      //println("Plain : [" + plain.length + "] " + new String(plain) + " ; Plain2 size : [" + plain2.length + "] " + new String(plain) + ";");

      val expected = ((16 * Math.ceil((1.toFloat + plain.length) / 16)).toInt);
      assert(plain2.length == expected)

      val original_padded : Array[Byte] = CryptoToolBox.padArrayWith0BytesAtTheEndUntil(plain, expected);

      assert(new String(original_padded) == new String(plain2));
    }

    println("---------------------------------")
    println("Test finished.")
  }
  def rsaOaepTest() {

    println("Starting RSA-OAEP Test")
    println("---------------------------------")

    val keySize = 2048 //in bits
    val step = 4;
    val hashSize = 160
    val maxSize = keySize - 2 - 2 * hashSize
    println("Current keysize is " + keySize + " bits, " + (keySize / 8) + "bytes, max is " + maxSize + ", " + (maxSize / 8) + " bytes")

    for (messageSize <- 1 to ((maxSize / 8) / step)) {

      val kp = CryptoToolBox.generate_encrypt_keypair(keySize);

      val randomString = Randomness.randomAlphanumericString(messageSize * step); //RandomGenerator.randomSentence(5, 5, 30);
      val plain = randomString.getBytes("UTF-8");
      println("Testing with message of size = " + plain.length)

      val cipher = CryptoToolBox.encrypt_rsa_oaep(plain, kp.getPublic());
      val plain2 = CryptoToolBox.decrypt_rsa_oaep(cipher, kp.getPrivate());

      //println("Plain : [" + plain.length + "] " + new String(plain) + " ; Plain2 size : [" + plain2.length + "] " + new String(plain) + ";");
      assert(new String(plain) != new String(cipher));
      assert(plain.length == plain2.length);
      assert(new String(plain) == new String(plain2));
    }

    println("---------------------------------")
    println("Test finished.")
  }

  def signRsassaPssTest() {
    println("Starting RSA-SSA-PSS Test")
    println("---------------------------------")

    val crypto = new SHA256_AESCBC_RSAPSS_RSAOAEP_CryptoPrimitives();

    val keySize = 1024; //in bits

    for (messageSize <- 1 to 65) {

      val kp = CryptoToolBox.generate_sign_keypair(keySize);
      val randomString = Randomness.randomAlphanumericString(messageSize * 2); //RandomGenerator.randomSentence(5, 5, 30);
      val data = randomString.getBytes("UTF-8");

      val sign = CryptoToolBox.sign_rsassa_pss(data, kp.getPrivate());

      //simulate read/write public key from file
      val stringTemp = crypto.publicKey_2_string(kp.getPublic())
      //println(stringTemp)
      val publicKey2 = crypto.string_2_publicKey(stringTemp);

      assert(CryptoToolBox.verify_rsassa_pss(data, sign, publicKey2));

      data(Randomness.randomNumber(data.length)) = 0; //change a random byte

      assert(!CryptoToolBox.verify_rsassa_pss(data, sign, kp.getPublic()));
      assert(!CryptoToolBox.verify_rsassa_pss(data, sign, publicKey2));
    }

    println("---------------------------------")
    println("Test finished.")
  }

  def base64SafeTest() {
    println("Starting Base 64 Safe Test")
    println("---------------------------------")

    for (messageSize <- 5 to 65) {

      val randomString = Randomness.randomNonAlphanumericString(messageSize * 5);
      val b64 = CryptoToolBox.byteArrayToBase64Safe(randomString.getBytes("UTF-8"))
      val finalString = new String(CryptoToolBox.base64safeToByteArray(b64));

      println("Encoding random " + (messageSize * 5) + "-bytes string, base64 has " + b64.length() + " characters")
      //println(randomString)
      //println(b64)
      //println(finalString)

      assert(randomString == finalString);
    }

    println("---------------------------------")
    println("Test finished.")
  }

  def bouncyCastleAesCBCTest() {
    println("Starting AES-CBC-256  Test")
    println("---------------------------------")

    val keySize = 32; //bytes = 256 bits
    for (messageSize <- 5 to 65) {
      val randomString = CryptoToolBox.byteArrayToBase64Safe(Randomness.randomBytes(messageSize * 5));
      println("Crypting random " + (messageSize * 5) + "-bytes string, base64 has " + randomString.length() + " characters")
      val key = Randomness.randomBytes(keySize);
      val iv = Randomness.randomBytes(16);

      val plain = randomString.getBytes("UTF-8");
      val cipher = CryptoToolBox.encrypt_aes_cbc(plain, key, iv);
      val plain2 = CryptoToolBox.decrypt_aes_cbc(cipher, key, iv);

      val expected = ((16 * Math.ceil((1.toFloat + plain.length) / 16)).toInt);
      assert(plain2.length == expected)

      val original_padded : Array[Byte] = CryptoToolBox.padArrayWith0BytesAtTheEndUntil(plain, expected);

      assert(new String(original_padded) == new String(plain2));
    }

    println("---------------------------------")
    println("Test finished.")
  }
  def bouncyCastleAesOFBTest() {

    println("Starting AES-OFB-256 Test")
    println("---------------------------------")

    val keySize = 32; //bytes = 256 bits
    for (messageSize <- 5 to 65) {
      val randomString = CryptoToolBox.byteArrayToBase64Safe(Randomness.randomBytes(messageSize * 5));
      println("Crypting random " + (messageSize * 5) + "-bytes string, base64 has " + randomString.length() + " characters")
      val key = Randomness.randomBytes(keySize);
      val iv = Randomness.randomBytes(keySize);

      val plain = randomString.getBytes("UTF-8");
      val cipher = CryptoToolBox.encrypt_aes_ofb(plain, key, iv);
      val plain2 = CryptoToolBox.decrypt_aes_ofb(cipher, key, iv);

      val expected = ((16 * Math.ceil((1.toFloat + plain.length) / 16)).toInt);
      assert(plain2.length == expected)

      val original_padded : Array[Byte] = CryptoToolBox.padArrayWith0BytesAtTheEndUntil(plain, expected);

      assert(new String(original_padded) == new String(plain2));
    }

    println("---------------------------------")
    println("Test finished.")
  }

  def fileBasedDatabaseTest() {
    println("Starting File-Based Database Test")
    println("---------------------------------")

    def rndPSDE(addressing_prefix : String) = {
      var psde = new PublicStructuralFileDatabaseEntry();

      psde.addressing = addressing_prefix + Randomness.randomAlphanumericStringOfMaxLength(50)
      psde.filename_path_version = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.metadata_aes = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.metadata_plain = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.sign_v0_publickey = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.sign_v0_tag = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.sign_vN_publickey = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.sign_vN_tag = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.cCascRead = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.cCascWrite = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.hash_directory_path = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde.iv = Randomness.randomAlphanumericStringOfMaxLength(10)
      psde
    }

    println("Trying to write entries....");
    val results1 = new StringBuilder();
    val results2 = new StringBuilder();

    val p1 = rndPSDE("a"); //prefix is important since Database will read them back in order
    val p2 = rndPSDE("b");
    val p3 = rndPSDE("c");
    results1.append(p1)
    results1.append(p2)
    results1.append(p3)

    val f = new FileBasedDatabase("A:\\DB\\", false)

    assert(f.insertEntry(p1));
    assert(f.insertEntry(p2));
    assert(f.insertEntry(p3));

    println("Trying to read them back....");
    println("---------------------------------")

    val list = f.getEntriesPSFDB();
    val list2 = list.filter(x => x.getAddressing.getValue == p1.addressing.getValue || x.getAddressing.getValue == p2.addressing.getValue || x.getAddressing.getValue == p3.addressing.getValue)

    list2 foreach println
    list2 foreach (x => results2.append(x))

    assert(results1.toString() == results2.toString())

    new File(f.absolutePath(p1.getAcronym + "_" + p1.getAddressing().getValue)).delete()
    new File(f.absolutePath(p2.getAcronym + "_" + p2.getAddressing().getValue)).delete()
    new File(f.absolutePath(p3.getAcronym + "_" + p3.getAddressing().getValue)).delete()

    println("---------------------------------")
    println("Test finished.")
  }

  def dbRandomReadWriteTest() {
    println("Starting Random Serialization Deserialization Test")
    var psde = new PublicStructuralFileDatabaseEntry();

    psde.addressing = Randomness.randomAlphanumericStringOfMaxLength(50)
    psde.filename_path_version = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.metadata_aes = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.metadata_plain = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.sign_v0_publickey = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.sign_v0_tag = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.sign_vN_publickey = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.sign_vN_tag = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.cCascRead = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.cCascWrite = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.hash_directory_path = Randomness.randomAlphanumericStringOfMaxLength(10)
    psde.iv = Randomness.randomAlphanumericStringOfMaxLength(10)

    println(psde)

    val filename = Randomness.randomUniqueFileName("A:/", "psdbe_", ".data")
    println("Saving object in " + filename)
    val res = Serializer.serialize(filename, psde)
    assert(res)

    println("Trying to read it back...")
    val psde2 = Serializer.deserialize(filename).getOrElse(new PublicStructuralFileDatabaseEntry())

    println("******************************************************")
    println(psde)
    println("******************************************************")
    println(psde2)
    println("******************************************************")

    assert(psde.toString() == psde2.toString())
    new File(filename).delete()

    println("---------------------------------")
    println("Test finished.")
  }

  def writeRightsTest() {
    println("Starting Read-Write Rights Test")
    println("---------------------------------")

    println("Trying to write a file that exists, expected : TRUE");
    assert(FileToolBox.fileWritable("A:/data.obj"))

    println("Trying to write in a directory with rights, expected : TRUE");
    assert(FileToolBox.fileWritable("A:/file_that_does_not_exists.unlikelyextension"))

    println("Trying to read a directory, expected : FALSE");
    assert(!FileToolBox.fileReadable("A:/"))

    println("Trying to read a file that doesn't exist : FALSE");
    assert(!FileToolBox.fileReadable("A:/file_that_does_not_exists.unlikelyextension"))

    println("---------------------------------")
    println("Test finished.")
  }
}