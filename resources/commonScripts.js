

 $('#navbar-identity').html('<p><i class="fa fa-spin fa-circle-o-notch"></i> Loading...</p>');
 function refreshIdentity()
 {
   $.ajax(
     {
       type: "GET",
       url: '/repo-read/current-id'
     })
     .done(function (data)
     {
       $('#navbar-identity').html((data + "").replace("fa-check-circle-o", "fa-user"));
     });
 }



$(function() {
   $('#menu1').hover(
   function ()
   {
     $(this).addClass('open')
   },
   function ()
   {
     $(this).removeClass('open')
   }
 )
   refreshIdentity();
});

 var storedAction = ""

function confirmAndAjaxWithIdAndCallback(url, id, text, callback)
{
  storedAction = url;
  $('#'+id).html('<div class="actionConfirm">' +
    '<p class="confirm"><i class="fa fa-bolt"></i> Confirmation requested</p>' +
    '<p>The following action will start : </p><p class="action">' +
    text + '</p><p>would you like to proceed ?&nbsp;' +
    '<a href="#" id="actionYes'+id+'"><i class="fa fa-check"></i> Yes, Continue.</a></p>' +
    '</div>');


  $('#actionYes'+id).click(function ()
  {
    getReplyWithCallBack(storedAction, callback)
  });
}

function nullFunc()
{

}
function confirmAndAjaxWithCallback(url, text, callback)
{
  storedAction = url;
  $('#results').html('<div class="actionConfirm">' +
    '<p class="confirm"><i class="fa fa-bolt"></i> Confirmation requested</p>' +
    '<p>The following action will start : </p><p class="action">' +
    text + '</p><p>would you like to proceed ?&nbsp;' +
    '<a href="#" id="actionYes"><i class="fa fa-check"></i> Yes, Continue.</a></p>' +
    '</div>');


  $('#actionYes').click(function ()
  {
    getReplyWithCallBack(storedAction, callback)
  });
}
function confirmAndAjax(url, text)
{
  storedAction = url;
  $('#results').html('<div class="actionConfirm">' +
    '<p class="confirm"><i class="fa fa-bolt"></i> Confirmation requested</p>' +
    '<p>The following action will start : </p><p class="action">' +
    text + '</p><p>would you like to proceed ?&nbsp;' +
    '<a href="#" id="actionYes"><i class="fa fa-check"></i> Yes, Continue.</a></p>' +
    '</div>');


  $('#actionYes').click(function ()
  {
    getReply(storedAction)
  });
}

function getReply(url)
{
  $('#results').html('<p><i class="fa fa-spin fa-circle-o-notch"></i> Working in background...</p>');
  $.ajax(
    {
      type: "GET",
      url: url
    })
    .done(function (data)
    {
      $('#results').html('<div class="resultsHeader">' +
        '<i class="fa fa-bar-chart-o"></i> Results of operation :</div>' +
        data);
    });
}

function getReplyWithIdAndCallBack(url, id, callback)
{
  $('#r'+id).html('<p><i class="fa fa-spin fa-circle-o-notch"></i> Working in background...</p>');
  $.ajax(
    {
      type: "GET",
      url: url
    })
    .done(function (data)
    {
      $('#'+id).html('<div class="resultsHeader">' +
        '<i class="fa fa-bar-chart-o"></i> Results of operation :</div>' +
        data);
      callback();
    });
}

function getReplyWithCallBack(url, callback)
{
  $('#results').html('<p><i class="fa fa-spin fa-circle-o-notch"></i> Working in background...</p>');
  $.ajax(
    {
      type: "GET",
      url: url
    })
    .done(function (data)
    {
      $('#results').html('<div class="resultsHeader">' +
        '<i class="fa fa-bar-chart-o"></i> Results of operation :</div>' +
        data);
      callback();
    });
}

function highlightIfEmpty(id)
{
  if ($('#' + id).val() == "")
  {
    highlight(id)
    return true;
  }
  else
  {
    return false;
  }
}

function highlight(id)
{

  $('#' + id).addClass('text-shakefix');
  $('#' + id).effect("shake");
  $('#' + id).removeClass('text-shakefix');
  $('#' + id).addClass('highlight');
  $('#' + id).focus();

  setTimeout(function ()
  {
    $('#' + id).removeClass('highlight');
  }, 2000);
}