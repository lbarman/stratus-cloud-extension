var userTypeId = '#navbar-poweruser'
var poweruser = '<i class="fa fa-wrench"></i>&nbsp;Power user'
var normaluser = '<i class="fa fa-smile-o"></i>&nbsp;Normal user'
var isPowerUser = false;

function refreshPowerUser()
{
  isPowerUser = $.cookie('isPowerUser')
  if (isPowerUser == "true")
  {
    $(userTypeId).html(poweruser);
    $('link[title=normalUserCss]')[0].disabled = true;
    $('link[title=normalUserCss]').prop('disabled', true);
  }
  else
  {
    $(userTypeId).html(normaluser);
    $('link[title=normalUserCss]')[0].disabled = false;
    $('link[title=normalUserCss]').prop('disabled', false);
  }
}


$(function() {


$(userTypeId).click(function ()
{
  if (isPowerUser == "true")
  {
    $.cookie('isPowerUser', 'false',
    {
      expires: 365,
      path: '/'
    });
  }
  else
  {
    $.cookie('isPowerUser', 'true',
    {
      expires: 365,
      path: '/'
    });
  }
  refreshPowerUser()
});

	$(userTypeId).attr('href', '#');
    refreshPowerUser();
});