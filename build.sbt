name := "SecureCloudRepo"

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.5.1") 

packSettings

version := "1.0"

packMain := Map("SecureCloudRepo" -> "main.scala.Core")

scalaVersion := "2.10.3"

libraryDependencies += "org.mashupbots.socko" %% "socko-webserver" % "0.4.1"

mainClass in (Compile, run) := Some("main.scala.Core")